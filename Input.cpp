/*
Name :  Input.cpp
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 20-06-2016
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.


Description:
This module gets input from a file for entering a program.
Notes:
In the next version, read input from a source file as filename.c
Bug and Version history;
0.1 Initial version
0.2 Implemented a end of input flag
*/

#include "Input.h"
#include "Lexer.h"
#include <fstream>
#include "Output.h"
/* Expresssion string */
std::vector <UINT8> s;

/* Get input from user. */
void GetInput(void)
{
   s.clear();
   std::string Filename = "ArithmeticExpression.txt";
   std::cout <<"Enter a arithmetic expression.Operators are: * / - + % ^ (pow), , <<, >>, <, <=, >, >= , ==, != , &, xor, |, &&, ||,sin,cos,tan,cosec,sec and cot,loge. Use ( ) for subexpressions.";
   std::cout <<"Reading input file, Input arithmetic expression(s) are:" << std::endl;
   ReadFile(Filename);
   DisplayOutput(s);
}
 
/*
 Component Function: void ReadFile(std::string &Filename)
 Arguments:  Vector to process input and input file name.
 returns: None
 Description:
 Reads a ascii or UTF8 file into a vector.
 Version : 0.1
 Notes:
 */
void ReadFile(std::string &Filename)
{
  std::string line;
  BOOL EndofInput = false;
  std::ifstream myfile (Filename);
  SINT8 c;
  if (myfile.is_open())
  {
    while (myfile.get(c ) && EndofInput== false)  // For reading bytes
    {
    	/*
          Read input until EOF
        */
       if(c == EOF)
       {
         EndofInput = true;
       }
       else
       {
      	 s.push_back(c);
	   }
	   
    }
    myfile.close();
  }

  else
  {
      std::cout << std:: endl << "File not found";
  }
}
