
/***********************************************************
* Name: std_types.h                                        *
* Author: Amit Malyala                                     *
* Description: Custom data types for C/C++ programs        *
***********************************************************/
/*
Version and bug history:
0.1 Initial version.
0.2 Added variant data type for storing various data types in a container.
0.3 Modified variant data type to define a non Data which can contain a operator.
0.4 Changed Variant container, removed enum with a type Data that takes header constants.
0.5 Added SINT64 and VSINT64 for long int and volatile long int and long double-Long double.
*/
/*
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files(the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions :
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/

#ifndef STD_TYPES_H
#define STD_TYPES_H

/*--------------------- Typedefs--------------------------*/
typedef bool BOOL;	   /* UINT8 8 bits */
typedef signed int SINT32; 		   /* signed int 32 bits */
typedef unsigned int UINT32;   /* usnigned int 32 bits */
typedef unsigned short USHORT16; /* unsigned short 16 bits */
typedef signed short SHORT16; 	/* signed short int 16 bits */
typedef unsigned long UINT64;  /* unsigned long */
typedef signed long int SINT64;        /* Long int */
typedef unsigned char UINT8;   /* unsigned char 8 bits */
typedef char SINT8;    	       /* signed char 8 bits */
typedef unsigned long long ULONGLONG; /* unsigned long bits unknown*/
typedef long long LONGLONG;  /* int64_t long bits unknown */
typedef float  FLOAT32;		   /* Floating point float 32 bits */
typedef double FLOAT64;		   /* Floating point double 64 bits */
typedef volatile signed int VSINT32; 		     /* signed int 32 bits */
typedef volatile unsigned int VUINT32;   /* usnigned int 32 bits */
typedef volatile unsigned short VUSHORT16; /* unsigned short int 16 bits */
typedef volatile short  VSHORT16; 		 /* signed short int 16 bits */
typedef volatile unsigned long VUINT64;  /* unsigned long */
typedef volatile long int VSINT64;        /* Long int */

typedef volatile unsigned char VUINT8;   /* unsigned char 8 bits */
typedef volatile unsigned long long  VULONGLONG; /* unsigned long bits unknown*/
typedef volatile long long VLONGLONG;  /* long long bits unknown */
typedef volatile char VSINT8;    	     /* signed char 8 bits */
typedef volatile float  VFLOAT32;		 /* Floating point float 32 bits */
typedef volatile double VFLOAT64;		 /* Floating point double 64 bits */

typedef long double FLOAT128;   /* Floating point long double 128 bits */
typedef volatile long double VFLOAT128;  /* Floating point long double 128 bits */

#ifndef ZERO
#define ZERO 0
#endif

#ifndef ONE
#define ONE 1
#endif
// A Variant data type
// Container of this type will store data of type unsigned int , int, double, float,short ,ushort and char Array
typedef struct
{
   // Name of the variable
   const SINT8 *Identifier;
   /* Type of data */
   UINT32 Type;
   union 
   {
      FLOAT128 longdoubleValue;
      FLOAT64 doubleValue;
      FLOAT32 floatValue;
      ULONGLONG ulonglongValue;
	  LONGLONG longlongValue;
      UINT64 ulongValue;
      SINT64 longValue;
      UINT32 uintValue;
      SINT32 intValue;
      USHORT16 ushortValue;
	  SHORT16 shortValue;
      UINT8 ucharValue;
      SINT8 charValue;
      BOOL boolValue;
      const SINT8 *cString;
    } u;
} Variant;
#endif /* #ifndef STD_TYPES_H */
