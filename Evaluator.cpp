/*
Name :  Evaluator.cpp
Author: Amit Malyala
Date : 05-10-2016
Version:
0.1 Initial version
0.2 Added ComputeArithmetic, ChangeTypes functions
0.2 Added debug code.
0.3 Introduced static_cast for all type conversions.
0.4 Added functionality for modulo operator.
0.5 Added loge operators on Valueleft.doublevalue for double data type in ComputeArithmeticonTokens() function.
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
This module evaluates expressions in a Binary tree or in a binary expression tree or RPN stack.
To do:
*/

#include "Parser.h"
#include <iostream>
#include "Evaluator.h"
#include "Lexer.h"
#include "cmath"

// Define value of PI
constexpr FLOAT64 PI = 3.14159265;

#if defined (EVALUATION_METHOD_EXPRESSIONTREE)
/*
Component Function: Variant EvaluateExpressionTree(Tnode *Tree)
Arguments: A binary tree with its root as argument
Returns : Variant
Version : 0.1

Reference:
Ullman and Aho - Book: Foundations of computer science, W.H Freeman and Co. Ltd, 1995

Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double.
The requirement for this function to work is that the binary expression tree should be in correct order and all variant data types
have their type declared.
A variant is used to return a data type which is calculated at runtime.
To do:
*/
Variant EvaluateExpressionTree(Tnode *Tree)
{
	//std::cout  <<"Evaluate expression tree called " << std::endl;
	Variant ValLeft, ValRight, Value;
	InitVariant(Value);

	UINT32 OpType = NOTOP;
	if (Tree->NodeType == NODE_LABEL_OPERATOR)
	{
		//std::string OpString(Tree->Op);
		//OpType= DetectOpType(OpString);
		OpType = DetectOpType(Tree->Op);
	}
	/* If node is a leaf or operand */
	if (Tree->NodeType == NODE_LABEL_OPERAND)
	{
		//std::cout <<"Node is operand" << std::endl;
		Value = Tree->Data;
	}
	else if (Tree->NodeType == NODE_LABEL_OPERATOR && (OpType != OPUNARYMINUS && OpType != OPSIN && OpType != OPCOS && OpType != OPTAN && \
		OpType != OPSEC && OpType != OPCOT && OpType != OPCOSEC && OpType != OPSQRT && OpType != OPLOG))
	{
		//std::cout <<"Node is operator" << std::endl;
		ValLeft = EvaluateExpressionTree(Tree->LeftChild);
		ValRight = EvaluateExpressionTree(Tree->RightChild);
		Value = ComputeArithmetic(Tree, ValLeft, ValRight, Value);
	}

	else if (Tree->NodeType == NODE_LABEL_OPERATOR && (OpType == OPUNARYMINUS || OpType == OPSIN || OpType == OPCOS || OpType == OPTAN || \
		OpType == OPSEC || OpType == OPCOT || OpType == OPCOSEC || OpType == OPSQRT || OpType == OPLOG))
	{
		ValLeft = EvaluateExpressionTree(Tree->LeftChild);
		switch (ValLeft.Type)
		{
		case INTTYPE:
			ValRight.Type = INTTYPE;
			ValRight.u.intValue = 0;
			break;
		case SHORTTYPE:
			ValRight.Type = SHORTTYPE;
			ValRight.u.shortValue = 0;
			break;
		case USHORTTYPE:
			ValRight.Type = SHORTTYPE;
			ValRight.u.shortValue = 0;
			break;
			break;
		case UINTTYPE:
			ValRight.Type = UINTTYPE;
			ValRight.u.uintValue = 0;
			break;
		case LONGINTTYPE:
			ValRight.Type = LONGINTTYPE;
			ValRight.u.longValue = 0;
			break;
		case ULONGINTTYPE:
			ValRight.Type = ULONGINTTYPE;
			ValRight.u.ulongValue = 0;
			break;
		case FLOATTYPE:
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = 0;
			break;
		case DOUBLETYPE:
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = 0;
			break;
		default:
			break;
		}
		Value = ComputeArithmetic(Tree, ValLeft, ValRight, Value);
	}
	/* Return a variant type */
	return Value;
}

/*
Component Function: Variant ComputeArithmetic(Tnode *Tree, Variant& ValLeft, Variant& ValRight, Variant& Value)
Arguments:
Returns : Variant
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double.
The requirement for this function to work is that the binary expression tree should be incorrect order.
A variant is used to return a data type which is calculated at runtime.
To do:
Known issues:
None
*/
Variant ComputeArithmetic(Tnode *Tree, Variant& ValLeft, Variant& ValRight, Variant& Value)
{
	UINT32 DataType = NOTADATATYPE;
	//std::string OpString(Tree->Op);
	//UINT32 OpType= DetectOpType(OpString);
	UINT32 OpType = DetectOpType(Tree->Op);
	/* Type cast types of operands */
	ChangeTypes(ValLeft, ValRight, Value);
	DataType = Value.Type;
	/* Do arithmetic operation on operands */
	#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
	switch (DataType)
	{
	case INTTYPE:
		std::cout << "\nType returned by Change types is INTTYPE";
		break;
	case UINTTYPE:
		std::cout << "\nType returned by Change types is UINTTYPE";
		break;
	case FLOATTYPE:
		std::cout << "\nType returned by Changetypes is FLOATTYPE";
		break;
	case DOUBLETYPE:
		std::cout << "\nType returned by Changetypes is DOUBLETYPE";
		break;
	case LONGDOUBLETYPE:
		std::cout << "\nType returned by Changetypes is LONGDOUBLETYPE";
		break;
	case LONGINTTYPE:
		std::cout << "\nType returned by Changetypes is LONGINTTYPE";
		break;
	case ULONGINTTYPE:
		std::cout << "\nType returned by Changetypes is Unsigned LONGINTTYPE";
		break;
	case SHORTTYPE:
		std::cout << "\nType returned by Changetypes is short type";
		break;
	case USHORTTYPE:
		std::cout << "\nType returned by Changetypes is unsigned short type";
		break;
	default:
		break;
	}
	std::cout << "\nOperator in Compute arithmetic is " << Tree->Op;
	#endif

	if (DataType == INTTYPE)
	{
		switch (OpType)
		{
		case  OPPLUS:
			Value.u.intValue = ValLeft.u.intValue + ValRight.u.intValue;
			break;
		case OPMINUS:
			Value.u.intValue = ValLeft.u.intValue - ValRight.u.intValue;
			break;
		case OPMUL:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.intValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.intValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPMODULUS:
			Value.u.intValue = ValLeft.u.intValue % ValRight.u.intValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.intValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;

		case OPUNARYMINUS:
			Value.u.intValue = -ValLeft.u.intValue;
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.intValue = (ValLeft.u.intValue < ValRight.u.intValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.intValue = (ValLeft.u.intValue > ValRight.u.intValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.intValue = (ValLeft.u.intValue <= ValRight.u.intValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.intValue = (ValLeft.u.intValue >= ValRight.u.intValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.intValue = (ValLeft.u.intValue << ValRight.u.intValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.intValue = (ValLeft.u.intValue >> ValRight.u.intValue);
			break;
		case OPBITWISEAND:
			Value.u.intValue = (ValLeft.u.intValue & ValRight.u.intValue);
			break;
		case OPBITWISEOR:
			Value.u.intValue = (ValLeft.u.intValue | ValRight.u.intValue);
			break;
		case OPLOGICALNOT:
			Value.u.intValue = !(ValLeft.u.intValue);
			break;
		case OPLOGICALAND:
			Value.u.intValue = ((ValLeft.u.intValue) && (ValRight.u.intValue));
			break;
		case OPLOGICALOR:
			Value.u.intValue = ((ValLeft.u.intValue) || (ValRight.u.intValue));
			break;
		/* For later	
		case OPEQUALTO:
			ValLeft.u.intValue = ValRight.u.intValue;
			Value.u.intValue = ValLeft.u.intValue;
			break;
		*/	
		default:
			break;
		}

#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (Value.Type == INTTYPE)
		{
			//std::cout << "\nType is int type in ComputeArithmetic ";
			//std::cout << std::endl << "Value left " << ValLeft.u.intValue;
			//std::cout << std::endl << "Value right " << ValRight.u.intValue;
			//std::cout << std::endl << "Value " << Value.u.uintValue;
		}
#endif
	}
	else if (DataType == UINTTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.uintValue = ValLeft.u.uintValue + ValRight.u.uintValue;
			break;
		case OPMINUS:
			Value.u.uintValue = ValLeft.u.uintValue - ValRight.u.uintValue;
			break;
		case OPMUL:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.uintValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.uintValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPUNARYMINUS: /* Unary minus */
			ValLeft.Type = LONGINTTYPE;
			ValLeft.u.longValue = static_cast<SINT64> (ValLeft.u.uintValue);
			/*
			ValRight.Type = LONGINTTYPE;
			ValRight.u.longValue = 0;
			ChangeTypes(ValLeft, ValRight, Value);
			*/
			Value.u.longValue = -ValLeft.u.longValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.uintValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPMODULUS:
			Value.u.uintValue = ValLeft.u.uintValue % ValRight.u.uintValue;
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.uintValue = (ValLeft.u.uintValue < ValRight.u.uintValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.uintValue = (ValLeft.u.uintValue > ValRight.u.uintValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.uintValue = (ValLeft.u.uintValue <= ValRight.u.uintValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.uintValue = (ValLeft.u.uintValue >= ValRight.u.uintValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.uintValue = (ValLeft.u.uintValue << ValRight.u.uintValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.uintValue = (ValLeft.u.uintValue >> ValRight.u.uintValue);
			break;
		case OPBITWISEAND:
			Value.u.uintValue = (ValLeft.u.uintValue & ValRight.u.uintValue);
			break;
		case OPBITWISEOR:
			Value.u.uintValue = (ValLeft.u.uintValue | ValRight.u.uintValue);
			break;
		case OPLOGICALNOT:
			Value.u.uintValue = !(ValLeft.u.uintValue);
			break;
		case OPLOGICALAND:
			Value.u.uintValue = (((((ValLeft.u.uintValue) && (ValRight.u.uintValue)))));
			break;
		case OPLOGICALOR:
			Value.u.uintValue = (ValLeft.u.uintValue) || (ValRight.u.uintValue);
			break;
		/* 
		For later
		case OPEQUALTO:
			ValLeft.u.uintValue = ValRight.u.uintValue;
			Value.u.uintValue = ValLeft.u.uintValue;
			break;
		*/	
		default:
			break;
		}
		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (Value.Type == UINTTYPE)
		{
			//std::cout << "\nType is uint type in ComputeArithmetic ";
			//std::cout << std::endl << "Value left " << ValLeft.u.uintValue;
			//std::cout << std::endl << "Value right " << ValRight.u.uintValue;
			//std::cout << std::endl << "Value " << Value.u.uintValue;
		}
		#endif

	}
	else if (DataType == SHORTTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.shortValue = ValLeft.u.shortValue + ValRight.u.shortValue;
			break;
		case OPMINUS:
			Value.u.shortValue = ValLeft.u.shortValue - ValRight.u.shortValue;
			break;
		case OPMUL:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.shortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.shortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPUNARYMINUS:
			Value.u.shortValue = -ValLeft.u.shortValue;
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;

		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;

		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPMODULUS:
			Value.u.shortValue = ValLeft.u.shortValue % ValRight.u.shortValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.shortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.shortValue = (ValLeft.u.shortValue < ValRight.u.shortValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.shortValue = (ValLeft.u.shortValue > ValRight.u.shortValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.shortValue = (ValLeft.u.shortValue <= ValRight.u.shortValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.shortValue = (ValLeft.u.shortValue >= ValRight.u.shortValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.shortValue = (ValLeft.u.shortValue << ValRight.u.shortValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.shortValue = (ValLeft.u.shortValue >> ValRight.u.shortValue);
			break;
		case OPBITWISEAND:
			Value.u.shortValue = (ValLeft.u.shortValue & ValRight.u.shortValue);
			break;
		case OPBITWISEOR:
			Value.u.shortValue = (ValLeft.u.shortValue | ValRight.u.shortValue);
			break;
		case OPLOGICALNOT:
			Value.u.shortValue = !(ValLeft.u.shortValue);
			break;
		case OPLOGICALAND:
			Value.u.shortValue = (ValLeft.u.shortValue) && (ValRight.u.shortValue);
			break;
		case OPLOGICALOR:
			Value.u.shortValue = (ValLeft.u.shortValue) || (ValRight.u.shortValue);
			break;

        /*  
		For later
		case OPEQUALTO:
			ValLeft.u.shortalue = ValRight.u.shortValue;
			Value.u.shortValue = ValLeft.u.shortValue;
			break;
		*/	

		default:
			break;
		}
		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (Value.Type == SHORTTYPE)
		{
			std::cout << "\nType is short type in ComputeArithmetic ";
			std::cout << std::endl << "Value left " << ValLeft.u.shortValue;
			std::cout << std::endl << "Value right " << ValRight.u.shortValue;
			std::cout << std::endl << "Value " << Value.u.shortValue;
		}
		#endif

	}
	else if (DataType == USHORTTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.ushortValue = ValLeft.u.ushortValue + ValRight.u.ushortValue;
			break;
		case OPMINUS:
			Value.u.ushortValue = ValLeft.u.ushortValue - ValRight.u.ushortValue;
			break;
		case OPMUL:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ushortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ushortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPUNARYMINUS: /* Unary minus */
			ValLeft.Type = INTTYPE;
			ValLeft.u.intValue = static_cast<SINT32> (ValLeft.u.ushortValue);
			/*
			ValRight.Type = INTTYPE;
			ValRight.u.intValue = 0;
			ChangeTypes(ValLeft, ValRight, Value);
			*/
			Value.u.intValue = -ValLeft.u.intValue;
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;

		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;

		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPMODULUS:
			Value.u.ushortValue = ValLeft.u.ushortValue % ValRight.u.ushortValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ushortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.ushortValue = (ValLeft.u.ushortValue < ValRight.u.ushortValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.ushortValue = (ValLeft.u.ushortValue > ValRight.u.ushortValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.ushortValue = (ValLeft.u.ushortValue <= ValRight.u.ushortValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.ushortValue = (ValLeft.u.ushortValue >= ValRight.u.ushortValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.ushortValue = (ValLeft.u.ushortValue << ValRight.u.ushortValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.ushortValue = (ValLeft.u.ushortValue >> ValRight.u.ushortValue);
			break;
		case OPBITWISEAND:
			Value.u.ushortValue = (ValLeft.u.ushortValue & ValRight.u.ushortValue);
			break;
		case OPBITWISEOR:
			Value.u.ushortValue = (ValLeft.u.ushortValue | ValRight.u.ushortValue);
			break;
		case OPLOGICALNOT:
			Value.u.ushortValue = !(ValLeft.u.ushortValue);
			break;
		case OPLOGICALAND:
			Value.u.ushortValue = ((ValLeft.u.ushortValue) && (ValRight.u.ushortValue));
			break;
		case OPLOGICALOR:
			Value.u.ushortValue = ((ValLeft.u.ushortValue) || (ValRight.u.ushortValue));
			break;
		/* For later	
		case  OPEQUALTO:
			ValLeft.u.ushortalue = ValRight.u.ushortValue;
			Value.u.ushortValue = ValLeft.u.ushortValue;
			break;
		*/	
		default:
			break;
		}
		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (Value.Type == USHORTTYPE)
		{
			//std::cout << "\nType is short type in ComputeArithmetic ";
			//std::cout << std::endl << "Value left " << ValLeft.u.ushortValue;
			//std::cout << std::endl << "Value right " << ValRight.u.ushortValue;
			//std::cout << std::endl << "Value " << Value.u.ushortValue;
		}
		#endif

	}
	else if (DataType == FLOATTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.floatValue = ValLeft.u.floatValue + ValRight.u.floatValue;
			break;
		case OPMINUS:
			Value.u.floatValue = ValLeft.u.floatValue - ValRight.u.floatValue;
			break;
		case OPMUL:
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPUNARYMINUS: /* Unary minus */
			Value.u.floatValue = -ValLeft.u.floatValue;
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
			break;
		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPMODULUS:
			ErrorTracer(MODULUS_OPERATOR_ON_FLOAT_NUMBERS, 0, 0);
			Value.u.floatValue = 0;
			break;
		case OPPOWEXPONENT:
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		/* For later	

		case OPEQUALTO:
			ValLeft.u.floatValue = ValRight.u.floatValue;
			Value.u.floatValue = ValLeft.u.floatValue;
			break;
		*/	
		default:
			break;
		}
  		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (DataType == FLOATTYPE)
		{
			//std::cout << "\nType is float type in ComputeArithmetic ";
			//std::cout << std::endl << "Value left " << ValLeft.u.floatValue;
			//std::cout << std::endl << "Value right " << ValRight.u.floatValue;
			//std::cout << std::endl << "Value " << Value.u.floatValue;
		}
		#endif

	}
	else if (DataType == DOUBLETYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.doubleValue = ValLeft.u.doubleValue + ValRight.u.doubleValue;
			break;
		case OPMINUS:
			Value.u.doubleValue = ValLeft.u.doubleValue - ValRight.u.doubleValue;
			break;
		case OPMUL:
			Value.u.doubleValue = ValLeft.u.doubleValue * ValRight.u.doubleValue;
			break;
		case OPDIV:
			Value.u.doubleValue = ValLeft.u.doubleValue / ValRight.u.doubleValue;
			break;
		case OPUNARYMINUS: /* Unary minus */
			Value.u.doubleValue = -ValLeft.u.doubleValue;
			break;
		case OPSIN:

			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
		case OPCOSEC:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPMODULUS:
			ErrorTracer(MODULUS_OPERATOR_ON_FLOAT_NUMBERS, 0, 0);
			Value.u.doubleValue = 0;
			break;
		case OPPOWEXPONENT:
			Value.u.doubleValue = pow(ValLeft.u.doubleValue, ValRight.u.doubleValue);
			break;
		case OPSQRT:
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		/*	
		case OPEQUALTO:
			ValLeft.u.doubleValue = ValRight.u.doubleValue;
			Value.u.doubleValue = ValLeft.u.doubleValue;
			break;
		*/	
		default:
			break;
		}
		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (DataType == DOUBLETYPE)
		{
			std::cout << "\nType is double type in ComputeArithmetic ";
			std::cout << std::endl << "Value left " << ValLeft.u.doubleValue;
			std::cout << std::endl << "Value right " << ValRight.u.doubleValue;
			std::cout << std::endl << "Value " << Value.u.doubleValue;
		}
		#endif
	}
	else if (DataType == LONGINTTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.longValue = ValLeft.u.longValue + ValRight.u.longValue;
			break;
		case OPMINUS:
			Value.u.longValue = ValLeft.u.longValue - ValRight.u.longValue;
			break;
		case OPMUL:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.longValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:

			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.longValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPUNARYMINUS: /* Unary minus */
			Value.u.longValue = -ValLeft.u.longValue;
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;

		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;

		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPMODULUS:
			Value.u.longValue = ValLeft.u.longValue % ValRight.u.longValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.longValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.longValue = (ValLeft.u.longValue < ValRight.u.longValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.longValue = (ValLeft.u.longValue > ValRight.u.longValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.longValue = (ValLeft.u.longValue <= ValRight.u.longValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.longValue = (ValLeft.u.longValue >= ValRight.u.longValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.longValue = (ValLeft.u.longValue << ValRight.u.longValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.longValue = (ValLeft.u.longValue >> ValRight.u.longValue);
			break;
		case OPBITWISEAND:
			Value.u.longValue = (ValLeft.u.longValue & ValRight.u.longValue);
			break;
		case OPBITWISEOR:
			Value.u.longValue = (ValLeft.u.longValue | ValRight.u.longValue);
			break;
		case OPLOGICALNOT:
			Value.u.longValue = !(ValLeft.u.longValue);
			break;
		case OPLOGICALAND:
			Value.u.longValue = ((ValLeft.u.longValue) && (ValRight.u.longValue));
			break;
		case OPLOGICALOR:
			Value.u.longValue = ((ValLeft.u.longValue) || (ValRight.u.longValue));
			break;
		/* For later	

		case OPEQUALTO:
			ValLeft.u.longValue = ValRight.u.longValue;
			Value.u.longValue = ValLeft.u.longValue;
			break;
		*/	
		default:
			break;
		}

		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (DataType == LONGINTTYPE)
		{
			//std::cout << "\nType is long int type in ComputeArithmetic ";
			//std::cout << std::endl << "Value " << Value.u.longValue;
			//std::cout << std::endl << "Value left " << ValLeft.u.longValue;
			//std::cout << std::endl << "Value right " << ValRight.u.longValue;
		}
		#endif
	}
	else if (DataType == ULONGINTTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.ulongValue = ValLeft.u.ulongValue + ValRight.u.ulongValue;
			break;
		case OPMINUS:
			Value.u.ulongValue = ValLeft.u.ulongValue - ValRight.u.ulongValue;
			break;
		case OPMUL:

			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ulongValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ulongValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPUNARYMINUS: /* Unary minus */
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ulongValue);
			/*
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = 0;
			ChangeTypes(ValLeft, ValRight, Value);
			*/
			Value.u.floatValue = -ValLeft.u.floatValue;
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);

			break;
		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);

			break;
		case OPMODULUS:
			Value.u.ulongValue = ValLeft.u.ulongValue % ValRight.u.ulongValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ulongValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.ulongValue = (ValLeft.u.ulongValue < ValRight.u.ulongValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.ulongValue = (ValLeft.u.ulongValue > ValRight.u.ulongValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.ulongValue = (ValLeft.u.ulongValue <= ValRight.u.ulongValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.ulongValue = (ValLeft.u.ulongValue >= ValRight.u.ulongValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.ulongValue = (ValLeft.u.ulongValue << ValRight.u.ulongValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.ulongValue = (ValLeft.u.ulongValue >> ValRight.u.ulongValue);
			break;
		case OPBITWISEAND:
			Value.u.ulongValue = (ValLeft.u.ulongValue & ValRight.u.ulongValue);
			break;
		case OPBITWISEOR:
			Value.u.ulongValue = (ValLeft.u.ulongValue | ValRight.u.ulongValue);
			break;
		case OPLOGICALNOT:
			Value.u.ulongValue = !(ValLeft.u.ulongValue);
			break;
		case OPLOGICALAND:
			Value.u.ulongValue = ((ValLeft.u.ulongValue) && (ValRight.u.ulongValue));
			break;
		case OPLOGICALOR:
			Value.u.ulongValue = ((ValLeft.u.ulongValue) || (ValRight.u.ulongValue));
			break;
		/* 	

		case OPEQUALTO:
			ValLeft.u.ulongValue = ValRight.u.ulongValue;
			Value.u.ulongValue = ValLeft.u.ulongValue;
			break;
		*/	
		default:
			break;
		}

		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (DataType == ULONGINTTYPE)
		{
			//std::cout << "\nType is unsigned long type in ComputeArithmetic ";
			//std::cout << std::endl << "Value " << Value.u.ulongValue;
			//std::cout << std::endl << "Value left " << ValLeft.u.ulongValue;
			//std::cout << std::endl << "Value right " << ValRight.u.ulongValue;
		}
		#endif
	}
	else if (DataType == LONGDOUBLETYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue + ValRight.u.longdoubleValue;
			break;
		case OPMINUS:
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue - ValRight.u.longdoubleValue;
			break;
		case OPMUL:
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue * ValRight.u.longdoubleValue;
			break;
		case OPDIV:
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue / ValRight.u.longdoubleValue;
			break;
		case OPUNARYMINUS: /* Unary minus */
			Value.u.longdoubleValue = -ValLeft.u.longdoubleValue;
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPMODULUS:
			ErrorTracer(MODULUS_OPERATOR_ON_FLOAT_NUMBERS, 0, 0);
			Value.u.longdoubleValue = 0;
			break;
		case OPPOWEXPONENT:
			Value.u.longdoubleValue = pow(ValLeft.u.longdoubleValue, ValRight.u.longdoubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		/* 
		For later
		case OPEQUALTO:
			ValLeft.u.longdoubleValue = ValRight.u.longdoubleValue;
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue;
			break;
		*/	
		default:
			break;

		}

#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (DataType == LONGDOUBLETYPE)
		{
			std::cout << "\nType is double type in ComputeArithmetic ";
			std::cout << std::endl << "Value " << Value.u.longdoubleValue;
			std::cout << std::endl << "Value left " << ValLeft.u.longdoubleValue;
			std::cout << std::endl << "Value right " << ValRight.u.longdoubleValue;
		}
#endif
	}
	return Value;
}

#endif // #if defined (EVALUATION_METHOD_EXPRESSIONTREE)

/*
Component Function: void ChangeTypes(Variant& ValLeft, Variant& ValRight, Variant& Value)
Arguments:
Returns : Variant
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double.
The requirement for this function to work is that the binary expression tree should be incorrect order.
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed. Write clear code.
Notes:
Determine type of each operand, promote lower data type to higher data type in this order
int, unsigned int, long int, unsigned long int, float,double, long double
Arithmetic conversion pattern:
If either operand is of type long double, the other shall be converted to long double.
Otherwise, if either operand is double, the other shall be converted to double.
Otherwise, if either operand is float, the other shall be converted to float.
Otherwise, the integral promotions  shall be performed on both operands.)
Then, if either operand is unsigned long the other shall be converted to unsigned long.
Otherwise, if one operand is a long int and the other unsigned int, then if a long int can represent all
the values of an unsigned int, the unsigned int shall be converted to a long int; otherwise both operands
shall be converted to unsigned long int.
Otherwise, if either operand is long, the other shall be converted to long.
Otherwise, if either operand is unsigned, the other shall be converted to unsigned.
[Note: otherwise, the only remaining case is that both operands are int ]
Write a faster way to convert Data.
To do:
Calculate or assign operands for negative operations on unsigned numbers by converting them to higher order types.
Unary minus node has either left or right child operand or operator with the other child being nullptr.
 */

void ChangeTypes(Variant& ValLeft, Variant& ValRight, Variant& Value)
{
	/* Code to be added */

	if (ValLeft.Type == LONGDOUBLETYPE || ValRight.Type == LONGDOUBLETYPE)
	{
		switch (ValLeft.Type)
		{
		case INTTYPE:
			ValLeft.Type = LONGDOUBLETYPE;
			ValLeft.u.longdoubleValue = static_cast<long double> (ValLeft.u.intValue);
			break;
		case UINTTYPE:
			ValLeft.Type = LONGDOUBLETYPE;
			ValLeft.u.longdoubleValue = static_cast<long double> (ValLeft.u.uintValue);
			break;
		case LONGINTTYPE:
			ValLeft.Type = LONGDOUBLETYPE;
			ValLeft.u.longdoubleValue = static_cast<long double> (ValLeft.u.longValue);
			break;
		case ULONGINTTYPE:
			ValLeft.Type = LONGDOUBLETYPE;
			ValLeft.u.longdoubleValue = static_cast<long double> (ValLeft.u.ulongValue);
			break;
		case FLOATTYPE:
			ValLeft.Type = LONGDOUBLETYPE;
			ValLeft.u.longdoubleValue = static_cast<long double> (ValLeft.u.floatValue);
			break;
		case DOUBLETYPE:
			ValLeft.Type = LONGDOUBLETYPE;
			ValLeft.u.longdoubleValue = static_cast<long double> (ValLeft.u.doubleValue);
			break;
		case SHORTTYPE:
			ValLeft.Type = LONGDOUBLETYPE;
			ValLeft.u.longdoubleValue = static_cast<long double> (ValLeft.u.shortValue);
			break;
		case USHORTTYPE:
			ValLeft.Type = LONGDOUBLETYPE;
			ValLeft.u.longdoubleValue = static_cast<long double> (ValLeft.u.ushortValue);
			break;
		default:
			break;
		}
		switch (ValRight.Type)
		{
		case INTTYPE:
			ValRight.Type = LONGDOUBLETYPE;
			ValRight.u.longdoubleValue = static_cast<long double> (ValRight.u.intValue);
			break;
		case UINTTYPE:
			ValRight.Type = LONGDOUBLETYPE;
			ValRight.u.longdoubleValue = static_cast<long double> (ValRight.u.uintValue);
			break;
		case LONGINTTYPE:
			ValRight.Type = LONGDOUBLETYPE;
			ValRight.u.longdoubleValue = static_cast<long double> (ValRight.u.longValue);
			break;
		case ULONGINTTYPE:
			ValRight.Type = LONGDOUBLETYPE;
			ValRight.u.longdoubleValue = static_cast<long double> (ValRight.u.ulongValue);
			break;
		case FLOATTYPE:
			ValRight.Type = LONGDOUBLETYPE;
			ValRight.u.longdoubleValue = static_cast<long double> (ValRight.u.floatValue);
			break;
		case DOUBLETYPE:
			ValRight.Type = LONGDOUBLETYPE;
			ValRight.u.longdoubleValue = static_cast<long double> (ValRight.u.doubleValue);
			break;
		case SHORTTYPE:
			ValRight.Type = LONGDOUBLETYPE;
			ValRight.u.longdoubleValue = static_cast<long double> (ValRight.u.shortValue);
			break;
		case USHORTTYPE:
			ValRight.Type = LONGDOUBLETYPE;
			ValRight.u.longdoubleValue = static_cast<long double> (ValRight.u.ushortValue);
			break;
		default:
			break;
		}
		/*
		ValLeft.Type = LONGDOUBLETYPE;
		ValRight.Type = LONGDOUBLETYPE;
		 */
		Value.Type = LONGDOUBLETYPE;
	}
	else if (ValLeft.Type == DOUBLETYPE || ValRight.Type == DOUBLETYPE)
	{
		switch (ValLeft.Type)
		{
		case INTTYPE:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			break;
		case UINTTYPE:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			break;
		case LONGINTTYPE:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			break;
		case ULONGINTTYPE:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			break;
		case FLOATTYPE:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			break;

		case SHORTTYPE:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			break;
		case USHORTTYPE:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			break;
		default:
			break;
		}
		switch (ValRight.Type)
		{
		case INTTYPE:
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			break;
		case UINTTYPE:
			ValLeft.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			break;
		case LONGINTTYPE:
			ValRight.Type = DOUBLETYPE;
			ValRight.u.longdoubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			break;
		case ULONGINTTYPE:
			ValRight.Type = DOUBLETYPE;
			ValRight.u.longdoubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			break;
		case FLOATTYPE:
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			break;
		case SHORTTYPE:
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			break;
		case USHORTTYPE:
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			break;
		default:
			break;
		}

		/*
		ValLeft.Type = DOUBLETYPE;
		ValRight.Type = DOUBLETYPE;
		 */
		Value.Type = DOUBLETYPE;
	}
	else if (ValLeft.Type == FLOATTYPE || ValRight.Type == FLOATTYPE)
	{
		switch (ValLeft.Type)
		{
		case INTTYPE:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.intValue);
			break;
		case UINTTYPE:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.uintValue);

			break;
		case LONGINTTYPE:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.longValue);
			break;
		case ULONGINTTYPE:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ulongValue);
			break;
		case SHORTTYPE:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.shortValue);
			break;
		case USHORTTYPE:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ushortValue);
			break;
		default:
			break;
		}
		switch (ValRight.Type)
		{
		case INTTYPE:
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.intValue);
			break;
		case UINTTYPE:
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.uintValue);
			break;
		case LONGINTTYPE:
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.longValue);
			break;
		case ULONGINTTYPE:
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ulongValue);
			break;
		case SHORTTYPE:
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.shortValue);
			break;
		case USHORTTYPE:
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ushortValue);
			break;
		default:
			break;
		}
		/*
		ValLeft.Type = FLOATTYPE;
		ValRight.Type = FLOATTYPE;
		 */
		Value.Type = FLOATTYPE;
	}
	else if (ValLeft.Type == ULONGINTTYPE || ValRight.Type == ULONGINTTYPE)
	{
		switch (ValLeft.Type)
		{
		case LONGINTTYPE:
			if (ValLeft.u.longValue)
			{
				ValLeft.Type = ULONGINTTYPE;
				ValLeft.u.ulongValue = static_cast<UINT64> (ValLeft.u.longValue);
				Value.Type = ULONGINTTYPE;
			}
			else
			{
				//write code here
				ValLeft.Type = FLOATTYPE;
				ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.longValue);
				Value.Type = FLOATTYPE;
			}
			break;
		case UINTTYPE:
			ValLeft.Type = ULONGINTTYPE;
			ValLeft.u.ulongValue = static_cast<UINT64> (ValLeft.u.uintValue);
			Value.Type = ULONGINTTYPE;
			break;
		case INTTYPE:
			ValLeft.Type = ULONGINTTYPE;
			ValLeft.u.ulongValue = static_cast<UINT64> (ValLeft.u.intValue);

			break;
		case USHORTTYPE:
			ValLeft.Type = ULONGINTTYPE;
			ValLeft.u.ulongValue = static_cast<UINT64> (ValLeft.u.ushortValue);
			Value.Type = ULONGINTTYPE;
			break;
		case SHORTTYPE:
			if (ValRight.u.shortValue)
			{
				ValLeft.Type = LONGINTTYPE;
				ValRight.u.longValue = static_cast<UINT64> (ValRight.u.shortValue);
				Value.Type = ULONGINTTYPE;
			}
			else
			{
				ValRight.Type = LONGINTTYPE;
				ValRight.u.longValue = static_cast<SINT64> (ValRight.u.shortValue);
				Value.Type = LONGINTTYPE;
			}

			break;
		default:
			break;
		}
		switch (ValRight.Type)
		{
		case LONGINTTYPE:
			if (ValRight.u.longValue)
			{
				ValRight.Type = ULONGINTTYPE;
				ValRight.u.ulongValue = ValRight.u.longValue;
				ValRight.Type = ULONGINTTYPE;
			}
			else
			{
				// Write code here
				ValRight.Type = FLOATTYPE;
				ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.longValue);
				Value.Type = FLOATTYPE;
			}

			break;

		case UINTTYPE:
			ValRight.Type = ULONGINTTYPE;
			ValRight.u.ulongValue = static_cast<UINT64> (ValRight.u.uintValue);
			Value.Type = ULONGINTTYPE;
			break;
		case INTTYPE:
			if (ValRight.u.intValue)
			{
				ValRight.Type = ULONGINTTYPE;
				ValRight.u.ulongValue = static_cast<UINT64> (ValRight.u.intValue);
				Value.Type = ULONGINTTYPE;
			}
			else
			{
				ValRight.Type = LONGINTTYPE;
				ValRight.u.longValue = static_cast<SINT64> (ValRight.u.intValue);
				Value.Type = LONGINTTYPE;
			}

			break;
		case USHORTTYPE:
			ValRight.Type = ULONGINTTYPE;
			ValRight.u.ulongValue = static_cast<UINT64> (ValRight.u.ushortValue);
			Value.Type = ULONGINTTYPE;
			break;
		case SHORTTYPE:
			if (ValRight.u.shortValue)
			{
				ValRight.Type = ULONGINTTYPE;
				ValRight.u.ulongValue = static_cast<UINT64> (ValRight.u.shortValue);
				Value.Type = ULONGINTTYPE;
			}
			else
			{
				ValRight.Type = LONGINTTYPE;
				ValRight.u.longValue = static_cast<SINT64> (ValRight.u.shortValue);
				Value.Type = LONGINTTYPE;
			}
			break;
		default:
			break;
		}
	}
	else if (ValLeft.Type == LONGINTTYPE || ValRight.Type == LONGINTTYPE)
	{
		switch (ValLeft.Type)
		{

		case UINTTYPE:
			ValLeft.Type = LONGINTTYPE;
			ValLeft.u.longValue = static_cast<SINT64> (ValLeft.u.uintValue);
			break;
		case INTTYPE:
			ValLeft.Type = LONGINTTYPE;
			ValLeft.u.longValue = static_cast<SINT64> (ValLeft.u.intValue);
			break;
		case SHORTTYPE:
			ValLeft.Type = LONGINTTYPE;
			ValLeft.u.longValue = static_cast<SINT64> (ValLeft.u.shortValue);
			break;
		case USHORTTYPE:
			ValLeft.Type = LONGINTTYPE;
			ValLeft.u.longValue = static_cast<SINT64> (ValLeft.u.ushortValue);
			break;
		default:
			break;

		}

		switch (ValRight.Type)
		{

		case UINTTYPE:
			ValRight.Type = LONGINTTYPE;
			ValRight.u.longValue = static_cast<SINT64> (ValRight.u.uintValue);
			break;
		case INTTYPE:
			ValRight.Type = LONGINTTYPE;
			ValRight.u.longValue = static_cast<SINT64> (ValRight.u.intValue);
			break;
		case SHORTTYPE:
			ValRight.Type = LONGINTTYPE;
			ValRight.u.longValue = static_cast<SINT64> (ValRight.u.shortValue);
			break;
		case USHORTTYPE:
			ValRight.Type = LONGINTTYPE;
			ValRight.u.longValue = static_cast<SINT64> (ValRight.u.ushortValue);
			break;
		default:
			break;
		}

		/*
		ValLeft.Type = longype;
		ValRight.Type = LONGINTTYPE;
		 */
		Value.Type = LONGINTTYPE;
	}
	else if (ValLeft.Type == UINTTYPE || ValRight.Type == UINTTYPE)
	{
		switch (ValLeft.Type)
		{
		case INTTYPE:
			ValLeft.Type = UINTTYPE;
			ValLeft.u.uintValue = static_cast<UINT32> (ValLeft.u.intValue);
			break;
		case USHORTTYPE:
			ValLeft.Type = UINTTYPE;
			ValLeft.u.uintValue = static_cast<UINT32> (ValLeft.u.ushortValue);
			break;
		default:
			break;
		}
		switch (ValRight.Type)
		{
		case INTTYPE:
			ValRight.Type = UINTTYPE;
			ValRight.u.uintValue = static_cast<UINT32> (ValRight.u.intValue);
			break;
		case USHORTTYPE:
			ValRight.Type = UINTTYPE;
			ValRight.u.uintValue = static_cast<UINT32> (ValRight.u.ushortValue);
			break;
		default:
			break;
		}
		/*
		ValLeft.Type = UINTTYPE;
		ValRight.Type = UINTTYPE;
		 */
		Value.Type = UINTTYPE;
	}
	else if (ValLeft.Type == INTTYPE || ValRight.Type == INTTYPE)
	{
		switch (ValLeft.Type)
		{
		case INTTYPE:
			ValLeft.Type = INTTYPE;
			ValLeft.u.intValue = static_cast<SINT32> (ValLeft.u.intValue);
			break;
		case SHORTTYPE:
			ValLeft.Type = INTTYPE;
			ValLeft.u.intValue = static_cast<SINT32> (ValLeft.u.shortValue);
			break;
		case USHORTTYPE:
			ValLeft.Type = INTTYPE;
			ValLeft.u.intValue = static_cast<SINT32> (ValLeft.u.ushortValue);
			break;
		default:
			break;
		}
		switch (ValRight.Type)
		{
		case INTTYPE:
			ValRight.Type = UINTTYPE;
			ValRight.u.uintValue = static_cast<SINT32> (ValRight.u.intValue);
			break;
		case SHORTTYPE:
			ValRight.Type = INTTYPE;
			ValRight.u.intValue = static_cast<SINT32> (ValRight.u.shortValue);
			break;
		case USHORTTYPE:
			ValRight.Type = INTTYPE;
			ValRight.u.intValue = static_cast<SINT32> (ValRight.u.ushortValue);
			break;
		default:
			break;
		}
		/* Check this code */
		/*
		ValLeft.Type = INTTYPE;
		ValRight.Type = INTTYPE;
		 */
		Value.Type = INTTYPE;
	}
	else if (ValLeft.Type == USHORTTYPE || ValRight.Type == USHORTTYPE)
	{
		switch (ValLeft.Type)
		{
		case SHORTTYPE:
			if (ValLeft.u.shortValue)
			{
				ValLeft.Type = USHORTTYPE;
				ValLeft.u.ushortValue = static_cast<unsigned short> (ValLeft.u.shortValue);
				Value.Type = USHORTTYPE;
			}
			else
			{
				ValLeft.Type = INTTYPE;
				ValLeft.u.intValue = static_cast<SINT32> (ValLeft.u.shortValue);
				Value.Type = INTTYPE;
			}
			break;
		default:
			break;
		}
		switch (ValRight.Type)
		{
		case SHORTTYPE:
			if (ValRight.u.shortValue)
			{
				ValRight.Type = USHORTTYPE;
				ValRight.u.ushortValue = static_cast<unsigned short> (ValRight.u.shortValue);
				Value.Type = USHORTTYPE;
			}
			else
			{
				ValRight.Type = INTTYPE;
				ValRight.u.intValue = static_cast<SINT32> (ValRight.u.shortValue);
				Value.Type = INTTYPE;
			}
			break;
		default:
			break;
		}
		/* Check this code */
		/*
		ValLeft.Type = USHORTTYPE;
		ValRight.Type = USHORTTYPE;
		 */
		 //Value.Type = USHORTTYPE;
	}
	else if (ValLeft.Type == SHORTTYPE && ValRight.Type == SHORTTYPE)
	{


		/*
		ValLeft.Type = SHORTTYPE;
		ValRight.Type = SHORTTYPE;
		 */

		Value.Type = SHORTTYPE;
	}

}


#if defined (EVALUATION_METHOD_RPN)
/*
Component Function: Variant ComputeArithmeticOnTokens(const SINT8* Op, Variant& ValLeft, Variant& ValRight, Variant& Value)
Arguments:
Returns : Variant
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double.
The requirement for this function to work is that the binary expression tree should be incorrect order.
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed.
To do:
Modify this function to include unary minus detection and compuation on unary minus.
Write code so that unary minus is allowed on operands , watch for unary minus  in ushort, uint or ulong type operands.
Unary minus subtree is folded as a operand for evaluation with other nodes.
Unary minus has either left or right child operand or operator with the other child being nullptr.
 */
Variant ComputeArithmeticOnTokens(const SINT8* Op, Variant& ValLeft, Variant& ValRight, Variant& Value)
{
	UINT32 DataType = NOTADATATYPE;
	//std::string OpString (Op);
	//UINT32 OpType= DetectOpType(OpString);

	UINT32 OpType = DetectOpType(Op);
	/* Type cast types of operands */

	ChangeTypes(ValLeft, ValRight, Value);

	DataType = Value.Type;
	/* Do arithmetic operation on operands */
	#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
	switch (DataType)
	{
	case INTTYPE:
		//std::cout << "\nType returned by Change types is INTTYPE";
		break;
	case UINTTYPE:
		//std::cout << "\nType returned by Change types is UINTTYPE";
		break;
	case FLOATTYPE:
		//std::cout << "\nType returned by Changetypes is FLOATTYPE";
		break;
	case DOUBLETYPE:
		//std::cout << "\nType returned by Changetypes is DOUBLETYPE";
		break;
	case LONGDOUBLETYPE:
		//std::cout << "\nType returned by Changetypes is LONGDOUBLETYPE";
		break;
	case LONGINTTYPE:
		//std::cout << "\nType returned by Changetypes is LONGINTTYPE";
		break;
	case ULONGINTTYPE:
		//std::cout << "\nType returned by Changetypes is Unsigned LONGINTTYPE";
		break;
	case SHORTTYPE:
		//std::cout << "\nType returned by Changetypes is short type";
		break;
	case USHORTTYPE:
		//std::cout << "\nType returned by Changetypes is unsigned short type";
		break;
	default:
		//std::cout << "\nUnknown type in Compute arithmetic on tokens";
		break;
	}
	#endif
	if (DataType == INTTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.intValue = ValLeft.u.intValue + ValRight.u.intValue;
			break;
		case OPMINUS:
			Value.u.intValue = ValLeft.u.intValue - ValRight.u.intValue;
			break;
		case OPMUL:
			Value.u.intValue = ValLeft.u.intValue * ValRight.u.intValue;
			break;
		case OPDIV:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.intValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;

		case OPMODULUS:
			Value.u.intValue = ValLeft.u.intValue % ValRight.u.intValue;
			break;
		case OPUNARYMINUS:
			Value.u.intValue = ValLeft.u.intValue - ValRight.u.intValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.intValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.intValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.intValue = (ValLeft.u.intValue < ValRight.u.intValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.intValue = (ValLeft.u.intValue > ValRight.u.intValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.intValue = (ValLeft.u.intValue <= ValRight.u.intValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.intValue = (ValLeft.u.intValue >= ValRight.u.intValue) ? 1 : 0;
			break;
		case OPLOGICALISEQUALTO:
			Value.u.intValue = (ValLeft.u.intValue == ValRight.u.intValue) ? 1 : 0;
			break;
		case OPLOGICALISNOTEQUALTO:
			Value.u.intValue = (ValLeft.u.intValue != ValRight.u.intValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.intValue = (ValLeft.u.intValue << ValRight.u.intValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.intValue = (ValLeft.u.intValue >> ValRight.u.intValue);
			break;
		case OPBITWISEAND:
			Value.u.intValue = (ValLeft.u.intValue & ValRight.u.intValue);
			break;
		case OPBITWISEOR:
			Value.u.intValue = (ValLeft.u.intValue | ValRight.u.intValue);
			break;
		case OPBITWISEXOR:
			Value.u.intValue = (ValLeft.u.intValue ^ ValRight.u.intValue);
			break;
		case OPLOGICALNOT:
			Value.u.intValue = !(ValLeft.u.intValue);
			break;
		case OPLOGICALAND:
			Value.u.intValue = ((ValLeft.u.intValue) && (ValRight.u.intValue));
			break;
		case OPLOGICALOR:
			Value.u.intValue = ((ValLeft.u.intValue) || (ValRight.u.intValue));
			break;
		case OPCOMPLEMENT:
			Value.u.intValue = ~(ValLeft.u.intValue);
			break;
		/*
		For later
		case OPEQUALTO:
			ValLeft.u.intValue = ValRight.u.intValue;
			Value.u.intValue = ValLeft.u.intValue;
			break;
		*/	
		default:
			break;

		}

#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (Value.Type == INTTYPE)
		{
			//std::cout << "\nType is int type in ComputeArithmetic ";
			//std::cout << std::endl << "Value left " << ValLeft.u.intValue;
			//std::cout << std::endl << "Value right " << ValRight.u.intValue;
			//std::cout << std::endl << "Value " << Value.u.uintValue;
		}
#endif
	}
	else if (DataType == UINTTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.uintValue = ValLeft.u.uintValue + ValRight.u.uintValue;
			break;
		case OPMINUS:
			Value.u.uintValue = ValLeft.u.uintValue - ValRight.u.uintValue;
			break;
		case OPMUL:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.uintValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.uintValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.intValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPMODULUS:
			Value.u.uintValue = ValLeft.u.uintValue % ValRight.u.uintValue;
			break;
		case OPUNARYMINUS:
			Value.u.uintValue = ValLeft.u.uintValue - ValRight.u.uintValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.uintValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;

		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.uintValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.uintValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.uintValue = (ValLeft.u.uintValue < ValRight.u.uintValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.uintValue = (ValLeft.u.uintValue > ValRight.u.uintValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.uintValue = (ValLeft.u.uintValue <= ValRight.u.uintValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.uintValue = (ValLeft.u.uintValue >= ValRight.u.uintValue) ? 1 : 0;
			break;
		case OPLOGICALISEQUALTO:
			Value.u.uintValue = (ValLeft.u.uintValue == ValRight.u.uintValue) ? 1 : 0;
			break;
		case OPLOGICALISNOTEQUALTO:
			Value.u.uintValue = (ValLeft.u.uintValue != ValRight.u.uintValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.uintValue = (ValLeft.u.uintValue << ValRight.u.uintValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.uintValue = (ValLeft.u.uintValue >> ValRight.u.uintValue);
			break;
		case OPBITWISEAND:
			Value.u.uintValue = (ValLeft.u.uintValue & ValRight.u.uintValue);
			break;
		case OPBITWISEOR:
			Value.u.uintValue = (ValLeft.u.uintValue | ValRight.u.uintValue);
			break;
		case OPBITWISEXOR:
			Value.u.uintValue = (ValLeft.u.uintValue ^ ValRight.u.uintValue);
			break;
		case OPLOGICALNOT:
			Value.u.uintValue = !(ValLeft.u.uintValue);
			break;
		case OPLOGICALAND:
			Value.u.uintValue = ((ValLeft.u.uintValue) && (ValRight.u.uintValue));
			break;
		case OPLOGICALOR:
			Value.u.uintValue = ((ValLeft.u.uintValue) || (ValRight.u.uintValue));
			break;
		case OPCOMPLEMENT:
			Value.u.uintValue = ~(ValLeft.u.uintValue);
			break;
		#if defined (COMPLETE_AST)
		case OPEQUALTO:
			ValLeft.u.uintValue = ValRight.u.uintValue;
			Value.u.uintValue = ValLeft.u.uintValue;
			break;
		#endif 
		default:
			break;
		}
		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (Value.Type == UINTTYPE)
		{
			//std::cout << "\nType is uint type in ComputeArithmetic ";
			//std::cout << std::endl << "Value left " << ValLeft.u.uintValue;
			//std::cout << std::endl << "Value right " << ValRight.u.uintValue;
			//std::cout << std::endl << "Value " << Value.u.uintValue;
		}
		#endif	
	}
	else if (DataType == SHORTTYPE)
	{

		switch (OpType)
		{
		case OPPLUS:
			Value.u.shortValue = ValLeft.u.shortValue + ValRight.u.shortValue;
			break;
		case OPMINUS:
			Value.u.shortValue = ValLeft.u.shortValue - ValRight.u.shortValue;
			break;
		case OPMUL:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.shortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
			break;
		case OPDIV:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.shortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPMODULUS:
			Value.u.shortValue = ValLeft.u.shortValue % ValRight.u.shortValue;
			break;
		case OPUNARYMINUS:
			Value.u.shortValue = ValLeft.u.shortValue - ValRight.u.shortValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.shortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;

		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;

		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.shortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.shortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.shortValue = (ValLeft.u.shortValue < ValRight.u.shortValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.shortValue = (ValLeft.u.shortValue > ValRight.u.shortValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.shortValue = (ValLeft.u.shortValue <= ValRight.u.shortValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.shortValue = (ValLeft.u.shortValue >= ValRight.u.shortValue) ? 1 : 0;
			break;
		case OPLOGICALISEQUALTO:
			Value.u.shortValue = (ValLeft.u.shortValue == ValRight.u.shortValue) ? 1 : 0;
			break;
		case OPLOGICALISNOTEQUALTO:
			Value.u.shortValue = (ValLeft.u.shortValue != ValRight.u.shortValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.shortValue = (ValLeft.u.shortValue << ValRight.u.shortValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.shortValue = (ValLeft.u.shortValue >> ValRight.u.shortValue);
			break;
		case OPBITWISEAND:
			Value.u.shortValue = (ValLeft.u.shortValue & ValRight.u.shortValue);
			break;
		case OPBITWISEOR:
			Value.u.shortValue = (ValLeft.u.shortValue | ValRight.u.shortValue);
			break;
		case OPBITWISEXOR:
			Value.u.shortValue = (ValLeft.u.shortValue ^ ValRight.u.shortValue);
			break;
		case OPLOGICALNOT:
			Value.u.shortValue = !(ValLeft.u.shortValue);
			break;
		case OPLOGICALAND:
			Value.u.shortValue = ((ValLeft.u.shortValue) && (ValRight.u.shortValue));
			break;
		case OPLOGICALOR:
			Value.u.shortValue = ((ValLeft.u.shortValue) || (ValRight.u.shortValue));
			break;
		case OPCOMPLEMENT:
			Value.u.shortValue = ~(ValLeft.u.shortValue);
			break;
		/* For later	

		case OPEQUALTO:
			ValLeft.u.shortalue = ValRight.u.shortValue;
			Value.u.shortValue = ValLeft.u.shortValue;
			break;
		*/	

		default:
			break;
		}
		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (Value.Type == SHORTTYPE)
		{
			//std::cout << "\nType is short type in ComputeArithmetic ";
			//std::cout << std::endl << "Value left " << ValLeft.u.shortValue;
			//std::cout << std::endl << "Value right " << ValRight.u.shortValue;
			//std::cout << std::endl << "Value " << Value.u.shortValue;
		}
		#endif

	}
	else if (DataType == USHORTTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.ushortValue = ValLeft.u.ushortValue + ValRight.u.ushortValue;
			break;
		case OPMINUS:
			Value.u.ushortValue = ValLeft.u.ushortValue - ValRight.u.ushortValue;
			break;
		case OPMUL:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ushortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ushortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPMODULUS:
			Value.u.ushortValue = ValLeft.u.ushortValue % ValRight.u.ushortValue;
			break;
		case OPUNARYMINUS:
			Value.u.ushortValue = ValLeft.u.ushortValue - ValRight.u.ushortValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ushortValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;

		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;

		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ushortValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ushortValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.ushortValue = (ValLeft.u.ushortValue < ValRight.u.ushortValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.ushortValue = (ValLeft.u.ushortValue > ValRight.u.ushortValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.ushortValue = (ValLeft.u.ushortValue <= ValRight.u.ushortValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.ushortValue = (ValLeft.u.ushortValue >= ValRight.u.ushortValue) ? 1 : 0;
			break;
		case OPLOGICALISEQUALTO:
			Value.u.ushortValue = (ValLeft.u.ushortValue == ValRight.u.ushortValue) ? 1 : 0;
			break;
		case OPLOGICALISNOTEQUALTO:
			Value.u.ushortValue = (ValLeft.u.ushortValue != ValRight.u.ushortValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.ushortValue = (ValLeft.u.ushortValue << ValRight.u.ushortValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.ushortValue = (ValLeft.u.ushortValue >> ValRight.u.ushortValue);
			break;
		case OPBITWISEAND:
			Value.u.ushortValue = (ValLeft.u.ushortValue & ValRight.u.ushortValue);
			break;
		case OPBITWISEOR:
			Value.u.ushortValue = (ValLeft.u.ushortValue | ValRight.u.ushortValue);
			break;
		case OPBITWISEXOR:
			Value.u.ushortValue = (ValLeft.u.ushortValue ^ ValRight.u.ushortValue);
			break;
		case OPLOGICALNOT:
			Value.u.ushortValue = !(ValLeft.u.ushortValue);
			break;
		case OPLOGICALAND:
			Value.u.ushortValue = ((ValLeft.u.ushortValue) && (ValRight.u.ushortValue));
			break;
		case OPLOGICALOR:
			Value.u.ushortValue = ((ValLeft.u.ushortValue) || (ValRight.u.ushortValue));
			break;
		case OPCOMPLEMENT:
			Value.u.ushortValue = ~(ValLeft.u.ushortValue);
			break;
		/* 
		For later
		case OPEQUALTO:
			ValLeft.u.ushortalue = ValRight.u.ushortValue;
			Value.u.ushortValue = ValLeft.u.ushortValue;
			break;
		*/	
		default:
			break;
		}
		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (Value.Type == USHORTTYPE)
		{
			//std::cout << "\nType is short type in ComputeArithmetic ";
			//std::cout << std::endl << "Value left " << ValLeft.u.ushortValue;
			//std::cout << std::endl << "Value right " << ValRight.u.ushortValue;
			//std::cout << std::endl << "Value " << Value.u.ushortValue;
		}
		#endif

	}
	else if (DataType == FLOATTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.floatValue = ValLeft.u.floatValue + ValRight.u.floatValue;
			break;
		case OPMINUS:
			Value.u.floatValue = ValLeft.u.floatValue - ValRight.u.floatValue;
			break;
		case OPMUL:
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPMODULUS:
			ErrorTracer(MODULUS_OPERATOR_ON_FLOAT_NUMBERS, 0, 0);
			Value.u.floatValue = 0;
			break;
		case OPUNARYMINUS:
			Value.u.floatValue = ValLeft.u.floatValue - ValRight.u.floatValue;
			break;
		case OPPOWEXPONENT:
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
			break;
		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.floatValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.floatValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
        /* 

		case OPEQUALTO:
			ValLeft.u.floatValue = ValRight.u.floatValue;
			Value.u.floatValue = ValLeft.u.floatValue;
			break;
		*/	
		default:
			break;
		}
		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (DataType == FLOATTYPE)
		{
			//std::cout << "\nType is float type in ComputeArithmetic ";
			//std::cout << std::endl << "Value left " << ValLeft.u.floatValue;
			//std::cout << std::endl << "Value right " << ValRight.u.floatValue;
			//std::cout << std::endl << "Value " << Value.u.floatValue;
		}
		#endif

	}
	else if (DataType == DOUBLETYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.doubleValue = ValLeft.u.doubleValue + ValRight.u.doubleValue;
			break;
		case OPMINUS:
			Value.u.doubleValue = ValLeft.u.doubleValue - ValRight.u.doubleValue;
			break;
		case OPMUL:
			Value.u.doubleValue = ValLeft.u.doubleValue * ValRight.u.doubleValue;
			break;
		case OPDIV:
			Value.u.doubleValue = ValLeft.u.doubleValue / ValRight.u.doubleValue;
			break;
		case OPMODULUS:
			ErrorTracer(MODULUS_OPERATOR_ON_FLOAT_NUMBERS, 0, 0);
			Value.u.doubleValue = 0;
			break;
		case OPUNARYMINUS:
			Value.u.doubleValue = ValLeft.u.doubleValue - ValRight.u.doubleValue;
			break;
		case OPPOWEXPONENT:
			Value.u.doubleValue = pow(ValLeft.u.doubleValue, ValRight.u.doubleValue);
			break;
		case OPSIN:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
		case OPCOSEC:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
        /* For later  
		case OPEQUALTO:
			ValLeft.u.doubleValue = ValRight.u.doubleValue;
			Value.u.doubleValue = ValLeft.u.doubleValue;
			break;
        */ 
		default:
			break;
		}
		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (DataType == DOUBLETYPE)
		{
			//std::cout << "\nType is double type in ComputeArithmetic ";
			//std::cout << std::endl << "Value left " << ValLeft.u.doubleValue;
			//std::cout << std::endl << "Value right " << ValRight.u.doubleValue;
			//std::cout << std::endl << "Value " << Value.u.doubleValue;
		}
		#endif
	}
	else if (DataType == LONGINTTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.longValue = ValLeft.u.longValue + ValRight.u.longValue;
			break;
		case OPMINUS:
			Value.u.longValue = ValLeft.u.longValue - ValRight.u.longValue;
			break;
		case OPMUL:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.longValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
			break;
		case OPDIV:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.longValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
		case OPMODULUS:
			Value.u.longValue = ValLeft.u.longValue % ValRight.u.longValue;
			break;
		case OPUNARYMINUS:
			Value.u.longValue = ValLeft.u.longValue - ValRight.u.longValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.longValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;

		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;

		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.longValue = (ValLeft.u.longValue < ValRight.u.longValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.longValue = (ValLeft.u.longValue > ValRight.u.longValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.longValue = (ValLeft.u.longValue <= ValRight.u.longValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.longValue = (ValLeft.u.longValue >= ValRight.u.longValue) ? 1 : 0;
			break;
		case OPLOGICALISEQUALTO:
			Value.u.longValue = (ValLeft.u.longValue == ValRight.u.longValue) ? 1 : 0;
			break;
		case OPLOGICALISNOTEQUALTO:
			Value.u.longValue = (ValLeft.u.longValue != ValRight.u.longValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.longValue = (ValLeft.u.longValue << ValRight.u.longValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.longValue = (ValLeft.u.longValue >> ValRight.u.longValue);
			break;
		case OPBITWISEAND:
			Value.u.longValue = (ValLeft.u.longValue & ValRight.u.longValue);
			break;
		case OPBITWISEOR:
			Value.u.longValue = (ValLeft.u.longValue | ValRight.u.longValue);
			break;
		case OPBITWISEXOR:
			Value.u.longValue = (ValLeft.u.longValue ^ ValRight.u.longValue);
			break;
		case OPLOGICALNOT:
			Value.u.longValue = !(ValLeft.u.longValue);
			break;
		case OPLOGICALAND:
			Value.u.longValue = ((ValLeft.u.longValue) && (ValRight.u.longValue));
			break;
		case OPLOGICALOR:
			Value.u.longValue = ((ValLeft.u.longValue) || (ValRight.u.longValue));
			break;
		case OPCOMPLEMENT:
			Value.u.longValue = ~(ValLeft.u.longValue);
			break;
        /* For later 
		case OPEQUALTO:
			ValLeft.u.longValue = ValRight.u.longValue;
			Value.u.longValue = ValLeft.u.longValue;
			break;
		*/
		default:
			break;
		}
		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (DataType == LONGINTTYPE)
		{
			std::cout << "\nType is double type in ComputeArithmetic ";
			std::cout << std::endl << "Value " << Value.u.longValue;
			std::cout << std::endl << "Value left " << ValLeft.u.longValue;
			std::cout << std::endl << "Value right " << ValRight.u.longValue;
		}
		#endif
	}
	else if (DataType == ULONGINTTYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.ulongValue = ValLeft.u.ulongValue + ValRight.u.ulongValue;
			break;
		case OPMINUS:
			Value.u.ulongValue = ValLeft.u.ulongValue - ValRight.u.ulongValue;
			break;
		case OPMUL:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ulongValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue * ValRight.u.floatValue;
			break;
		case OPDIV:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ulongValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = ValLeft.u.floatValue / ValRight.u.floatValue;
			break;
		case OPMODULUS:
			Value.u.ulongValue = ValLeft.u.ulongValue % ValRight.u.ulongValue;
			break;
		case OPUNARYMINUS:
			Value.u.ulongValue = ValLeft.u.ulongValue - ValRight.u.ulongValue;
			break;
		case OPPOWEXPONENT:
			ValLeft.Type = FLOATTYPE;
			ValLeft.u.floatValue = static_cast<FLOAT32> (ValLeft.u.ulongValue);
			ValRight.Type = FLOATTYPE;
			ValRight.u.floatValue = static_cast<FLOAT32> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.floatValue = pow(ValLeft.u.floatValue, ValRight.u.floatValue);
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.ulongValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.ulongValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		case OPLESSTHAN:
			Value.u.ulongValue = (ValLeft.u.ulongValue < ValRight.u.ulongValue) ? 1 : 0;
			break;
		case OPGREATERTHAN:
			Value.u.ulongValue = (ValLeft.u.ulongValue > ValRight.u.ulongValue) ? 1 : 0;
			break;
		case OPLESSTHANEQUALTO:
			Value.u.ulongValue = (ValLeft.u.ulongValue <= ValRight.u.ulongValue) ? 1 : 0;
			break;
		case OPGREATERTHANEQUALTO:
			Value.u.ulongValue = (ValLeft.u.ulongValue >= ValRight.u.ulongValue) ? 1 : 0;
			break;
		case OPLOGICALISEQUALTO:
			Value.u.ulongValue = (ValLeft.u.ulongValue == ValRight.u.ulongValue) ? 1 : 0;
			break;
		case OPLOGICALISNOTEQUALTO:
			Value.u.ulongValue = (ValLeft.u.ulongValue != ValRight.u.ulongValue) ? 1 : 0;
			break;
		case OPLEFTBITSHIFT:
			Value.u.ulongValue = (ValLeft.u.ulongValue << ValRight.u.ulongValue);
			break;
		case OPRIGHTBITSHIFT:
			Value.u.ulongValue = (ValLeft.u.ulongValue >> ValRight.u.ulongValue);
			break;
		case OPBITWISEAND:
			Value.u.ulongValue = (ValLeft.u.ulongValue & ValRight.u.ulongValue);
			break;
		case OPBITWISEOR:
			Value.u.ulongValue = (ValLeft.u.ulongValue | ValRight.u.ulongValue);
			break;
		case OPBITWISEXOR:
			Value.u.ulongValue = (ValLeft.u.ulongValue ^ ValRight.u.ulongValue);
			break;
		case OPLOGICALNOT:
			Value.u.ulongValue = !(ValLeft.u.ulongValue);
			break;
		case OPLOGICALAND:
			Value.u.ulongValue = ((ValLeft.u.ulongValue) && (ValRight.u.ulongValue));
			break;
		case OPLOGICALOR:
			Value.u.ulongValue = ((ValLeft.u.ulongValue) || (ValRight.u.ulongValue));
			break;
		case OPCOMPLEMENT:
			Value.u.ulongValue = ~(ValLeft.u.ulongValue);
			break;
		/* 
		For later
		case OPEQUALTO:
			ValLeft.u.ulongValue = ValRight.u.ulongValue;
			Value.u.ulongValue = ValLeft.u.ulongValue;
			break;
		*/	
		default:
			break;
		}

		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (DataType == ULONGINTTYPE)
		{
			//std::cout << "\nType is double type in ComputeArithmetic ";
			//std::cout << std::endl << "Value " << Value.u.ulongValue;
			//std::cout << std::endl << "Value left " << ValLeft.u.ulongValue;
			//std::cout << std::endl << "Value right " << ValRight.u.ulongValue;
		}
		#endif
	}
	else if (DataType == LONGDOUBLETYPE)
	{
		switch (OpType)
		{
		case OPPLUS:
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue + ValRight.u.longdoubleValue;
			break;
		case OPMINUS:
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue - ValRight.u.longdoubleValue;
			break;
		case OPMUL:
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue * ValRight.u.longdoubleValue;
			break;
		case OPDIV:
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue / ValRight.u.longdoubleValue;
			break;
		case OPMODULUS:
			ErrorTracer(MODULUS_OPERATOR_ON_FLOAT_NUMBERS, 0, 0);
			Value.u.longdoubleValue = 0;
			break;
		case OPUNARYMINUS:
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue - ValRight.u.longdoubleValue;
			break;
		case OPPOWEXPONENT:
			Value.u.longdoubleValue = pow(ValLeft.u.longdoubleValue, ValRight.u.longdoubleValue);
			break;
		case OPSIN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = sin(ValLeft.u.doubleValue);
			break;
		case OPCOS:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = cos(ValLeft.u.doubleValue);
			break;
		case OPTAN:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = tan(ValLeft.u.doubleValue);
			break;
		case OPSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / cos(ValLeft.u.doubleValue);
			break;
		case OPCOSEC:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / sin(ValLeft.u.doubleValue);
			break;
		case OPCOT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			ValLeft.u.doubleValue = ValLeft.u.doubleValue * PI / 180;
			Value.u.doubleValue = 1 / tan(ValLeft.u.doubleValue);
			break;
		case OPSQRT:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = sqrt(ValLeft.u.doubleValue);
			break;
		case OPLOG:
			ValLeft.Type = DOUBLETYPE;
			ValLeft.u.doubleValue = static_cast<FLOAT64> (ValLeft.u.longdoubleValue);
			ValRight.Type = DOUBLETYPE;
			ValRight.u.doubleValue = static_cast<FLOAT64> (ValRight.u.longdoubleValue);
			ChangeTypes(ValLeft, ValRight, Value);
			Value.u.doubleValue = log(ValLeft.u.doubleValue);
			break;
		/*	
		for later

		case OPEQUALTO:
			ValLeft.u.longdoubleValue = ValRight.u.longdoubleValue;
			Value.u.longdoubleValue = ValLeft.u.longdoubleValue;
			break;
		*/	
		default:
			break;

		}

		#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
		/* Debugging code */
		if (DataType == LONGDOUBLETYPE)
		{
			//std::cout << "\nType is double type in ComputeArithmetic ";
			//std::cout << std::endl << "Value " << Value.u.longdoubleValue;
			//std::cout << std::endl << "Value left " << ValLeft.u.longdoubleValue;
			//std::cout << std::endl << "Value right " << ValRight.u.longdoubleValue;
		}
		#endif
	}
	return Value;
}

/*
Component Function: void GetResult(Token_tag &ResultToken,Variant Value)
Arguments: Token, Token type with data.
Returns :
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double.
A variant is used to return a data type which is calculated at runtime.
To do:
*/
void GetResult(Token_tag &ResultToken, Variant Value)
{
	switch (Value.Type)
	{
	case INTTYPE:
		ResultToken.TokenDataType = INTTYPE;
		ResultToken.Data.u.intValue = Value.u.intValue;
		ResultToken.Meta = "INTEGER";
		break;
	case UINTTYPE:
		ResultToken.TokenDataType = UINTTYPE;
		ResultToken.Data.u.uintValue = Value.u.uintValue;
		ResultToken.Meta = "UNSIGNEDINTEGER";
		break;
	case FLOATTYPE:
		ResultToken.TokenDataType = FLOATTYPE;
		ResultToken.Data.u.floatValue = Value.u.floatValue;
		ResultToken.Meta = "FLOAT";
		break;
	case DOUBLETYPE:
		ResultToken.TokenDataType = DOUBLETYPE;
		ResultToken.Data.u.doubleValue = Value.u.doubleValue;
		ResultToken.Meta = "DOUBLE";
		break;
	case LONGDOUBLETYPE:
		ResultToken.TokenDataType = LONGDOUBLETYPE;
		ResultToken.Data = Value;
		ResultToken.Data.u.longdoubleValue = Value.u.longdoubleValue;
		ResultToken.Meta = "LONGDOUBLE";
		break;
	case LONGINTTYPE:
		ResultToken.TokenDataType = LONGINTTYPE;
		ResultToken.Data.u.longValue = Value.u.longValue;
		ResultToken.Meta = "LONGINTEGER";
		break;
	case ULONGINTTYPE:
		ResultToken.TokenDataType = ULONGINTTYPE;
		ResultToken.Data.u.ulongValue = Value.u.ulongValue;
		ResultToken.Meta = "UNSIGNEDLONGINTEGER";
		break;
	case SHORTTYPE:
		ResultToken.TokenDataType = SHORTTYPE;
		ResultToken.Data.u.shortValue = Value.u.shortValue;
		ResultToken.Meta = "SHORTINTEGER";
		break;
	case USHORTTYPE:
		ResultToken.TokenDataType = USHORTTYPE;
		ResultToken.Data.u.ushortValue = Value.u.ushortValue;
		ResultToken.Meta = "UNsigned shortINTEGER";
		break;
	default:
		break;
	}
}
#endif // #if defined (EVALUATION_METHOD_RPN)
