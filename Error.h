/*
Name :  Error.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 20-06-2016
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.


Description:
This module produces errors when called from different modules. These errors would be reported
to the Interpreter control program.
Notes:

Bug and revisioin history;
        Version 0.1 Initial version
                
*/

#ifndef ERROR_H
#define ERROR_H

#include <string>
#include <vector>
#include <iostream>
#include "std_types.h"
#include "Datastructures.h"

// Error codes for various syntax errors.
#define MISSING_COMMENT 0x01
#define INCORRECT_DATA_DEFINITION 0x02
#define INCORRECT_DATA_DECLARATION 0x03
#define MISSING_FUNCTION_ARGS 0x04
#define INCORRECT_FUNCTION_DEFINITION 0x05
#define ILLEGAL_CHARACTER 0x06
#define MISSING_LEFT_PARENTHESIS 0x07
#define MISSING_RIGHT_PARENTHESIS 0x08
#define INVALID_IDENT_KEYWORD_USED 0x09
#define STACK_OVERFLOW 0x0A
#define MEMORY_ALLOCATION_ERROR 11
#define NUMBER_OUT_OF_RANGE 12
#define UNABLE_TO_PROCESS_LINENUMBER 13
#define MISSING_SLASH 14
#define DEALLOCATING_MEMORY_FOR_DELETED_NODE 15
#define CONSECUTIVE_MULTIPLICATION_OPERATORS 16
#define CONSECUTIVE_DIVIDE_OPERATORS 17
#define INCORRECT_ARITHMETIC 18
#define STACK_ACCESS_ERROR 19
#define MISSING_OPERAND 20
#define MISSING_OPERATOR 21
#define EMPTY_PARENTHESES 22
#define EMPTY_EXPRESSION 23
#define CONSECUTIVE_MODULUS_OPERATORS 24
#define MODULUS_OPERATOR_ON_FLOAT_NUMBERS 25
#define CONSECUTIVE_EXPONENT_OPERATORS 26
#define BITWISEARITHMETIC_ON_FLOATINGPOINTNUMBERS 27
#define MACRO_DEFINITION_DECLARATION_DONT_MATCH 28
#define MACRO_DEFINITION_EXPRESSION_DONT_MATCH 29
#define MACRO_MISSING_IDENTS 30
#define MACRO_MISSING_OPERANDS 31
#define MACRO_MISSING_DEFINITION 32
#define MACRO_MISSING_DECLARATION 33

// Other error codes
#define EVALUATION_ERRORS 0x1
#define NO_ERRORS 0x0

/*
Component Function: void ErrorTracer ( UINT32 ErrorCode, UINT32 LineNumber,UINT32 ColumnNumber)
Arguments:  Error from module string and Error code 
returns: None
Description:
Adds Error from module with error code to a vector of strings which contains total errors ever captured.
Version : 0.2
*/
void ErrorTracer ( UINT32 ErrorCode, UINT32 LineNumber,UINT32 ColumnNumber);

/*
Component Function: UINT32 CheckforErrors(void)
Arguments:  None
returns: None
Description:
Checks if errors are present and returns errors present or not present.
Version : 0.1
 */
UINT32 CheckforErrors(void);
/*
Component Function: void PrintErrors(void)
Arguments:  None
returns: None
Description:
Displays errors present on a console.
Version : 0.1
 */
void PrintErrors(void);

/*
Component Function:  void InitErrorTracer(void)
Arguments:  None
returns: None
Description:
Initialize Error tracer module.
Version : 0.1
*/
void InitErrorTracer(void);
#endif
