/*
Name :  Output.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 20-06-2016
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
This module prints output 
Notes:
Bug and Version history;
0.1 Initial version
         
*/

#ifndef OUTPUT_H
#define OUTPUT_H

#include <vector>
#include "std_types.h"
#include <iostream>

/*
 This function should be commented in the main project.
 Diplay output with line numbers.
*/  
void DisplayOutput(const std::vector <UINT8> & s);

#endif
