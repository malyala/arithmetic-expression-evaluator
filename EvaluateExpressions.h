/*
Name :  MacroProcessor.h
Author: Amit Malyala ,Copyright Amit Malyala 2016, All rights reserved.
Date : 14-11-16 02:08

License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
This C++ project is for processing of arithmetic expressions.
*/

#ifndef EVALUATE_EXPRESSION_H
#define EVALUATE_EXPRESSION_H

#include "Lexer.h"

// Compiler switch to select Binaryexpression tree evaluation or RPN stack evaluation at compile time.

//#define EVALUATION_METHOD_EXPRESSIONTREE 0x01

#define EVALUATION_METHOD_RPN        0x02

/* To print messages during execution*/

//#define DEVELOPMENT_DEBUGGING_ENABLED 0x01


/* For enabling test framework */
//#define TESTFRAMEWORK_ENABLED
#if defined (EVALUATION_METHOD_EXPRESSIONTREE)
/*
Component Function: void EvaluateExpression(void)
Arguments: None
Returns : None
Description:
This function evaluates a Tree.
Version and bug history:
0.1 Initial version.
Notes:
 */
void EvaluateExpression(void);
#endif  // if defined (EVALUATION_METHOD_EXPRESSIONTREE)

#if defined (EVALUATION_METHOD_RPN)

/*
Component Function: void InitMacroProcessor(void)
Arguments: None
Returns : None
Description:
Intialize macro processor by initializing underlying data structures.
Version and bug history:
0.1 Initial version.
Notes:

*/

/*
Component Function: void EvaluatePostfixExpression(void)
Arguments: None
Returns : None
Description:
This function evaluates a Tree.
Version and bug history:
0.1 Initial version.
Notes: 
Function being designed.
 */
void EvaluatePostfixExpression(void);

#endif //#if defined (EVALUATION_METHOD_RPN)

#endif
