/*
Name :  Lexer.h
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module creates lexer tokens.
Notes:
Bug and revision history;
0.1 Initial version
*/
#ifndef LEXER_H
#define LEXER_H

#include "std_types.h"
#include <cstddef>
#include "DataStructures.h"
#include "io.h"
#include "symboltable.h"


/* Type Numeric Tokens */
#define LONGDOUBLETYPE 0
#define DOUBLETYPE 1
#define FLOATTYPE 2
#define UINTTYPE 3
#define INTTYPE 4
#define LONGINTTYPE 5
#define LONGLONGINTTYPE 6
#define ULONGINTTYPE 7
#define SHORTTYPE 8
#define USHORTTYPE 9
#define UCHARTYPE 10
#define CHARTYPE 11
#define BOOLTYPE 12
#define CSTRINGTYPE 13
#define NOTADATATYPE 14



/* Other tokens */
#define HASHTYPE 15
#define LEFTPARENTHESIS 16
#define RIGHTPARENTHESIS 17
#define ENDOFINPUT 18
#define NOT_A_NUMBER 19
#define NOTOP 20
#define NEWLINETYPE 21
#define IDENT 22
#define NOTSTRING 34

/* List of operators */
#define OPPLUS 250
#define OPMINUS 251
#define OPMUL 252
#define OPDIV 253
#define OPMODULUS 254
#define OPEQUALTO 255
#define OPUNARYMINUS 256
#define OPUNARYPLUS 257
#define OPPOWEXPONENT 258
#define OPSIN 259
#define OPCOS 260
#define OPTAN 261
#define OPCOSEC 262
#define OPSEC 263
#define OPCOT 264
#define OPSQRT 265
#define OPLOG 266
#define OPLESSTHAN 267
#define OPGREATERTHAN 268
#define OPLEFTBITSHIFT 269
#define OPRIGHTBITSHIFT 270
#define OPLOGICALNOT 271
#define OPBITWISEOR 272
#define OPBITWISEAND 273
#define OPLOGICALAND 274
#define OPLOGICALOR 275
#define OPLESSTHANEQUALTO 276
#define OPGREATERTHANEQUALTO 277
#define OPLOGICALISEQUALTO 278
#define OPLOGICALISNOTEQUALTO 279
#define OPBITWISEXOR 280
#define OPCOMPLEMENT 281
#define OPADDEQUALTO 282
#define OPMINUSEQUALTO 283
#define OPMODEQUALTO 284
#define OPDIVEQUALTO 285
#define OPCOMPLEMENTEQUALTO 286
#define OPMULTIPLYEQUALTO 287
#define OPANDEQUALTO 288
#define OPOREQUALTO 289
#define OPLEFTSHIFTEQUALTO 290
#define OPRIGHTSHIFTEQUALTO 291

/*
define constants for another way of definining token types.
#define Token_identifier 1
#define Token_keyword 2
#define Token_separator 3
#define Token_operator 4
#define Token_literal 5
#define Token_comment 6
#define Token_other 7   
*/

/* Lexer functions and token types */
namespace Lexer
{

/* Token types */
enum class TokenType
{
	identifier,
	separator_leftParen,
	separator_rightParen,
	op,
	numericliteral,
	command,
	endofline,
	error,
	Emptytoken
	
};

	
/* Token definition */
typedef struct Token_tag
{
	SCHAR* name;
	std::size_t LineNumber;
	std::size_t ColumnNumber;
	TokenType Type;
	Variant Data;
} Token_tag;


/*	
Component function: Token_tag getNextToken(void)
Arguments:  None
Returns: Token
Description:  returns a token
Version : 0.1
*/
Token_tag getNextToken(void);

 /* Peek next char in input */
 
/*
Component function: SCHAR PeekNextChar(SCHAR* str, UINT32 Length, UINT32 index)
Arguments:  char string, length, current index.
Returns: next char of index.
Description:  returns a char
Version : 0.1
*/
 
SCHAR PeekNextChar(SCHAR* str, UINT32 Length, UINT32 index);


/* Skip whitespace */
void SkipWhitespace(SCHAR* str, UINT32 Length, UINT32& i);


/*
 Component Function: BOOL IsSeparatorr(SCHAR Character)
 Arguments:  Character to be searched in separators
 Returns:
 Description:
 Checks if a char is a separator.
  Version : 0.1
 */
BOOL IsSeparator(SCHAR Character);
 /*
 Component Function: BOOL isOperchar(SCHAR Ch)
 Arguments:  character
 Returns:
 Description:
 returns char is a operator or not 
  Version : 0.1
 */
BOOL isOperchar(SCHAR Ch);

/*
 Component Function: BOOL  isIdent(const SCHAR* str)
 Arguments:  String characters to be searched in list of alphabets and _
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z and digits
 Returns Alphabetic or not
 Optional: Write a regex algorithm to detect alphabetical characters in a string.
 Version : 0.1
 */
BOOL isIdent(const SCHAR* str);
/*
 Component Function: BOOL IsIdentorNumericchar(SCHAR Character)
 Arguments:  Character after ( 
 Returns:
 Description:
 Checks if a char is ident or numeric char.
  Version : 0.1
 */
BOOL IsIdentorNumericchar(SCHAR Character);

/*
 Component Function: BOOL isIdentChar(SCHAR Character)
 Arguments:  Character to be searched in list of alphabets and characters
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z
 Returns Alphabetic or not
  Version : 0.1
 */
BOOL isIdentChar(SCHAR Character);
/*
 Component function: SCHAR* getTokencharstring(void)
 Arguments: char string
 returns : a substring of type char*
 Version : 0.1
 Description:
 parses a char string into substrings.
 Each substring canbe these:
 ident,
 numeric,
 operator 
 parenthesis ( )
 These substrings would be read by Tokenprocessor() function.
 
 Notes:
 Function being edited
 
 */
 SCHAR* getTokencharstring(void);
/*
Component function: SCHAR PeekNextChar(SCHAR* str, UINT32 Length, UINT32 index)
Arguments:  char string, length, current index.
Returns: next char of index.
Description:  returns a char
Version : 0.1
*/
SCHAR PeekNextChar(SCHAR* str, UINT32 Length, UINT32 index);


/* define a char for end of input. */    
constexpr SCHAR ENDOFINPUTSTRING = '#'; 



/*
 Component Function: BOOL isWhitespace(SCHAR Character)
 Arguments:  Character to be checked
 Returns:
 Description:
 This function checks if a character is whitespace or not 
  Version : 0.1
 */
BOOL isWhitespace(SCHAR Character);


/*
 Component Function: BOOL isSeparator(SCHAR Character)
 Arguments:  Character to be searched in separators
 Returns:
 Description:
 Checks if a char is a separator.
  Version : 0.1
 */
BOOL isSeparator(SCHAR Character);


/*
 Component function: void TokenProcessor(Lexer::Token_tag& CurrentToken)
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns current Token after classifying it from a stream of tokens created on the fly. 
 Each token can be these:
 ident,
 numeric,
 operator 
 Separator- parenthesis ( ) or ' '
 Newline \n
 Notes:
 Function being edited
 */
void TokenProcessor(Lexer::Token_tag& CurrentToken);

/*
 Component Function: BOOL isOperator(const SCHAR* str)
 Arguments:  string to be checked.
 Returns:
 Description: This function checks if char string is a operator. 
 Returns ident or not
 Version : 0.1
 */
BOOL isOperator(const SCHAR* str);
/*
 Component Function: UINT32 DetectOpType(SCHAR* OpString)
 Arguments:  char string
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
UINT32 DetectOpType(SCHAR* OpString);



/*
 Component Function: UINT32 DetectOpToken(Lexer::Token_tag& OpToken)
 Arguments:  Token
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
UINT32 DetectOpToken(Lexer::Token_tag& OpToken);
/*
 Component Function: BOOL isValidOp(UINT32 OpType)
 Arguments:  Op type
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
BOOL isValidOp(UINT32 OpType);

/*
 Component Function: void PrintToken(const Lexer::Token_tag& Token)
 Arguments: 
 Returns:
 Description:
  Prints a token.
  Version : 0.1
 */
void PrintToken(const Lexer::Token_tag& Token);


/*
 Component Function: void ClearToken(Lexer::Token_tag& Token)
 Arguments: 
 Returns:
 Description:
  Prints a token.
  Version : 0.1
 */
void ClearToken(Lexer::Token_tag& Token);

} 

/* Tree node for construction of expression tree */
class Tnode
{
	public:
	Lexer::Token_tag Token;
    
	Tnode *pNext;
	Tnode *pPrevious;	
};
 
/*
Component function: void InitTokenList(void)
Arguments:  None 
Returns: NOne
Description:  
Initializes Token list. 
Version : 0.1
*/
void InitTokenList(void);


/*
Component function: void AddNode(Tnode *pNode)
Arguments:  A node of type Token node. 
Returns: NOne
Description:  
adds a node.
Version : 0.1
*/
void AddNode(Tnode *pNode);

/*
Component function: void DeleteList(void)
Arguments:  None.
Returns: None
Description:  
Deletes entire list excpet pHead and psentinel.
Version : 0.1
*/
void DeleteList(void);

#endif // #ifndef LEXER_H