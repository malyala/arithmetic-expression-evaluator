/*
 Name :  DataStructures.cpp
 Author: Amit Malyala 
 License:
 Copyright <2017> <Amit Malyala>

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
  Description:

 This module has API functions which are used by Lexer, Parser and Binary tree and evaluator modules.
 Notes:
 To do:
 
 Bug and version history:
 0.01 Initial version
 0.02 Added InitVariant() function
 0.03 Rewrote atofloat and atodouble functions

 Known issues:
 None

 Unknown issues:
 
 typedef signed short SINT16; gives a SINT16 does not name a type compilation error with option std=C++11.
 When the type SINT16 is used with std=c++14 , there are no errors.
*/

#include "DataStructures.h"
#include <cmath>
#include <iomanip>
#include <stdio.h>

/*
 Component Function: static UINT32 getNumofDecDigit(UINT32 Number)
 Arguments:  None
 returns: Number of digits
 Description:
 Returns Number of digits in a UINT32eger
 Version : 0.1
 */

/*
 Component Function: static BOOL IsDigit(SINT8 Digit)
 Arguments: Digit
 returns: Digit or not
 Description:
 Returns if a char is a digit
 Version : 0.1
*/
static BOOL IsDigit(SINT8 Digit);

/*
 Component static inline UINT32 getNumofDecDigit(UINT32 Number);
 Arguments: Number
 returns : Number of decimal digits
 Version : 0.1
 Description:
 Returns number of decimal digts
 */

static inline UINT32 getNumofDecDigit(UINT32 Number);
/*
 Component static SINT32 strlength(const SINT8 *str);
 Arguments: char string
 returns : decimal digit
 Version : 0.1
 Description:
 Returns length of a char string.
 */
static SINT32 strlength(const SINT8 *str);
/*
 Component static UINT32 getDecDigit(const UINT8& str)
 Arguments: Chat digit
 returns : decimal digit
 Version : 0.1
 Description:
 Converts a char to decimal digit.
 */
static UINT32 getDecDigit(const UINT8& str);
/*
 Component Function: static UINT8 getStrChar(UINT32 digit)
 Arguments:  None
 returns: a digit as a char
 Description:
 Get a character from  digit input number
 Version : 0.1
 */
static UINT8 getStrChar(const UINT32& digit);
// Comment when module is integrated with the project
// Uncomment to develop or test
#if(0)
SINT32 main(void)
{
    InitDataStructures();
    std::string number = "123456";
    UINT32 num = atoint(number.c_str());
    std::cout << "\nNum is " << num;
    return ZERO;
}
#endif

/*
 Component Function: void InitDataStructures(void);
 Arguments: None
 returns : None
 Version : 0.1
 Description:
 Initialize all vector tables and data structures, keyword list, symbol tables.
 */
void InitDataStructures(void)
{
}

/*
 Component Function: static BOOL IsDigit(SINT8 Digit)
 Arguments: Digit
 returns: Digit or not
 Description:
 Returns if a char is a digit
 Version : 0.1
*/
static BOOL IsDigit(SINT8 Digit)
{
  return (Digit >= '0' && Digit <= '9');
}

/*
 Component Function: signed short atoshort(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
 Get the Number of digits entered as a string.

 For each digit, place it in appropriate place of an integer such as
 digit x 1000 or digit x10000 depending on integer size and input range.
 The multiple of the the first and subsequent digits would be selected depending
 on the Number of digits.

 For example: 123 would be calculated as an integer in the following steps:

 1* 100
 2* 10
 3* 1

 The calculated value is then returned by the function.

 For example, if the digits entered are 12345
 Then, the multipliers are:
 str[0] * 10000
 str[1] * 1000
 str[2] * 100
 str[3] * 10
 str[4] * 1
 Notes:
 Optional: Convert this function use std::string instead of const char * c-style string
 This function would be used by parser module.
 */
signed short atoshort(const SINT8* str)
{
    SINT32 strlen = strlength(str);
    signed short Number = 0;
    switch (strlen)
    {
        case 0:
			Number += static_cast<signed short>( getDecDigit(str[0]) * 0);
			break;
        // Convert characters to digits with another function.
        case 1:
            Number += static_cast<signed short>(getDecDigit(str[0]) * 1);
            break;

        case 2:
            Number += static_cast<signed short>(getDecDigit(str[0]) * 10);
            Number += static_cast<signed short>(getDecDigit(str[1]) * 1);
            break;
        case 3:
            Number += static_cast<signed short>(getDecDigit(str[0]) * 100);
            Number += static_cast<signed short>(getDecDigit(str[1]) * 10);
            Number += static_cast<signed short>(getDecDigit(str[2]) * 1);
            break;
        case 4:
            Number += static_cast<signed short>(getDecDigit(str[0]) * 1000);
            Number += static_cast<signed short>(getDecDigit(str[1]) * 100);
            Number += static_cast<signed short>(getDecDigit(str[2]) * 10);
            Number += static_cast<signed short>(getDecDigit(str[3]) * 1);
            break;
        case 5:
            Number += static_cast<signed short>(getDecDigit(str[0]) * 10000);
            Number += static_cast<signed short>(getDecDigit(str[1]) * 1000);
            Number += static_cast<signed short>(getDecDigit(str[2]) * 100);
            Number += static_cast<signed short>(getDecDigit(str[3]) * 10);
            Number += static_cast<signed short>(getDecDigit(str[4]) * 1);
            break;
        default:
            Number = 0;
            break;
    }
    return Number;
}

/*
 Component Function: unsigned short atoushort(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
 Get the Number of digits entered as a string.

 For each digit, place it in appropriate place of an integer such as
 digit x 1000 or digit x10000 depending on integer size and input range.
 The multiple of the the first and subsequent digits would be selected depending
 on the Number of digits.

 For example: 123 would be calculated as an integer in the following steps:

 1* 100
 2* 10
 3* 1

 The calculated value is then returned by the function.

 For example, if the digits entered are 12345
 Then, the multipliers are:
 str[0] * 10000
 str[1] * 1000
 str[2] * 100
 str[3] * 10
 str[4] * 1
 Notes:
 Optional: Convert this function use std::string instead of const char * c-style string
 This function would be used by parser module.
 */

unsigned short atoushort(const SINT8* str)
{
    SINT32 strlen = strlength(str);
    unsigned short Number = 0;
    switch (strlen)
    {
        case 0:
            Number += static_cast<unsigned short> (getDecDigit(str[0]) * 0);
            break;

            // Convert characters to digits with another function.
        case 1:
            Number += static_cast<unsigned short> (getDecDigit(str[0]) * 1);
            break;
        case 2:
            Number += static_cast<unsigned short> (getDecDigit(str[0]) * 10);
            Number += static_cast<unsigned short> (getDecDigit(str[1]) * 1);
            break;
        case 3:
            Number += static_cast<unsigned short> (getDecDigit(str[0]) * 100);
            Number += static_cast<unsigned short> (getDecDigit(str[1]) * 10);
            Number += static_cast<unsigned short> (getDecDigit(str[2]) * 1);
            break;
        case 4:
            Number += static_cast<unsigned short> (getDecDigit(str[0]) * 1000);
            Number += static_cast<unsigned short> (getDecDigit(str[1]) * 100);
            Number += static_cast<unsigned short> (getDecDigit(str[2]) * 10);
            Number += static_cast<unsigned short> (getDecDigit(str[3]) * 1);
            break;
        case 5:
            Number += static_cast<unsigned short> (getDecDigit(str[0]) * 10000);
            Number += static_cast<unsigned short> (getDecDigit(str[1]) * 1000);
            Number += static_cast<unsigned short> (getDecDigit(str[2]) * 100);
            Number += static_cast<unsigned short> (getDecDigit(str[3]) * 10);
            Number += static_cast<unsigned short> (getDecDigit(str[4]) * 1);
            break;
        default:
            Number = 0;
            break;
    }
    //std::cout << "\nNumber in atouShort is " << Number;
    return Number;
}

/*
 Component Function: SINT32 atoint(const char* str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
 Get the Number of digits entered as a string.

 For each digit, place it in appropriate place of an integer such as
 digit x 1000 or digit x10000 depending on integer size and input range.
 The multiple of the the first and subsequent digits would be selected depending
 on the Number of digits.

 For example: 123 would be calculated as an integer in the following steps:

 1* 100
 2* 10
 3* 1

 The calculated value is then returned by the function.

 For example, if the digits entered are 12345
 Then, the multipliers are:
 str[0] * 10000
 str[1] * 1000
 str[2] * 100
 str[3] * 10
 str[4] * 1
 Notes:
 Optional: Convert this function use std::string instead of const char * c-style string
 This function would be used by parser module.
 */

SINT32 atoint(const SINT8* str)
{
    SINT32 strlen = strlength(str);
    SINT32 Number = 0;
	switch (strlen)
	{
		case 0:
			Number += getDecDigit(str[0]) * 0;
			break;
			// Convert characters to digits with another function.
		case 1:
			Number += getDecDigit(str[0]) * 1;
			break;
		case 2:
			Number += getDecDigit(str[0]) * 10;
			Number += getDecDigit(str[1]) * 1;
			break;
		case 3:
			Number += getDecDigit(str[0]) * 100;
			Number += getDecDigit(str[1]) * 10;
			Number += getDecDigit(str[2]) * 1;
			break;
		case 4:
			Number += getDecDigit(str[0]) * 1000;
			Number += getDecDigit(str[1]) * 100;
			Number += getDecDigit(str[2]) * 10;
			Number += getDecDigit(str[3]) * 1;
			break;
		case 5:
			Number += getDecDigit(str[0]) * 10000;
			Number += getDecDigit(str[1]) * 1000;
			Number += getDecDigit(str[2]) * 100;
			Number += getDecDigit(str[3]) * 10;
			Number += getDecDigit(str[4]) * 1;
			break;
		case 6:
			Number += getDecDigit(str[0]) * 100000;
			Number += getDecDigit(str[1]) * 10000;
			Number += getDecDigit(str[2]) * 1000;
			Number += getDecDigit(str[3]) * 100;
			Number += getDecDigit(str[4]) * 10;
			Number += getDecDigit(str[5]) * 1;
			break;
		case 7:
			Number += getDecDigit(str[0]) * 1000000;
			Number += getDecDigit(str[1]) * 100000;
			Number += getDecDigit(str[2]) * 10000;
			Number += getDecDigit(str[3]) * 1000;
			Number += getDecDigit(str[4]) * 100;
			Number += getDecDigit(str[5]) * 10;
			Number += getDecDigit(str[6]) * 1;
			break;
		case 8:
			Number += getDecDigit(str[0]) * 10000000;
			Number += getDecDigit(str[1]) * 1000000;
			Number += getDecDigit(str[2]) * 100000;
			Number += getDecDigit(str[3]) * 10000;
			Number += getDecDigit(str[4]) * 1000;
			Number += getDecDigit(str[5]) * 100;
			Number += getDecDigit(str[6]) * 10;
			Number += getDecDigit(str[7]) * 1;
			break;

			/* Reserved for future implementation */
			/* Define custom data types to hold numeric data
			 with 64 and 128 bits if machine architecture supports it.*/
		case 9:
			Number += getDecDigit(str[0]) * 100000000;
			Number += getDecDigit(str[1]) * 10000000;
			Number += getDecDigit(str[2]) * 1000000;
			Number += getDecDigit(str[3]) * 100000;
			Number += getDecDigit(str[4]) * 10000;
			Number += getDecDigit(str[5]) * 1000;
			Number += getDecDigit(str[6]) * 100;
			Number += getDecDigit(str[7]) * 10;
			Number += getDecDigit(str[8]) * 1;
			break;
		case 10:
			Number += getDecDigit(str[0]) * 1000000000;
			Number += getDecDigit(str[1]) * 100000000;
			Number += getDecDigit(str[2]) * 10000000;
			Number += getDecDigit(str[3]) * 1000000;
			Number += getDecDigit(str[4]) * 100000;
			Number += getDecDigit(str[5]) * 10000;
			Number += getDecDigit(str[6]) * 1000;
			Number += getDecDigit(str[7]) * 100;
			Number += getDecDigit(str[8]) * 10;
			Number += getDecDigit(str[9]) * 1;
			break;
		default:
			Number = 0;
			break;
	}
    return Number;
}

/*
 Component Function: SINT32 stringtoint(const std::string & str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
 Get the Number of digits entered as a string.

 For each digit, place it in appropriate place of an integer such as
 digit x 1000 or digit x10000 depending on integer size and input range.
 The multiple of the the first and subsequent digits would be selected depending
 on the Number of digits.

 For example: 123 would be calculated as an integer in the following steps:

 1* 100
 2* 10
 3* 1

 The calculated value is then returned by the function.

 For example, if the digits entered are 12345
 Then, the multipliers are:
 str[0] * 10000
 str[1] * 1000
 str[2] * 100
 str[3] * 10
 str[4] * 1
 Notes:
 Optional: Convert this function use std::string instead of const char * c-style string
 This function would be used by parser module.
 */

SINT32 stringtoint(const std::string &str)
{
    SINT32 strlen = str.length();
    SINT32 Number = 0;
	switch (strlen)
	{
		case 0:
			Number += getDecDigit(str[0]) * 0;
			break;
			// Convert characters to digits with another function.
		case 1:
			Number += getDecDigit(str[0]) * 1;
			break;
		case 2:
			Number += getDecDigit(str[0]) * 10;
			Number += getDecDigit(str[1]) * 1;
			break;
		case 3:
			Number += getDecDigit(str[0]) * 100;
			Number += getDecDigit(str[1]) * 10;
			Number += getDecDigit(str[2]) * 1;
			break;
		case 4:
			Number += getDecDigit(str[0]) * 1000;
			Number += getDecDigit(str[1]) * 100;
			Number += getDecDigit(str[2]) * 10;
			Number += getDecDigit(str[3]) * 1;
			break;
		case 5:
			Number += getDecDigit(str[0]) * 10000;
			Number += getDecDigit(str[1]) * 1000;
			Number += getDecDigit(str[2]) * 100;
			Number += getDecDigit(str[3]) * 10;
			Number += getDecDigit(str[4]) * 1;
			break;
		case 6:
			Number += getDecDigit(str[0]) * 100000;
			Number += getDecDigit(str[1]) * 10000;
			Number += getDecDigit(str[2]) * 1000;
			Number += getDecDigit(str[3]) * 100;
			Number += getDecDigit(str[4]) * 10;
			Number += getDecDigit(str[5]) * 1;
			break;
		case 7:
			Number += getDecDigit(str[0]) * 1000000;
			Number += getDecDigit(str[1]) * 100000;
			Number += getDecDigit(str[2]) * 10000;
			Number += getDecDigit(str[3]) * 1000;
			Number += getDecDigit(str[4]) * 100;
			Number += getDecDigit(str[5]) * 10;
			Number += getDecDigit(str[6]) * 1;
			break;
		case 8:
			Number += getDecDigit(str[0]) * 10000000;
			Number += getDecDigit(str[1]) * 1000000;
			Number += getDecDigit(str[2]) * 100000;
			Number += getDecDigit(str[3]) * 10000;
			Number += getDecDigit(str[4]) * 1000;
			Number += getDecDigit(str[5]) * 100;
			Number += getDecDigit(str[6]) * 10;
			Number += getDecDigit(str[7]) * 1;
			break;

			/* Reserved for future implementation */
			/* Define custom data types to hold numeric data
			 with 64 and 128 bits if machine architecture supports it.*/

		case 9:
			Number += getDecDigit(str[0]) * 100000000;
			Number += getDecDigit(str[1]) * 10000000;
			Number += getDecDigit(str[2]) * 1000000;
			Number += getDecDigit(str[3]) * 100000;
			Number += getDecDigit(str[4]) * 10000;
			Number += getDecDigit(str[5]) * 1000;
			Number += getDecDigit(str[6]) * 100;
			Number += getDecDigit(str[7]) * 10;
			Number += getDecDigit(str[8]) * 1;
			break;
		case 10:
			Number += getDecDigit(str[0]) * 1000000000;
			Number += getDecDigit(str[1]) * 100000000;
			Number += getDecDigit(str[2]) * 10000000;
			Number += getDecDigit(str[3]) * 1000000;
			Number += getDecDigit(str[4]) * 100000;
			Number += getDecDigit(str[5]) * 10000;
			Number += getDecDigit(str[6]) * 1000;
			Number += getDecDigit(str[7]) * 100;
			Number += getDecDigit(str[8]) * 10;
			Number += getDecDigit(str[9]) * 1;
			break;
		default:
			Number = 0;
			break;
	}

    return Number;
}

/*
 Component Function: UINT32 atouint(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
 Get the Number of digits entered as a string.

 For each digit, place it in appropriate place of an integer such as
 digit x 1000 or digit x10000 depending on integer size and input range.
 The multiple of the the first and subsequent digits would be selected depending
 on the Number of digits.

 For example: 123 would be calculated as an integer in the following steps:

 1* 100
 2* 10
 3* 1

 The calculated value is then returned by the function.

 For example, if the digits entered are 12345
 Then, the multipliers are:
 str[0] * 10000
 str[1] * 1000
 str[2] * 100
 str[3] * 10
 str[4] * 1
 Notes:
 Optional: Convert this function use std::string instead of const char * c-style string
 This function would be used by parser module.
 */

UINT32 atouint(const SINT8* str)
{
    UINT32 strlen = strlength(str);
    UINT32 Number = 0;
    switch (strlen)
    {
        case 0:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 0);
            break;
            // Convert characters to digits with another function.
        case 1:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 1);
            break;
        case 2:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 10);
            Number += static_cast<UINT32> (getDecDigit(str[1]) * 1);
            break;
        case 3:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 100);
            Number += static_cast<UINT32> (getDecDigit(str[1]) * 10);
            Number += static_cast<UINT32> (getDecDigit(str[2]) * 1);
            break;
        case 4:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 1000);
            Number += static_cast<UINT32> (getDecDigit(str[1]) * 100);
            Number += static_cast<UINT32> (getDecDigit(str[2]) * 10);
            Number += static_cast<UINT32> (getDecDigit(str[3]) * 1);
            break;
        case 5:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 10000);
            Number += static_cast<UINT32> (getDecDigit(str[1]) * 1000);
            Number += static_cast<UINT32> (getDecDigit(str[2]) * 100);
            Number += static_cast<UINT32> (getDecDigit(str[3]) * 10);
            Number += static_cast<UINT32> (getDecDigit(str[4]) * 1);
            break;
        case 6:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 100000);
            Number += static_cast<UINT32> (getDecDigit(str[1]) * 10000);
            Number += static_cast<UINT32> (getDecDigit(str[2]) * 1000);
            Number += static_cast<UINT32> (getDecDigit(str[3]) * 100);
            Number += static_cast<UINT32> (getDecDigit(str[4]) * 10);
            Number += static_cast<UINT32> (getDecDigit(str[5]) * 1);
            break;
        case 7:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 1000000);
            Number += static_cast<UINT32> (getDecDigit(str[1]) * 100000);
            Number += static_cast<UINT32> (getDecDigit(str[2]) * 10000);
            Number += static_cast<UINT32> (getDecDigit(str[3]) * 1000);
            Number += static_cast<UINT32> (getDecDigit(str[4]) * 100);
            Number += static_cast<UINT32> (getDecDigit(str[5]) * 10);
            Number += static_cast<UINT32> (getDecDigit(str[6]) * 1);
            break;
        case 8:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 10000000);
            Number += static_cast<UINT32> (getDecDigit(str[1]) * 1000000);
            Number += static_cast<UINT32> (getDecDigit(str[2]) * 100000);
            Number += static_cast<UINT32> (getDecDigit(str[3]) * 10000);
            Number += static_cast<UINT32> (getDecDigit(str[4]) * 1000);
            Number += static_cast<UINT32> (getDecDigit(str[5]) * 100);
            Number += static_cast<UINT32> (getDecDigit(str[6]) * 10);
            Number += static_cast<UINT32> (getDecDigit(str[7]) * 1);
            break;

            /* Reserved for future implementation */
            /* Define custom data types to hold numeric data
             with 64 and 128 bits if machine architecture supports it.*/
        case 9:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 100000000);
            Number += static_cast<UINT32> (getDecDigit(str[1]) * 10000000);
            Number += static_cast<UINT32> (getDecDigit(str[2]) * 1000000);
            Number += static_cast<UINT32> (getDecDigit(str[3]) * 100000);
            Number += static_cast<UINT32> (getDecDigit(str[4]) * 10000);
            Number += static_cast<UINT32> (getDecDigit(str[5]) * 1000);
            Number += static_cast<UINT32> (getDecDigit(str[6]) * 100);
            Number += static_cast<UINT32> (getDecDigit(str[7]) * 10);
            Number += static_cast<UINT32> (getDecDigit(str[8]) * 1);
            break;
        case 10:
            Number += static_cast<UINT32> (getDecDigit(str[0]) * 1000000000);
            Number += static_cast<UINT32> (getDecDigit(str[1]) * 100000000);
            Number += static_cast<UINT32> (getDecDigit(str[2]) * 10000000);
            Number += static_cast<UINT32> (getDecDigit(str[3]) * 1000000);
            Number += static_cast<UINT32> (getDecDigit(str[4]) * 100000);
            Number += static_cast<UINT32> (getDecDigit(str[5]) * 10000);
            Number += static_cast<UINT32> (getDecDigit(str[6]) * 1000);
            Number += static_cast<UINT32> (getDecDigit(str[7]) * 100);
            Number += static_cast<UINT32> (getDecDigit(str[8]) * 10);
            Number += static_cast<UINT32> (getDecDigit(str[9]) * 1);
            break;
        default:
            Number = 0;
            break;
    }

    return Number;
}

/*
 Component Function: long atolong(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to long integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
 Get the Number of digits entered as a string.

 For each digit, place it in appropriate place of an integer such as
 digit x 1000 or digit x10000 depending on integer size and input range.
 The multiple of the the first and subsequent digits would be selected depending
 on the Number of digits.

 For example: 123 would be calculated as an integer in the following steps:

 1* 100
 2* 10
 3* 1

 The calculated value is then returned by the function.

 For example, if the digits entered are 12345
 Then, the multipliers are:
 str[0] * 10000
 str[1] * 1000
 str[2] * 100
 str[3] * 10
 str[4] * 1
 Notes:
 Optional: Convert this function use std::string instead of const char * c-style string
 This function would be used by parser module.
 */

SINT64 atolong(const SINT8* str)
{
    int strlen = strlength(str); 
    long int Number = 0; 
    switch (strlen) 
    {
		case 0:
		   Number += getDecDigit(str[0]) * 0;
		   break;
		// Convert characters to digits with another function.
		case 1:
			Number += getDecDigit(str[0]) * 1;
			break;
		case 2:
			Number += getDecDigit(str[0]) * 10;
			Number += getDecDigit(str[1]) * 1;
			break;
		case 3:
			Number += getDecDigit(str[0]) * 100;
			Number += getDecDigit(str[1]) * 10;
			Number += getDecDigit(str[2]) * 1;
		break;
		case 4:
			Number += getDecDigit(str[0]) * 1000;
			Number += getDecDigit(str[1]) * 100;
			Number += getDecDigit(str[2]) * 10;
			Number += getDecDigit(str[3]) * 1;
		break;
	   case 5:
			Number += getDecDigit(str[0]) * 10000;
			Number += getDecDigit(str[1]) * 1000;
			Number += getDecDigit(str[2]) * 100;
			Number += getDecDigit(str[3]) * 10;
			Number += getDecDigit(str[4]) * 1;
			break;
		case 6:
			Number += getDecDigit(str[0]) * 100000;
			Number += getDecDigit(str[1]) * 10000;
			Number += getDecDigit(str[2]) * 1000;
			Number += getDecDigit(str[3]) * 100;
			Number += getDecDigit(str[4]) * 10;
			Number += getDecDigit(str[5]) * 1;
			break;
		case 7:
			Number += getDecDigit(str[0]) * 1000000;
			Number += getDecDigit(str[1]) * 100000;
			Number += getDecDigit(str[2]) * 10000;
			Number += getDecDigit(str[3]) * 1000;
			Number += getDecDigit(str[4]) * 100;
			Number += getDecDigit(str[5]) * 10;
			Number += getDecDigit(str[6]) * 1;
			break;
		case 8:
			Number += getDecDigit(str[0]) * 10000000;
			Number += getDecDigit(str[1]) * 1000000;
			Number += getDecDigit(str[2]) * 100000;
			Number += getDecDigit(str[3]) * 10000;
			Number += getDecDigit(str[4]) * 1000;
			Number += getDecDigit(str[5]) * 100;
			Number += getDecDigit(str[6]) * 10;
			Number += getDecDigit(str[7]) * 1;
			break;

			/* Reserved for future implementation */
			/* Define custom data types to hold numeric data
			 with 64 and 128 bits if machine architecture supports it.*/

		case 9:
			Number += getDecDigit(str[0]) * 100000000;
			Number += getDecDigit(str[1]) * 10000000;
			Number += getDecDigit(str[2]) * 1000000;
			Number += getDecDigit(str[3]) * 100000;
			Number += getDecDigit(str[4]) * 10000;
			Number += getDecDigit(str[5]) * 1000;
			Number += getDecDigit(str[6]) * 100;
			Number += getDecDigit(str[7]) * 10;
			Number += getDecDigit(str[8]) * 1;
			break;
		case 10:
			Number += getDecDigit(str[0]) * 1000000000;
			Number += getDecDigit(str[1]) * 100000000;
			Number += getDecDigit(str[2]) * 10000000;
			Number += getDecDigit(str[3]) * 1000000;
			Number += getDecDigit(str[4]) * 100000;
			Number += getDecDigit(str[5]) * 10000;
			Number += getDecDigit(str[6]) * 1000;
			Number += getDecDigit(str[7]) * 100;
			Number += getDecDigit(str[8]) * 10;
			Number += getDecDigit(str[9]) * 1;
			break;
		default:
			Number = 0;
			break;
	}
    return Number;
}

/*
 Component Function: UINT64 atoulong(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to unsigned long integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
 Get the Number of digits entered as a string.

 For each digit, place it in appropriate place of an integer such as
 digit x 1000 or digit x10000 depending on integer size and input range.
 The multiple of the the first and subsequent digits would be selected depending
 on the Number of digits.

 For example: 123 would be calculated as an integer in the following steps:

 1* 100
 2* 10
 3* 1

 The calculated value is then returned by the function.

 For example, if the digits entered are 12345
 Then, the multipliers are:
 str[0] * 10000
 str[1] * 1000
 str[2] * 100
 str[3] * 10
 str[4] * 1
 Notes:
 Optional: Convert this function use std::string instead of const char * c-style string
 This function would be used by parser module.
 */

UINT64 atoulong(const SINT8* str)
{
    UINT32 strlen = strlength(str);
    UINT64 Number = 0;
    switch (strlen)
    {
        case 0:
            Number += getDecDigit(str[0]) * 0;
            break;
            // Convert characters to digits with another function.
        case 1:
            Number += getDecDigit(str[0]) * 1;
            break;
        case 2:
            Number += getDecDigit(str[0]) * 10;
            Number += getDecDigit(str[1]) * 1;
            break;
        case 3:
            Number += getDecDigit(str[0]) * 100;
            Number += getDecDigit(str[1]) * 10;
            Number += getDecDigit(str[2]) * 1;
            break;
        case 4:
            Number += getDecDigit(str[0]) * 1000;
            Number += getDecDigit(str[1]) * 100;
            Number += getDecDigit(str[2]) * 10;
            Number += getDecDigit(str[3]) * 1;
            break;
        case 5:
            Number += getDecDigit(str[0]) * 10000;
            Number += getDecDigit(str[1]) * 1000;
            Number += getDecDigit(str[2]) * 100;
            Number += getDecDigit(str[3]) * 10;
            Number += getDecDigit(str[4]) * 1;
            break;
        case 6:
            Number += getDecDigit(str[0]) * 100000;
            Number += getDecDigit(str[1]) * 10000;
            Number += getDecDigit(str[2]) * 1000;
            Number += getDecDigit(str[3]) * 100;
            Number += getDecDigit(str[4]) * 10;
            Number += getDecDigit(str[5]) * 1;
            break;
        case 7:
            Number += getDecDigit(str[0]) * 1000000;
            Number += getDecDigit(str[1]) * 100000;
            Number += getDecDigit(str[2]) * 10000;
            Number += getDecDigit(str[3]) * 1000;
            Number += getDecDigit(str[4]) * 100;
            Number += getDecDigit(str[5]) * 10;
            Number += getDecDigit(str[6]) * 1;
            break;
        case 8:
            Number += getDecDigit(str[0]) * 10000000;
            Number += getDecDigit(str[1]) * 1000000;
            Number += getDecDigit(str[2]) * 100000;
            Number += getDecDigit(str[3]) * 10000;
            Number += getDecDigit(str[4]) * 1000;
            Number += getDecDigit(str[5]) * 100;
            Number += getDecDigit(str[6]) * 10;
            Number += getDecDigit(str[7]) * 1;
            break;
        case 9:
            Number += getDecDigit(str[0]) * 100000000;
            Number += getDecDigit(str[1]) * 10000000;
            Number += getDecDigit(str[2]) * 1000000;
            Number += getDecDigit(str[3]) * 100000;
            Number += getDecDigit(str[4]) * 10000;
            Number += getDecDigit(str[5]) * 1000;
            Number += getDecDigit(str[6]) * 100;
            Number += getDecDigit(str[7]) * 10;
            Number += getDecDigit(str[8]) * 1;
            break;
        case 10:
            Number += getDecDigit(str[0]) * 1000000000;
            Number += getDecDigit(str[1]) * 100000000;
            Number += getDecDigit(str[2]) * 10000000;
            Number += getDecDigit(str[3]) * 1000000;
            Number += getDecDigit(str[4]) * 100000;
            Number += getDecDigit(str[5]) * 10000;
            Number += getDecDigit(str[6]) * 1000;
            Number += getDecDigit(str[7]) * 100;
            Number += getDecDigit(str[8]) * 10;
            Number += getDecDigit(str[9]) * 1;
            break;
            /* Reserved for future implementation */
            /* Define custom data types to hold numeric data
             with 64 and 128 bits if machine architecture supports it.*/
            /*
   case 11:
           Number += getDecDigit(str[0]) * 10000000000;
           Number += getDecDigit(str[1]) * 1000000000;
           Number += getDecDigit(str[2]) * 100000000;
           Number += getDecDigit(str[3]) * 10000000;
           Number += getDecDigit(str[4]) * 1000000;
           Number += getDecDigit(str[5]) * 100000;
           Number += getDecDigit(str[6]) * 10000;
           Number += getDecDigit(str[7]) * 1000;
           Number += getDecDigit(str[8]) * 100;
           Number += getDecDigit(str[9]) * 10;
           Number += getDecDigit(str[10]) * 1;
           break;
   case 12:
           Number += getDecDigit(str[0]) * 100000000000;
           Number += getDecDigit(str[1]) * 10000000000;
           Number += getDecDigit(str[2]) * 1000000000;
           Number += getDecDigit(str[3]) * 100000000;
           Number += getDecDigit(str[4]) * 10000000;
           Number += getDecDigit(str[5]) * 1000000;
           Number += getDecDigit(str[6]) * 100000;
           Number += getDecDigit(str[7]) * 10000;
           Number += getDecDigit(str[8]) * 1000;
           Number += getDecDigit(str[9]) * 100;
           Number += getDecDigit(str[10]) * 10;
           Number += getDecDigit(str[11]) * 1;
             */

        default:
            Number = 0;
            break;
    }

    return Number;
}

/*
 Component static SINT32 strlength(const SINT8 *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
static SINT32 strlength(const SINT8 *str)
{
    SINT32 count = 0;
    while (str[count] != '\0')
    {
        count++;
    }
    return count;
}

// get a digit from corresponding character in input string

/*
 Component static UINT32 getDecDigit(const UINT8& str)
 Arguments: Chat digit
 returns : decimal digit
 Version : 0.1
 Description:
 Converts a char to decimal digit.
 */
static UINT32 getDecDigit(const UINT8& str)
{
    UINT32 digit = 0;
    switch (str)
    {
        case '0':
            digit = 0;
            break;
        case '1':
            digit = 1;
            break;
        case '2':
            digit = 2;
            break;
        case '3':
            digit = 3;
            break;
        case '4':
            digit = 4;
            break;
        case '5':
            digit = 5;
            break;
        case '6':
            digit = 6;
            break;
        case '7':
            digit = 7;
            break;
        case '8':
            digit = 8;
            break;
        case '9':
            digit = 9;
            break;
        default:
            digit = 0;
            break;
    }
    return digit;
}

/*
 Component Function: static std::string itostring(UINT32 a)
 Arguments:  UINT32 to be converted to std::string
 returns: None
 Description:
 Convert a decimal unsigned integer to std::string
 License: GPL V3
  Version : 0.1
To do:
itostring should have line number information.

 */
std::string itostring(const UINT32& a)
{
    std::string b;
    UINT32 NumofDigits = getNumofDecDigit(a);
    switch (NumofDigits)
    {
        case 0:
            break;
        case 1:
            b += getStrChar(a);
            break;
        case 2:
            b += getStrChar(a / 10);
            b += getStrChar(a % 10);
            break;
        case 3:
            b += getStrChar(a / 100);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);
            break;
        case 4:
            b += getStrChar(a / 1000);
            b += getStrChar((a / 100) % 10);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);
            break;
        case 5:
            b += getStrChar(a / 10000);
            b += getStrChar((a / 1000) % 10);
            b += getStrChar((a / 100) % 10);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);
            break;
        case 6:
            b += getStrChar(a / 100000);
            b += getStrChar((a / 10000) % 10);
            b += getStrChar((a / 1000) % 10);
            b += getStrChar((a / 100) % 10);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);
            break;
        case 7:
            b += getStrChar(a / 1000000);
            b += getStrChar((a / 100000) % 10);
            b += getStrChar((a / 10000) % 10);
            b += getStrChar((a / 1000) % 10);
            b += getStrChar((a / 100) % 10);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);
            break;
        case 8:
            b += getStrChar(a / 10000000);
            b += getStrChar((a / 1000000) % 10);
            b += getStrChar((a / 100000) % 10);
            b += getStrChar((a / 10000) % 10);
            b += getStrChar((a / 1000) % 10);
            b += getStrChar((a / 100) % 10);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);
            break;
        default:
            ErrorTracer(UNABLE_TO_PROCESS_LINENUMBER, 10, 1);
            break;
    }
    return b;
}

/*
 Component Function: static UINT32 getNumofDecDigit(UINT32 Number)
 Arguments:  Number to detect digits
 returns: Number of digits
 Description:
 Returns number of digits in a UINT32eger
 Version : 0.1
 */

static inline UINT32 getNumofDecDigit(UINT32 Number)
{
    UINT32 NumofDigits = 0;
    //Number= (Number>0)? Number: -Number; // This is always true for unsigned integers
    while (Number)
    {
        Number /= 10;
        NumofDigits++;
    }
    return NumofDigits;
}

/*
 Component Function: static UINT8 getStrChar(UINT32 digit)
 Arguments:  digit as UINT32
 Returns: a digit as a char
 Description:
 Get a character from  digit input number
 Version : 0.1
 */
static UINT8 getStrChar(const UINT32& digit)
{
    UINT8 StrChar = ' ';
    switch (digit)
    {
        case 0:
            StrChar = '0';
            break;
        case 1:
            StrChar = '1';
            break;
        case 2:
            StrChar = '2';
            break;
        case 3:
            StrChar = '3';
            break;
        case 4:
            StrChar = '4';
            break;
        case 5:
            StrChar = '5';
            break;
        case 6:
            StrChar = '6';
            break;
        case 7:
            StrChar = '7';
            break;
        case 8:
            StrChar = '8';
            break;
        case 9:
            StrChar = '9';
            break;
        default:
            StrChar = '0';
            break;
    }
    return StrChar;
}

/*
 Component Function: BOOL IsAlphabet(SINT8 Character)
 Arguments:  Character to be searched in list of alphabets
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z
 Returns Alphabetic or not
  Version : 0.1
 */
BOOL IsAlphabet(SINT8 Character)
{
	return ((Character >= 'a' && Character <= 'z') || (Character >= 'A' && Character <= 'z'));
}

/*
 Component Function: FLOAT32 atofloat(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to floating point
 Version : 0.1
 Description:
 Function to convert a string of Number into an Floating point number.
 Get the Number of digits entered as a string.
 Recognize a . in the string and put the decimal part before the . and the mantissa after the dot.
 Notes:
 This function would be used by parser/lexer module.
 */
FLOAT32 atofloat(const SINT8* str)
{
    UINT32 strlen = strlength(str);
    UINT32 index = ZERO;
    FLOAT32 Number = ZERO;
    FLOAT32 Characteristic = ZERO;
    FLOAT32 Mantissa = ZERO;
    UINT32 DecDigit = ZERO;
    UINT32 DecCount = ZERO;
    UINT32 FloatPosition = ZERO;
    UINT32 FloatIndex = ZERO;
    while (str[DecCount] != '.' && str[DecCount] != '\0')
    {
        DecCount++;
    }
    // Get the floating point part
    FloatPosition = DecCount + 1;
    //std::cout<<"\nDecCount "<< DecCount;
    //std::cout<<"\nFloatPosition " << FloatPosition;
    // For loop to calculate all characteristic and mantissa digits.
    for (index = 0; index < strlen; index++)
    {

        if (index < DecCount)
        {
            DecDigit = DecCount - index - 1;
            Characteristic += static_cast<FLOAT32> (getDecDigit(str[DecDigit]) * DecMultiplier(index));
        } else
        {
            if (index == DecCount)
            {
                index++;
                FloatIndex = 1;
            }
            if (FloatPosition != strlen)
            {
                Mantissa += static_cast<FLOAT32> (getDecDigit(str[FloatPosition]) * MantissaFloatMultiplier(FloatIndex));
                //std::cout << "\nMantissa " << Mantissa;
            } else
            {
                Mantissa = 0;
            }
            FloatPosition++;
            FloatIndex++;
        }

    }
    //std::cout <<"\nFloat number before return " << Number;
    //std::cout << "\nMantissa before return " << Mantissa;
    Number = static_cast<FLOAT32> (Characteristic + Mantissa);
    return Number;
}

/*
 Component Function: FLOAT64 todouble(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to double number
 Version : 0.1
 Description:
 Function to convert a string of Number into an Floating point number.
 Get the Number of digits entered as a string.
 Recognize a . in the string and put the decimal part before the . and the mantissa after the dot.
 Notes:
 This function would be used by lexer module.
 */
FLOAT64 atodouble(const SINT8* str)
{
    UINT32 index = ZERO;
    FLOAT64 Number = ZERO;
    FLOAT64 Characteristic = ZERO;
    FLOAT64 Mantissa = ZERO;
    UINT32 DecDigit = ZERO;
    UINT32 DecCount = ZERO;
    UINT32 FloatPosition = ZERO;
    UINT32 FloatIndex = ZERO;
    UINT32 strlen = strlength(str);
    while (str[DecCount] != '.' && str[DecCount] != '\0')
    {
        DecCount++;
    }
    // Get the floating point part
    FloatPosition = DecCount + 1;
    // For loop to calculate all characteristic and mantissa digits.
    for (index = 0; index < strlen; index++)
    {

        if (index < DecCount)
        {
            DecDigit = DecCount - index - 1;
            Characteristic += static_cast<FLOAT64> (getDecDigit(str[DecDigit]) * DecMultiplier(index));
        } else
        {
            if (index == DecCount)
            {
                index++;
                FloatIndex = 1;
            }
            if (FloatPosition != strlen)
            {
                Mantissa += static_cast<FLOAT64> (getDecDigit(str[FloatPosition]) * MantissaDoubleMultiplier(FloatIndex));
            } else
            {
                Mantissa = 0;
            }
            FloatPosition++;
            FloatIndex++;
        }

    }
    Number = Characteristic + Mantissa;
    return Number;
}

/*
 Component Function: ULONGLONG DecMultiplier(UINT32 index)
 Arguments: a string which contains digits
 returns : a multiplier of pow(10,n)
 Version : 0.1
 Description:
  To do:
 Use a loop to convert float digits upto 20 decimal points .
 */
ULONGLONG DecMultiplier(UINT32 index)
{
    ULONGLONG ReturnValue = 0;
    UINT32 TEN = 10;
    ReturnValue = static_cast<ULONGLONG> (pow(TEN, index));
    return ReturnValue;
}

/*
Component FLOAT32 MantissaFloatMultiplier(UINT32 index)
Arguments: Position of Mantissa digit
returns : a multiplier of pow(0.1,n)
Version : 0.1
Description:
Returns pow (0.1,index)
 */
FLOAT32 MantissaFloatMultiplier(UINT32 index)
{
    FLOAT32 ReturnValue = 0.0;
    FLOAT32 Mantissa = static_cast<FLOAT32> (0.1);
    ReturnValue = static_cast<FLOAT32> (pow(Mantissa, index));
    return ReturnValue;
}

/*
Component FLOAT64 MantissaDoubleMultiplier(UINT32 index)
Arguments: Position of Mantissa digit
returns : a multiplier of pow(0.1,n)
Version : 0.1
Description:
Returns pow (0.1,index)
 */
FLOAT64 MantissaDoubleMultiplier(UINT32 index)
{
    FLOAT64 ReturnValue = 0.0;
    FLOAT64 Mantissa = 0.1;
    ReturnValue = pow(Mantissa, index);
    return ReturnValue;
}

/*
 Component Function: void InitVariant(Variant &Var)
 Arguments:  None
 returns: None
 Description:
 Initializes a variant type. This function is necessary to avoid
 a anonymous union initialization warning.
 Version : 0.1
 */
void InitVariant(Variant &Var)
{
    Var.Identifier = "NONE";
    Var.Type = NOTADATATYPE;
    Var.u.cString = "NONE";
}
/*
 Component Function: UINT32 isNumeric(const SINT8* Str)
 Arguments:  String character to be searched in list of digits
 Returns:
 Description: This function looks up all characters of a string in a list of digits and returns the nature of the number whether
 it is a decimal,floating point or double precision number.
 Notes:
 Add support for hexa and octal numbers.
  Version : 0.1
 */
UINT32 IsNumeric(const SINT8* str)
{
    UINT32 index = 0;
    UINT32 StrLength = strlength(str);
    UINT32 DigitCount = 0;
    UINT32 NumberType = NOT_A_NUMBER;
    UINT32 FloatingDigits = 0;
    long double Number = 0;
    BOOL floatingprecision = false;
    while (index <= StrLength)
    {
        // Reading numeric chars
		if (IsDigit(str[index]))
		{
			if (NumberType == FLOATTYPE)
			{
				FloatingDigits++;
			}
			DigitCount++;
		}
		else if (str[index] == DOT)
		{
			NumberType = FLOATTYPE;
			floatingprecision = true;
		}
        index++;
    }
    if ((DigitCount == StrLength) || (DigitCount == StrLength - 1) || (DigitCount == StrLength - 2))
    {
        Number = static_cast<FLOAT64> (atodouble(str));
        if (Number >= -32768 && Number <= 32767 && FloatingDigits == 0 && floatingprecision == false)
        {
            NumberType = SHORTTYPE;

        } else if (Number >= 0 && Number <= 65535 && FloatingDigits == 0 && floatingprecision == false)
        {
            NumberType = USHORTTYPE;

        } else if (Number >= -2147483648LL && Number <= 2147483647LL && FloatingDigits == 0 && floatingprecision == false)
        {
            NumberType = LONGINTTYPE;

        } else if (Number >= 0 && Number <= 4294967295LL && FloatingDigits == 0 && floatingprecision == false)
        {
            NumberType = ULONGINTTYPE;

        }
		#if defined (MINGW64)
        else if (Number >= -9223372036854775808 && Number <= 9223372036854775807 && FloatingDigits == 0 && floatingprecision == false)
        {
            NumberType = LONGLONGINTTYPE;
        } else if (Number >= 0 && Number <= 18446744073709551615 && FloatingDigits == 0 && floatingprecision == false)
        {
            NumberType = ULONGLONGINTTYPE;
        }
		#endif
        else if (Number >= -3.4 * pow(10, 38) && Number <= 3.4 * pow(10, 38))
        {
            NumberType = FLOATTYPE;

        } else if (Number >= -1.7 * pow(10, 308) && 1.7 * pow(10, 308))
        {
            NumberType = DOUBLETYPE;

        }
        else
        {
            /* For C++ long double implementation */
        }
    }
    //std::cout << Number << std::endl;
    //PrintNumberType(NumberType);
    return NumberType;
}

/*
Component Function: void PrintNumberType(UINT32 NumberType)
Arguments:  Number type
Returns: None
Description:  Displays data in token.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintNumberType(UINT32 NumberType)
{
    switch (NumberType)
    {
        case SHORTTYPE:
            //std::cout << "SHORTTYPE number" << std::endl;
            break;
        case USHORTTYPE:
            //std::cout << "USHORTTYPE number" << std::endl;
            break;
        case INTTYPE:
            //std::cout << "INTTYPE number " << std::endl;
            break;
        case UINTTYPE:
            //std::cout << "UINTTYPE number " << std::endl;
            break;
        case LONGINTTYPE:
            //std::cout << "LONGINTTYPE number " << std::endl;
            break;
        case ULONGINTTYPE:
            //std::cout << "ULONGINTTYPE number " << std::endl;
            break;
        case FLOATTYPE:
            //std::cout << "FLOATTYPE number " << std::endl;
            break;
        case DOUBLETYPE:
            //std::cout << "DOUBLETYPE number " << std::endl;
            break;

        case LONGDOUBLETYPE:
            //std::cout << "LONGDOUBLETYPE number " << std::endl;
            break;
        default:
            break;
    }
}

/*
 Component Function: UINT32 DetectStringType(const std::string& CharString)
 Arguments:  String
 returns: None
 Description:
 Detects type of string such as define or a IDENT
 Version : 0.1
 Notes:
 Function being designed.
 Known issues:
 */
UINT32 DetectStringType(const std::string& CharString)
//UINT32 DetectStringType(const SINT8* CharString, UINT32 &index, std::vector <SINT8>& s)
{
    UINT32 StringType = NOTSTRING;
    //std::cout <<"In  DetectStringType() function " << std::endl;
    //std::cout << "String analyzed in DetectStringType() is " << CharString << std::endl;
   if (CharString == "sin")
    {
        StringType = OPSIN;
    } else if (CharString == "cos")
    {
        StringType = OPCOS;
    } 
    else if (CharString == "tan")
    {
        StringType = OPTAN;
    } 
    else if (CharString == "cosec")
    {
        StringType = OPCOSEC;
    } 
    else if (CharString == "sec")
    {
        StringType = OPSEC;
    } 
    else if (CharString == "cot")
    {
        StringType = OPCOT;
    } 
    else if (CharString == "sqrt")
    {
        StringType = OPSQRT;
    } 
    else if (CharString == "loge")
    {
        StringType = OPLOG;
    } 
    else if (CharString == "<")
    {
    	StringType = OPLESSTHAN;
	} 
	else if (CharString == ">")
	{
		StringType = OPGREATERTHAN;
	}
	else if (CharString == "<<")
	{
		StringType = OPLEFTBITSHIFT;
	}
	else if (CharString == "!")
	{
		StringType = OPLOGICALNOT;
	}
	else if (CharString == "|")
	{
		StringType = OPBITWISEOR;
	}
	else if (CharString == "&")
	{
		StringType = OPBITWISEAND;
	}
	else if (CharString == ">>")
	{
		StringType = OPRIGHTBITSHIFT;
	}
	else if (CharString == "&&")
	{
		StringType = OPLOGICALAND;
	}
	else if (CharString == "||")
	{
		StringType = OPLOGICALOR;
	}
	else if (CharString == "<=")
	{
		StringType = OPLESSTHANEQUALTO;
	}
	else if (CharString == ">=")
	{
		StringType = OPGREATERTHANEQUALTO;
	}
	else if (CharString == "==")
	{
		StringType = OPLOGICALISEQUALTO;
	}
	else if (CharString == "!=")
	{
		StringType = OPLOGICALISNOTEQUALTO;
	}
	else if (CharString == "xor")
	{
		StringType = OPBITWISEXOR;
	}
	else if (CharString == "~")
	{
		StringType = OPCOMPLEMENT;
	}
    //std::cout <<"End of DetectStringType() function " << std::endl;
    return StringType;
}

/*
 Component Function: UINT32 DetectOpType(const SINT8* OpString)
 Arguments:  char string
 returns: None
 Description:
 Detects type of string such as define or a IDENT
 Version : 0.1
 Notes:
 This seems a bit crude yet could be fine for user input type code.
 */
UINT32 DetectOpType(const SINT8* OpString)
{
    std::string Op(OpString);
    //const SINT8* Op =strdup(OpType.c_str());
    //std::cout << "In DetectOpType() function " << std::endl;
    UINT32 ReturnOpType = NOTOP;
	if (Op == "+")
	{
    ReturnOpType = OPPLUS;
	} 
	else if (Op == "-")
	{
    ReturnOpType = OPMINUS;
	} 
	else if (Op == "/")
	{
    ReturnOpType = OPDIV;
	} 
	else if (Op == "*")
	{
    ReturnOpType = OPMUL;
	} 
	else if (Op == "%")
	{
    ReturnOpType = OPMODULUS;
	} 
	else if (Op == "^")
	{
    ReturnOpType = OPPOWEXPONENT;
	} 
	else if (Op == "_")
	{
    ReturnOpType = OPUNARYMINUS;
	} 
	else if (Op == "=")
	{
    ReturnOpType = OPEQUALTO;
	} 
	else if (Op == "sin")
	{
    ReturnOpType = OPSIN;
	} 
	else if (Op == "cos")
	{
    ReturnOpType = OPCOS;
	} 
	else if (Op == "tan")
	{
    ReturnOpType = OPTAN;
	} 
	else if (Op == "sec")
	{
    ReturnOpType = OPSEC;
	} 
	else if (Op == "cosec")
	{
    ReturnOpType = OPCOSEC;
	} 
	else if (Op == "cot")
	{
    ReturnOpType = OPCOT;
	} 
	else if (Op == "sqrt")
	{
    ReturnOpType = OPSQRT;
	} 
	else if (Op == "loge")
	{
    ReturnOpType = OPLOG;
	}
	else if ( Op == "<")
	{
    ReturnOpType = OPLESSTHAN;
	}
	else if (Op == ">")
	{
    ReturnOpType = OPGREATERTHAN;
	}
	else if (Op == "<<")
	{
    ReturnOpType = OPLEFTBITSHIFT;
	}
	else if (Op == ">>")
	{
    ReturnOpType = OPRIGHTBITSHIFT;
	}
	else if (Op == "!")
	{
    ReturnOpType = OPLOGICALNOT;
	}
	else if (Op == "|")
	{
    ReturnOpType = OPBITWISEOR;
	}
	else if (Op == "&")
	{
    ReturnOpType = OPBITWISEAND;
	}
	else if (Op == "&&")
	{
    ReturnOpType = OPLOGICALAND;
	}
	else if (Op == "||")
	{
    ReturnOpType = OPLOGICALOR;
	}
	else if (Op == "<=")
	{
    ReturnOpType = OPLESSTHANEQUALTO;
	}
	else if (Op == ">=")
	{
    ReturnOpType = OPGREATERTHANEQUALTO;
	}
	else if (Op == "==")
	{
    ReturnOpType = OPLOGICALISEQUALTO;
	}
	else if (Op == "!=")
	{
    ReturnOpType = OPLOGICALISNOTEQUALTO;
	}
	else if (Op == "xor")
	{
    ReturnOpType = OPBITWISEXOR;
	}
	else if (Op == "~")
	{
    ReturnOpType = OPCOMPLEMENT;
	}
    //std::cout << "At endof DetectOpType() function " << std::endl;
    return ReturnOpType;
}
