/*
Name:Stack.h
Copyright:
Author:Amit Malyala
Date: 07-01-17 19:59
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
*/

#ifndef STACK_H
#define STACK_H

#include "std_types.h"
#include "Tree.h"
#include "Lexer.h"
#include "EvaluateExpressions.h"
#include <stack>

/* 
10000 operands and operators are enough for a complex expression
If there are stack access errors, increase number of operators and operands. */
/* For compiler binary, use a vector for stacks */
#define MAX 20000


/*
Component Function: void InitStack(void)
Arguments:  None
Returns: None
Description:  Initializes Operator and RPN stack
Version: 0.1 Initial version
*/
void InitStack(void);
/* Definitions for composite data in stack which contains operands and operators for
   RPN stack*/


/*
Component Function: BOOL PopTokenOpStack(Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenOpStack(Token_tag& px);

/*
Component Function: BOOL isOpStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isOpStackEmpty(void);

/*
Component Function: BOOL isArithmeticEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isArithmeticStackEmpty(void);

/*
Component Function: BOOL isPostfixStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isPostfixStackEmpty(void);



/*
Component Function: BOOL PopTokenPostfixStack(Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenPostfixStack(Token_tag& px);

/*
Component Function: BOOL PushTokenPostfixStack(const Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenPostfixStack(const Token_tag& Token);
/*
Component Function: BOOL PushToken(const Token_tag& Token);
Arguments:  Reference to stack, data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenOpStack(const Token_tag& Token);

#if defined (DEVELOPMENT_DEBUGGING_ENABLED)

/*
Component Function: void DisplayPostfixStack(const std::vector <Token_tag> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:  Displays Postfix stack.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void DisplayPostfixStack(const std::vector <Token_tag> &pS);

#endif 
/*
Component Function: BOOL PopTokenArithmeticStack(Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenArithmeticStack(Token_tag& px);

/*
Component Function: BOOL PushTokenArithmeticStack(const Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenArithmeticStack(const Token_tag& Token);

/*
Component Function: void DisplayOpStack(const std::vector <Token_tag> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:  
Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void DisplayOpStack(const std::vector <Token_tag> &pS);

/*
Component Function: void ReversePostfixStack(void)
Arguments:  None
Returns: Nothing
Description:  Reverses a postfix stack in RPN notation.
Version : 0.1
 */
void ReversePostfixStack(void);




 #if defined (EVALUATION_METHOD_EXPRESSIONTREE)
/*
Component Function: BOOL isTreeStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isTreeStackEmpty(void);

/*
Component Function: BOOL PopNode(Tnode &aNode)
Arguments:  Reference to node being popped
Returns: true or not
Description:  Returns true if pop is successful
Version : 0.1
 */
BOOL PopNode(Tnode &aNode);

/*
Component Function: BOOL PushNode(Tnode &aNode);
Arguments:  Reference to Tree node being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushNode(Tnode &aNode);


#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
/*
Component Function: void PrintTreeStack(void)
Arguments:  None
Returns: None
Description:  Displays Tree stack.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
*/
void PrintTreeStack(void);
 
/*
Component Function: void PrintTreeStackPos(void)
Arguments:  None
Returns: None
Description:  Displays Tree stack top pos
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintTreeStackPos(void);
 
/*
Component Function: void PrintOpStack(void)
Arguments:  None
Returns: None
Description:  Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintOpStack(void);
#endif // #if defined (DEVELOPMENT_DEBUGGING_ENABLED)



#endif // #if defined (EVALUATION_METHOD_EXPRESSIONTREE)

#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
 /*
Component Function: void PrintPostfixStack(void)
Arguments:  None
Returns: None
Description:  Displays Postfix stack.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintPostfixStack(void);



/*
Component Function: void PrintOpStack(void)
Arguments:  None
Returns: None
Description:  Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintOpStack(void);

#endif // #if defined (DEVELOPMENT_DEBUGGING_ENABLED)

/*
Component Function: BOOL PushTokenOpStack(const Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenOpStackVector(const Token_tag& Token);

/*
Component Function: BOOL PopOpStackVector (Token_tag& Token)
Arguments: Op Token being poppped
Returns: true or not
Description:  Returns true if push is successful
Version : 0.1
*/
BOOL PopOpStackVector(Token_tag& Token);

#endif

