/*
Name :  EvaluateExpressions.cpp
Author: Amit Malyala
Date : 27-05-17 12:23

Version: 0.1 Initial version
License:
Copyright <2017> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:

Calculator program to evaluate arithmetic expressions.

Enter a valid arithmetic expression in ArithmeticExpression.txt and run the program.

Notes:

To do:
Write a function in Parser module which can intelligently detect operators having higher precedence than a incoming operator compared to op on top of op stack and ops beneath the operator on top of stack
Operator table in decreasing order of precedence
-------------------------------------------------
(sin,cos,tan,sec,cosec,cot,loge, pow) -O1
                                                    ~  -O2
                                                    !  -O3
						  unary minus -O4
						   unary plus -O5
						   (* / %) -O6
						   (+-) -O7
						   ( << >> ) -O8
						   (< <= > >= )-O9
						   ( ==, != )-O10
						    &   -O11
						    xor  -O12
						    |   -O13
						    &&   -O14
						    ||   -O15

This algorithm should be able to check the type of  operator on top of opstack and calculate the number of operators beneath it which should be popped into postfix stack
The precedence of unary minus or unary plus is unknown and higher than MUL or DIV operators.A table or bitmap can be used to do this.

Order of precedence of operators as in higher to lower is:
(Operators within () have same precedence).
(sin,cos,tan,sec,cosec,cot,log,pow), ~,!,unary minus, unary plus, (* / %), (+-) , ( << >> ), (< <= > >= ), ( ==, != ) , &, |, &&, ||
Source: Page 120, chapter 6 in The C++ programming language - Bjarne stroustrup, 3rd edition.

Description about EvaluateExpressions:
This project is for evaluating arithmetic expressions such as sqrt(loge((3+sin(5.6))/7+(2*8)/cos(9.1-1))) using binary trees .The result should be 1.67619. The post order evaluation algorithm
would be used to evaluate a binary tree constructed from expressions as above. A variant data type would be use for storing characters which are
decimal and floating point numbers or operators "/ * - + ^" characters. Each expression within ( ) would be considered as a binary subtree and
evaluated if there are operands and added to the main binary tree as an operand. Precedence of operators should be observed as  % * / greater than  + - .
Each node as it is added to the tree would be configured as an operand or operator. The numeric type of each operand is detected and numeric data is stored
in a variant type which is configured as a node.The numeric types of operands detected are int, unsigned int, long int, unsigned long, float,
double, long double.If correct syntax of a arithmetic expression is not entered then a syntax error would be displayed. Make use of terminology as
Terminals,nonterminals and grammar in Compilers-practices,techniques and tools -AUSL to construct expression tree. The code in this project would
be reused in macro processor and compiler project.Design and develop each and every module, component according to specification and integrate.


The tree would be constructed as below:

 *
 *                                            +
 *                                      /            \
 *                                    /               /
 *                                  / \             /     \
 *                               +      7         *         -
 *                              / \     / \      /  \      /   \
 *                             3  5.6  N   N    2    8    9.1   1
 *                            /\   /\          / \   /\   /\   / \
 *                           N N  N  N         N  N  N N N  N  N  N
 *
 * Notes:
 * N means nullptr in the above diagram
 * Modify the headers if necessary to write code according to grammar.
 *
 * Known Issues:
   Compiler option -std=c++98 is pointing to ISO C90 - bug in IDE
   Compiler option -std=c++03 is pointing to ISO C90 - bug in IDE
   DEV C++ 5.1 IDE is buggy, settings are sometimes corrupted. Make file configuration isn't the same as in settings. Serious Bug in IDE.
   Debugger is not executing in IDE.

 *
 * Unknown issues:


Estimated effort:
------------------------
SRS - 10 hours
SDS - 10 hours
STS - 5 hours
Defining Software architecture -       20 hours
Deciding on storage classes such as
static and extern functions or
Datas,linkage of modules -             1 hours
Input data vector                      1 hours
Data structures for converting
data types and arithmetic
storing Data                            10 hours
C++ syntax keywords, symbols and
Digits look up                          1 hours
Line number and column number integration
in all modules                          5 hours
Testcase to check definition of
Lexer design and implementation        20 hours
Testcase to check definition            2 hours
of Lexer
Stack design and implementation        10 hours
Testcase for stack                      2 hours
Parser design and implementation       20 hours
RPN stack construction                 20 hours
RPN stack evaluator                    10 hours.
Testcase to check definition            2 hours
of Parser
Evaluator for evaluating expressions   20 hours
Testcase to check definition
of Evaluator function                   2 hours
Convert all // comments to c-style
comments                                2 hours
Modifying the project to perform
only arithmetic expresssions
evaluations                            5 hours.


Breakdown of project modules:
----------------------------------
Input data types                       1hour
Data structures for converting         5 hours
data types and arithmetic
Preprocessor core                      5 hours
Lexer design and implementation  -    10 hours
Stack design and implementation  -     2 hours
Parser design and implementation      20 hours
Construction of Tree                  20 hours
Evaluator for evaluating
expressions                      -    20 hours

Coding log:
-----------
21-01-17  Evaluate expressions project is taken as a baseline.

28-05-17 Added loge operators on Valueleft.doublevalue for double data type in ComputeArithmeticonTokens() function.
	Created a release version.
*/

#include "std_types.h"
#include "Tree.h"
#include "parser.h"
#include "DataStructures.h"
#include <iostream>
#include "Evaluator.h"
#include "Modules.h"
#include "EvaluateExpressions.h"
#include "Error.h"
#include "Stack.h"
#include "cstring"
#include <cmath>

#if defined (EVALUATION_METHOD_EXPRESSIONTREE)
extern Tnode* Root;
#endif

/* Get vector for storing tokens */
extern std::vector <Token_tag> TokenList;

/* Uncomment to integrate with a project . Comment to test*/
//#if(0)

SINT32 main(void)
{
	InitModules();
	ReversePostfixStack();
    #if defined (DEVELOPMENT_DEBUGGING_ENABLED)
	PrintPostfixStack();
    #endif
	std::cout << "Starting arithemtic expression evaluator " << std::endl;
    #if defined (EVALUATION_METHOD_RPN)
	EvaluatePostfixExpression();
	MemFreeTokenList(TokenList);
    #elif defined (EVALUATION_METHOD_EXPRESSIONTREE)
	// This function constructs a binary expression tree
	ConstructBinaryExpressionTree();
	EvaluateExpression();
    #endif
	return 0;
}
//#endif


#if defined (EVALUATION_METHOD_EXPRESSIONTREE)

/*
Component Function: void EvaluateExpression(void)
Arguments: Syntax Tree
Returns : None
Description:
This function evaluates a Tree.
Version and bug history:
0.1 Initial version.
Notes:
 */
void EvaluateExpression(void)
{
	Variant val;
	UINT32 ErrorsFound = NO_ERRORS;
	if ((Root != nullptr))
	{
		//std::cout << "Before calling evaluate expression tree" << std::endl;
		val = EvaluateExpressionTree(Root);
		ErrorsFound = CheckforErrors();
		if (ErrorsFound == NO_ERRORS)
		{
			switch (val.Type)
			{
			case SHORTTYPE:
				//std::cout << "Type is short type" << std::endl;
				std::cout << "Value computed is " << val.u.shortValue << std::endl;
				break;
			case USHORTTYPE:
				//std::cout << "Type is ushort type" << std::endl;
				std::cout << "Value computed is " << val.u.ushortValue << std::endl;
				break;
			case INTTYPE:
				//std::cout << "Type is int type" << std::endl;
				std::cout << "Value computed is " << val.u.intValue << std::endl;
				break;
			case UINTTYPE:
				//std::cout << "Type is uint type" << std::endl;
				std::cout << "Value computed is " << val.u.uintValue << std::endl;
				break;
			case LONGINTTYPE:
				//std::cout << "Type is long int type" << std::endl;
				std::cout << "Value computed is " << val.u.longValue << std::endl;
				break;
			case ULONGINTTYPE:
				//std::cout << "Type is ulong type" << std::endl;
				std::cout << "Value computed is " << val.u.ulongValue << std::endl;
				break;
			case FLOATTYPE:
				//std::cout << "Type is float type" << std::endl;
				std::cout << "Value computed is " << val.u.floatValue << std::endl;
				break;
			case DOUBLETYPE:
				//std::cout << "Type is double type" << std::endl;
				std::cout << "Value computed is " << val.u.doubleValue << std::endl;
				break;
			default:
				break;
			}
			DeleteBinaryTree(Root);
		}
		else
		{
			PrintErrors();
		}

	}
}


#endif // #if defined (EVALUATION_METHOD_EXPRESSIONTREE)



#if defined (EVALUATION_METHOD_RPN)

/*
Component Function: void EvaluatePostfixExpression(void)
Arguments: None
Returns : None
Description:
This function evaluates a stack in RPN notation.
Version and bug history:
0.1 Initial version.
Notes:
To do:
 */
void EvaluatePostfixExpression(void)
{
	Token_tag CurrentToken;
	Token_tag Tokenone;
	Token_tag Tokentwo;
	Token_tag ResultToken;
	ClearToken(CurrentToken);
	ClearToken(ResultToken);
	Variant Operandone, Operandtwo;
	Variant Value;
	UINT32 ErrorsFoundinExpression = CheckforErrors();
	while (!isPostfixStackEmpty() && ErrorsFoundinExpression == NO_ERRORS)
	{
		if (PopTokenPostfixStack(CurrentToken))
		{
			//PrintToken(CurrentToken);
			switch (CurrentToken.TokenDataType)
			{
			case USHORTTYPE:
			case SHORTTYPE:
			case INTTYPE:
			case UINTTYPE:
			case LONGINTTYPE:
			case ULONGINTTYPE:
			case FLOATTYPE:
			case DOUBLETYPE:
			case LONGDOUBLETYPE:
			case IDENT:
				PushTokenArithmeticStack(CurrentToken);
				//std::cout << "\nGot an operand ";
				break;
			case OPMUL:
			case OPDIV:
			case OPPLUS:
			case OPMINUS:
			case OPMODULUS:
			case OPBITWISEAND:
			case OPBITWISEOR:
			case OPLOGICALAND:
			case OPLOGICALOR:
			case OPGREATERTHANEQUALTO:
			case OPLESSTHANEQUALTO:
			case OPGREATERTHAN:
			case OPLESSTHAN:
			case OPLEFTBITSHIFT:
			case OPRIGHTBITSHIFT:
			case OPLOGICALISEQUALTO:
			case OPLOGICALISNOTEQUALTO:
			case OPPOWEXPONENT:
			case OPBITWISEXOR:
				if (PopTokenArithmeticStack(Tokenone))
				{
					if (PopTokenArithmeticStack(Tokentwo))
					{
						Operandone = Tokentwo.Data;
						Operandtwo = Tokenone.Data;
						Operandone.Type = Tokentwo.TokenDataType;
						Operandtwo.Type = Tokenone.TokenDataType;
						ComputeArithmeticOnTokens(CurrentToken.Data.u.cString, Operandone, Operandtwo, Value);
						GetResult(ResultToken, Value);
						PushTokenArithmeticStack(ResultToken);
					}
					else
					{
                        // Allow some internal errors to not have line numbers.Line and column numbers are mostly useful for syntax errors.
                        ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
					}
				}
				else
				{
                    // Allow some internal errors to not have line numbers.Line and column numbers are mostly useful for syntax errors.
                    ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
				}
				break;
			case OPUNARYMINUS:
				// convert types for unary minus.
				if (PopTokenArithmeticStack(Tokenone))
				{
					Operandone = Tokenone.Data;
					Operandone.Type = Tokenone.TokenDataType;
					// Copy value by casting that value into converted type.
					switch (Tokenone.TokenDataType)
					{
					case INTTYPE:
						Operandone.u.intValue = 0;
						break;
					case SHORTTYPE:
						Operandone.Type = INTTYPE;
						Operandone.u.intValue = 0;
						break;
					case USHORTTYPE:
						Operandone.Type = INTTYPE;
						Operandone.u.intValue = 0;
						break;
					case UINTTYPE:
						Operandone.Type = LONGINTTYPE;
						Operandone.u.longValue = 0;
						break;
					case LONGINTTYPE:
						Operandone.u.longValue = 0;
						break;
					case ULONGINTTYPE:
						Operandone.Type = FLOATTYPE;
						Operandone.u.floatValue = 0;
						break;
					case FLOATTYPE:
						Operandone.u.floatValue = 0;
						break;
					case DOUBLETYPE:
						Operandone.u.doubleValue = 0.0;
						break;
					default:
						break;
					}

					Operandtwo = Tokenone.Data;
					Operandtwo.Type = Tokenone.TokenDataType;
					// Convert operand two to operand one with this function.
					ComputeArithmeticOnTokens(CurrentToken.Data.u.cString, Operandone, Operandtwo, Value);
					GetResult(ResultToken, Value);
					//PrintToken(ResultToken);
					PushTokenArithmeticStack(ResultToken);
				}
				else
				{
					// Allow some internal errors to not have line numbers.Line and column numbers are mostly useful for arithmetic syntax errors.
					ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
				}

				break;
			case OPSIN:
			case OPCOS:
			case OPTAN:
			case OPSEC:
			case OPCOT:
			case OPCOSEC:
			case OPSQRT:
			case OPLOG:
				// convert types for  trignometric functions
				if (PopTokenArithmeticStack(Tokenone))
				{
					Operandone = Tokenone.Data;
					Operandone.Type = Tokenone.TokenDataType;

					Operandtwo = Tokenone.Data;
					Operandtwo.Type = Tokenone.TokenDataType;
					// Convert operand two to operand one with this function.
					ComputeArithmeticOnTokens(CurrentToken.Data.u.cString, Operandone, Operandtwo, Value);
					GetResult(ResultToken, Value);
					//PrintToken(ResultToken);
					PushTokenArithmeticStack(ResultToken);
				}
				else
				{
					// Allow some internal errors to not have line numbers.Line and column numbers are mostly useful for arithmetic syntax errors.
					ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
				}
				break;
			case OPLOGICALNOT:
			case OPCOMPLEMENT:
				// convert types for  trignometric functions
				if (PopTokenArithmeticStack(Tokenone))
				{
					Operandone = Tokenone.Data;
					Operandone.Type = Tokenone.TokenDataType;

					Operandtwo = Tokenone.Data;
					Operandtwo.Type = Tokenone.TokenDataType;

					ComputeArithmeticOnTokens(CurrentToken.Data.u.cString, Operandone, Operandtwo, Value);
					GetResult(ResultToken, Value);
					//PrintToken(ResultToken);
					PushTokenArithmeticStack(ResultToken);
				}
				else
				{
					// Allow some internal errors to not have line numbers.Line and column numbers are mostly useful for arithmetic syntax errors.
					ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
				}

                break;
			default:
                break;
			}
		}
		else
		{
            // Allow some internal errors to not have line numbers.Line and column numbers are mostly useful for syntax errors.
            //ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
		}
	}
	ErrorsFoundinExpression = CheckforErrors();
	if (!isArithmeticStackEmpty() && ErrorsFoundinExpression == NO_ERRORS)
	{
        PopTokenArithmeticStack(ResultToken);
        PrintResult(ResultToken);
	}
	else if (ErrorsFoundinExpression == EVALUATION_ERRORS)
	{
        PrintErrors();
	}
}
#endif // #if defined (EVALUATION_METHOD_RPN)
