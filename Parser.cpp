/*
Name :  Parser.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module initliazes parser
Notes:

Bug and Version history:
0.1 Initial version
Notes: construct syntax tree or binary expression tree.
The parser would get each token and construct a tree. It would either add each token into a tree or evaluate that tree which is being
constructed.This is based on token newline and if any '=' operator was present in the expression.

If '=' is detected in expression, then symbol is updated in symbol table or else the expression is evaluated after Newline token.

Operator table in decreasing order of precedence
-------------------------------------------------
(sin,cos,tan,sec,cosec,cot,loge, pow) -O1
								   ~  -O2
								   !  -O3
						  unary minus -O4
						   unary plus -O5
							  (* / %) -O6
								 (+-) -O7
							( << >> ) -O8
						  (< <= > >= )-O9
							( ==, != )-O10
								  &   -O11
								 xor  -O12
								  |   -O13
								 &&   -O14
								 ||   -O15

								  =	  -O16      Direct assignment (provided by default for C++ classes)
							+=   -=	  -O17      Compound assignment by sum and difference
						  *=   /=   %= -O18	    Compound assignment by product, quotient, and remainder
							 <<=   >>= -O19	    Compound assignment by bitwise left shift and right shift
						   &=   ^=  |= -O20

To do:

Add operators:
								  =	  -O16      Direct assignment (provided by default for C++ classes)
							+=   -=	  -O17      Compound assignment by sum and difference
						  *=   /=   %= -O18	    Compound assignment by product, quotient, and remainder
							 <<=   >>= -O19	    Compound assignment by bitwise left shift and right shift
						   &=   ^=  |= -O20



Notes:
Shorten parseTokenstream() use a bitmap or other data structure to store operators in precedence and detect precedence 
of incoming op type with existing op in RPN stack.
*/
#include "Parser.h"

/* Declare external scope */
extern Scopes* CurrentScope;

#if(0)
/* Comment to test module. Uncomment to integrate */
SINT32 main(void)
{
	Parser::InitParser();
	return 0;
}
#endif


/*
 Component Function: void Parser::InitParser(void)
 Arguments: None
 Returns: None
 Description:
  Initializes the parser
  Version : 0.1
*/
void Parser::InitParser(void)
{
	/* First try with stack, then a linked list */
	BOOL equaltoopdetected = false;
	InitTokenStack();
	Lexer::Token_tag Token;
	while (1)
	{
		Token = Lexer::getNextToken();
		if (Token.Type == Lexer::TokenType::op)
		{
			UINT32 OpType = Lexer::DetectOpType(Token.Data.u.cString);
			if (OpType == OPEQUALTO || OpType == OPADDEQUALTO || OpType == OPMINUSEQUALTO || OpType == OPMODEQUALTO \
				|| OpType == OPDIVEQUALTO || OpType == OPCOMPLEMENTEQUALTO || OpType == OPMULTIPLYEQUALTO)
			{
				equaltoopdetected = true;
			}
		}
		else if (Token.Type == Lexer::TokenType::command)
		{
			if (strcmp("exit", strlwr(Token.Data.u.cString)) == 0)
			{
				Lexer::PrintToken(Token);
				delete[] Token.Data.u.cString;
				Token.Data.u.cString = nullptr;
				while (PopTokenStack(Token))
				{
					if (Token.Data.u.cString)
					{
						delete[] Token.Data.u.cString;
						Token.Data.u.cString = nullptr;
					}
				}

				break;
			}
			else if (strcmp("clear", strlwr(Token.Data.u.cString)) == 0)
			{
				Lexer::PrintToken(Token);
				delete[] Token.Data.u.cString;
				Token.Data.u.cString = nullptr;
				CurrentScope->Delete(CurrentScope->GetCurrentLevel());
			}
		}
		else if (Token.Type == Lexer::TokenType::endofline)
		{
			delete[] Token.Data.u.cString;
			Token.Data.u.cString = nullptr;
			ReverseTokenStack();
			std::cout << "Printing token stack of incoming token stream" << std::endl;
			while (!isTokenStackEmpty())
			{
				/* Delete memory allocated for char strings */
				/* detect = op and construct expression tree.Print result only if = operator is not detected.*/
				/* Detect a=b=1 type expressions */
				/* Construct expression tree consisely? */
				PopTokenStack(Token);
				/* Construct Postfix stack with Token Stack */
			}

			if (!equaltoopdetected)
			{
				// Print result 
				PrintPostfixStack();
				/* Evaluate stree and print it */
			}

		}
	}
}


/*
Use a stack to push all tokens into a stack.
construct expression tree after detecting  = operator
Assign result of expression to lvalue of = operator
if there is no  = operator, construct and evalulate expression tree.
if there are one or more idents in expression, look up their value from symbol table, consrruct a tree, evaluate it and print it.
if there are one or more operands in expresssion, construct tree, evaluate and print it.

*/


/*
Component Function: void  Parser::ParseTokenstream(Lexer::Token_tag& CurrentToken)
Arguments: Token stream
Returns: None
Version : 0.1 Initial version.
Description:
Notes:
To do:
Add operators:
								  =	  -O16      Direct assignment (provided by default for C++ classes)
							+=   -=	  -O17      Compound assignment by sum and difference
						  *=   /=   %= -O18	    Compound assignment by product, quotient, and remainder
							 <<=   >>= -O19	    Compound assignment by bitwise left shift and right shift
						   &=   ^=  |= -O20

*/
void  Parser::ParseTokenstream(Lexer::Token_tag& CurrentToken)
{
	Lexer::Token_tag Temp;
	Lexer::Token_tag OpTemp;
	BOOL IsUnaryMinus = false;
	BOOL Unaryplus = false;
	static Lexer::Token_tag NextToken;
	static Lexer::Token_tag PreviousToken;
	static BOOL execonce = false;
	UINT32 OpType = NOTOP;
	Lexer::ClearToken(Temp);
	Lexer::ClearToken(OpTemp);

	if (execonce == false)
	{
		Lexer::ClearToken(NextToken);
		Lexer::ClearToken(PreviousToken);
		PreviousToken.Type = Lexer::TokenType::Emptytoken;
		NextToken.Type = Lexer::TokenType::Emptytoken;
		execonce = true;
	}
	/* Get Next Token */
	
	if (PopTokenStack(NextToken))
	{
		PushTokenStack(NextToken);
	}
	else
	{
		NextToken.Type = Lexer::TokenType::Emptytoken;
	}
    /* Write shuting yard algorithm here */
	switch (CurrentToken.Type)
	{
	    case Lexer::TokenType::separator_rightParen:
		if (NextToken.Type == Lexer::TokenType::separator_leftParen || NextToken.Type == Lexer::TokenType::identifier)
		{
			//PushTokenPostfixStack(Temp);  // start of expresssion 
		}
		else
		{

			if (PopTokenOpStack(Temp))
			{
				PushTokenOpStack(Temp);
				/* This line should be checked */
				while (!isOpStackEmpty())
				{
					PopTokenOpStack(OpTemp);
					if ((OpTemp.Type == Lexer::TokenType::separator_leftParen))
					{
						break;
					}
					else if (OpTemp.Type != Lexer::TokenType::separator_leftParen)
					{
						PushTokenPostfixStack(OpTemp);
					}
				}
			}
			else
			{
				/* Allow some internal errors to not have line numbers.Line and column numbers are mostly useful for syntax errors.
				ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
				*/
			}

		}
		break;

	case Lexer::TokenType::separator_leftParen:
		/* std::cout << "\nDetected Left parenthesis"; */
		PushTokenOpStack(CurrentToken);
		break;

	case Lexer::TokenType::numericliteral:
	case Lexer::TokenType::identifier:
		PushTokenPostfixStack(CurrentToken);
		break;

	case Lexer::TokenType::op:
		OpType = DetectOpToken(CurrentToken);
		switch (OpType)
		{
		case OPPLUS:
			/*
		std::cout << "\nDetected an operator O1 " << Token.Data.u.cString;
		*/
			Lexer::ClearToken(Temp);
			/*
			 Unary plus code begin
			 Change this code. The unary minus  or plus can also be pushed into op stack for correct arithmetic on a expression as -1-1 , +1+-1
			 */
			 /* #if(0) */
				/*
				Look before the token and lookahead after the token to detect unary plus.
				for +1+1
				*/
			if ((PreviousToken.Type == Lexer::TokenType::separator_leftParen) && (DetectOpToken(CurrentToken) == OPPLUS) &&
				((NextToken.Type == Lexer::TokenType::numericliteral) || (NextToken.Type == Lexer::TokenType::identifier)))
			{
				Unaryplus = true;
				/* std::cout << "\nOperator o1 is unary plus"; */
			} /* for (+1+2) */

			else if ((DetectOpToken(CurrentToken) == OPPLUS) && ((NextToken.Type == Lexer::TokenType::numericliteral) || \
				(NextToken.Type == Lexer::TokenType::identifier)))
			{
				Unaryplus = true;
				/* std::cout << "\nOperator o1 is unary plus"; */
			}/* for +(1+2) */
			else if ((DetectOpToken(CurrentToken) == OPPLUS) && (NextToken.Type == Lexer::TokenType::separator_leftParen))
			{
				Unaryplus = true;
				//std::cout << "\nOperator o1 is unary minus";
			}// for 1++1
			else if ((Lexer::isValidOp(DetectOpToken(PreviousToken))) && (DetectOpToken(CurrentToken) == OPPLUS))
			{
				Unaryplus = true;
				//std::cout << "\nOperator o1 is unary plus";
			}// for +sin(90)
			else if (((DetectOpToken(NextToken) == OPSIN) || (DetectOpToken(NextToken) == OPCOS) || (DetectOpToken(NextToken) == OPTAN) || (DetectOpToken(NextToken) == OPSEC) \
				|| (DetectOpToken(NextToken) == OPCOSEC) || (DetectOpToken(NextToken) == OPCOT) || (DetectOpToken(NextToken) == OPSQRT) || (DetectOpToken(NextToken) == OPLOG) || (DetectOpToken(NextToken) == OPPOWEXPONENT)))
			{
				Unaryplus = true;
				//std::cout << "\noperator o1 is unary minus";
			}

			if (Unaryplus)
			{
				//CurrentToken.Type = 
				// This block is from a previous baseline.
				//CurrentToken.Data.u.cString = TokenStream[index].Data.u.cString;
			}
			//else
			{

				if (PopTokenOpStack(Temp))
				{
					if (DetectOpToken(Temp) == OPLOGICALOR)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPLOGICALAND)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPBITWISEOR)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPBITWISEXOR)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPBITWISEAND)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);


					}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
					else if (DetectOpToken(Temp) == OPMINUS || DetectOpToken(Temp) == OPPLUS)
					{
						// Push operator o2 into op stack.
						PushTokenOpStack(Temp);
						// Pop operator o2 onto output stack.
						//modified code here

						PopTokenOpStack(OpTemp);
						PushTokenPostfixStack(OpTemp);
						// modified code end


						PushTokenOpStack(CurrentToken);
					}
					else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
					{
						/* Push token back into op stack. */
						PushTokenOpStack(Temp);
						// Pop operator o2 onto output stack.

						// This check seems unnecessary
						//modified code here

						PopTokenOpStack(OpTemp);
						PushTokenPostfixStack(OpTemp);
						// modified code end
						// One check should be here to compare precedence of exisiting op O2 on top of op stack with incoming op o1.
						// Check precedence of op on top of opstack with incoming op and keep on popping operators from op stack
						// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
						// Push operator o1 into opstack
						// This is when incoming operator is + or - and ops on top of opstack are + or -

						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}
						}
						PushTokenOpStack(CurrentToken);

					}// Unary minus code begin
					else if (DetectOpToken(Temp) == OPUNARYMINUS)
					{
						//std::cout << "\nOperator on top of op stack is unary minus";
						/* Push O2 back into op stack. */
						PushTokenOpStack(Temp);
						// Pop operator o2 onto output stack.
						//#if(0)
						//modified code here
						PopTokenOpStack(OpTemp);
						PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.
						// modified code end
						// modified code end
						//#endif
						// Check if the op beneath a unary minus is a / or */
						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}
						}
						//PushTokenOpStack(CurrentToken);

						// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
						// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
						// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
						// This is when incoming operator is + or - and ops at top of opstack are + or -
						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}
						}
						PushTokenOpStack(CurrentToken);

					}// unary minus code end
						// Modified code here
					else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || (DetectOpToken(Temp) == OPPOWEXPONENT))
					{
						//std::cout << "Detected sin or cos in Op stack " << std::endl;
						/* push token back into op stack. */
						PushTokenOpStack(Temp);
						// Pop operator o2 onto output stack.
						PopTokenOpStack(OpTemp);
						PushTokenPostfixStack(OpTemp);
						//std::cout << "Pushed sin or cos into postfix stack " << std::endl;
						// Check one more op on op stack
						// Check if the op beneath a unary minus is a / or */
						// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
						// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
						// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
						// This is when incoming operator is + or - and ops at top of opstack are * or / or + -  or unary minus

						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
							{
								//std::cout << "Detected Unary minus beneath sin or cos " << std::endl;
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}

						}
						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}

						}

						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}
						}
						PushTokenOpStack(CurrentToken);
						// Modified code end
					}
					else if (Temp.Type == Lexer::TokenType::separator_leftParen)
					{
						PushTokenOpStack(Temp);
						PushTokenOpStack(CurrentToken);
					}
				}
				else
				{
					PushTokenOpStack(CurrentToken);
				}
			}
			Unaryplus = false;
			break;
		case OPMINUS:
			// Look before the token and lookahead after the token to detect unary minus.
					// for -1+1 -1-1 -1*1 -1/1
			if ((PreviousToken.Type == Lexer::TokenType::Emptytoken && DetectOpToken(CurrentToken) == OPMINUS) && ((NextToken.Type == Lexer::TokenType::numericliteral) || \
				(NextToken.Type == Lexer::TokenType::identifier)))
			{
				IsUnaryMinus = true;
				//std::cout << "\noperator o1 is unary minus";
			}// for -(1+2)
			else if ((PreviousToken.Type == Lexer::TokenType::Emptytoken&& DetectOpToken(CurrentToken) == OPMINUS) && (NextToken.Type == Lexer::TokenType::separator_leftParen))
			{
				IsUnaryMinus = true;
				//std::cout << "\noperator o1 is unary minus";
			}// for (-1+2)
			else if (((PreviousToken.Type == Lexer::TokenType::separator_leftParen) && (DetectOpToken(CurrentToken) == OPMINUS)) &&
				((NextToken.Type == Lexer::TokenType::numericliteral) || (NextToken.Type == Lexer::TokenType::identifier)))
			{
				IsUnaryMinus = true;
				//std::cout << "\nOperator o1 is unary minus";
			}// if 1+-1 1*-1 1/-1 1--1
			else if (((DetectOpToken(PreviousToken) == OPPLUS) || (DetectOpToken(PreviousToken) == OPMINUS) \
				|| (DetectOpToken(PreviousToken) == OPMUL) || (DetectOpToken(PreviousToken) == OPMODULUS) || (DetectOpToken(PreviousToken) == OPDIV) || \
				(DetectOpToken(PreviousToken) == OPSIN) || (DetectOpToken(PreviousToken) == OPCOS) || (DetectOpToken(PreviousToken) == OPTAN) || (DetectOpToken(PreviousToken) == OPSEC) \
				|| (DetectOpToken(PreviousToken) == OPCOSEC) || (DetectOpToken(PreviousToken) == OPCOT) || (DetectOpToken(PreviousToken) == OPSQRT) || (DetectOpToken(PreviousToken) == OPPOWEXPONENT) || (DetectOpToken(PreviousToken) == OPLOG)) && \
				(DetectOpToken(CurrentToken) == OPMINUS))
			{
				IsUnaryMinus = true;
				//std::cout << "\nOperator o1 is unary minus";
			}// -sin(90)
			else if ((DetectOpToken(CurrentToken) == OPMINUS) && ((DetectOpToken(NextToken) == OPSIN) || (DetectOpToken(NextToken) == OPCOS) || (DetectOpToken(NextToken) == OPTAN) || (DetectOpToken(NextToken) == OPSEC) \
				|| (DetectOpToken(NextToken) == OPCOSEC) || (DetectOpToken(NextToken) == OPCOT) || (DetectOpToken(NextToken) == OPPOWEXPONENT) || (DetectOpToken(NextToken) == OPSQRT)))
			{
				IsUnaryMinus = true;
				//std::cout << "\noperator o1 is unary minus";
			}


			if (IsUnaryMinus)
			{

				delete[] CurrentToken.Data.u.cString;
				SCHAR* Sym = new SCHAR[2];
				Sym[0] = '_';
				Sym[1] = '\0';
				Memcopy(CurrentToken.Data.u.cString, Sym);
				delete[] Sym;
				Sym = nullptr;
				//Token.Data.u.cString = TokenStream[index].Data.u.cString;
				PushTokenOpStack(CurrentToken); // Push unary minus into op stack.
			}//#endif
			else
			{
				Lexer::ClearToken(Temp);
				if (PopTokenOpStack(Temp))
				{
					if (DetectOpToken(Temp) == OPLOGICALOR)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPLOGICALAND)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPBITWISEOR)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPBITWISEXOR)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPBITWISEAND)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
					{
						// Push O2 back into opstack
						PushTokenOpStack(Temp);
						// Push O1 into opstack
						PushTokenOpStack(CurrentToken);

					}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
					else if (DetectOpToken(Temp) == OPPLUS || DetectOpToken(Temp) == OPMINUS)
					{
						PushTokenOpStack(Temp);
						// Pop operator o2 onto output stack.
						//#if(0)
						//modified code here
						PopTokenOpStack(OpTemp);
						PushTokenPostfixStack(OpTemp);
						// modified code end
						//#endif

						PushTokenOpStack(CurrentToken);

					}
					else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
					{
						/* Push O2 back into op stack. */
						PushTokenOpStack(Temp);
						// Pop operator o2 onto output stack.
						//#if(0)
						//modified code here
						PopTokenOpStack(OpTemp);
						PushTokenPostfixStack(OpTemp);
						// modified code end
						//#endif
						// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
						// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
						// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
						// This is when incoming operator is + or - and ops at top of opstack are + or -
						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && ((OpTemp.Type) != Lexer::TokenType::separator_leftParen))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}
						}
						PushTokenOpStack(CurrentToken);
					}// Unary minus code begin
					else if (DetectOpToken(Temp) == OPUNARYMINUS)
					{
						//std::cout << "\nOperator on top of op stack is unary minus";
						/* Push O2 back into op stack. */
						PushTokenOpStack(Temp);
						// Pop operator o2 onto output stack.
						//#if(0)
						//modified code here
						PopTokenOpStack(OpTemp);
						PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

						//#endif


						// Check if the op beneath a unary minus is a / or */
						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && ((OpTemp.Type) != Lexer::TokenType::separator_leftParen))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}
						}
						//PushTokenOpStack(CurrentToken);

						// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
						// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
						// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
						// This is when incoming operator is + or - and ops at top of opstack are + or -
						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}
						}
						PushTokenOpStack(CurrentToken);

					}// unary minus code end
					else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
					{
						/* push token back into op stack. */
						PushTokenOpStack(Temp);
						// Pop operator o2 onto output stack.
						PopTokenOpStack(OpTemp);
						PushTokenPostfixStack(OpTemp);
						// Check one more op on op stack
						// Check if the op beneath a unary minus is a / or */
						// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
						// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
						// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
						// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus

						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}
						}


						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (DetectOpToken(OpTemp) != LEFTPARENTHESIS))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}
						}
						if (!isOpStackEmpty())
						{
							PopTokenOpStack(OpTemp);
							if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
							{
								PushTokenPostfixStack(OpTemp);
							}
							else
							{
								PushTokenOpStack(OpTemp);
							}
						}



						PushTokenOpStack(CurrentToken);
					}
					else if (Temp.Type == Lexer::TokenType::separator_leftParen)
					{
						PushTokenOpStack(Temp);
						PushTokenOpStack(CurrentToken);
					}

				}
				else
				{
					PushTokenOpStack(CurrentToken);
				}


			}
			IsUnaryMinus = false;
			break;
			//sqrt() has same precedence as any function such as sin() cos()
		case OPSIN:
		case OPCOS:
		case OPTAN:
		case OPCOT:
		case OPCOSEC:
		case OPSEC:
		case OPSQRT:
		case OPLOG:
			// This has to be done like unary minus;
		 //std::cout << "Trig operator detected in ParseTokenStream() " << std::endl;

			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);


				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPMINUS || DetectOpToken(Temp) == OPPLUS)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Push operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);

				}// unary minus code end
				else if (DetectOpToken(Temp) == OPCOMPLEMENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Push operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALNOT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Push operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Pop operator o1 onto output stack.
					PushTokenPostfixStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}
			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}


			break;
		case OPCOMPLEMENT:
			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);


				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);


				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPMINUS || DetectOpToken(Temp) == OPPLUS)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Push operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);

				}// unary minus code end

				else if (DetectOpToken(Temp) == OPLOGICALNOT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Push operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPCOMPLEMENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Pop operator o1 onto output stack.
					PushTokenPostfixStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					// Pop operator o1 onto output stack.
					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}
			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}

			break;
		case OPLOGICALNOT:
			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);


				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPMINUS || DetectOpToken(Temp) == OPPLUS)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Push operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);

				}// unary minus code end

				else if (DetectOpToken(Temp) == OPLOGICALNOT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Push operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPCOMPLEMENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Pop operator o1 onto output stack.
					PushTokenPostfixStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					// Pop operator o1 onto output stack.
					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}
			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}

			break;
		case OPDIV:
			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);


				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPMINUS || DetectOpToken(Temp) == OPPLUS)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* push o2 op token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					//#endif

					PushTokenPostfixStack(OpTemp);
					// modified code end

					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.
					// modified code end
					// modified code end
					//#endif

					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or /
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);
				}// unary minus code end
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or unary minus ^
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}

			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
			break;
		case OPMUL:
			Lexer::ClearToken(Temp);
			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);


				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
					//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPMINUS || DetectOpToken(Temp) == OPPLUS)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or /
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);
				}// unary minus code end
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or /
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}




					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}
			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
			break;
		case OPMODULUS:
			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);


				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
					//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPMINUS || DetectOpToken(Temp) == OPPLUS)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or /
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);
				}// unary minus code end
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or /
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && ((OpTemp.Type) != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}




					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}
			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
			break;
		case  OPPOWEXPONENT:
			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);


				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPMINUS || DetectOpToken(Temp) == OPPLUS)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{


					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//PopTokenOpStack(OpTemp);
					//PushTokenPostfixStack(OpTemp);
					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Push operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);


				}// unary minus code end

				else if (DetectOpToken(Temp) == OPCOMPLEMENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Push operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALNOT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Push operator o1 onto op stack.
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || \
					DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Push operator o1 onto output stack.

					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}
			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
			break;

		case OPLEFTBITSHIFT:
		case OPRIGHTBITSHIFT:

			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					PushTokenOpStack(CurrentToken);

				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPPLUS || DetectOpToken(Temp) == OPMINUS)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					//std::cout << "\nOperator on top of op stack is unary minus";
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

					//#endif


					// Check if the op beneath a unary minus is a / or */
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					//PushTokenOpStack(CurrentToken);

					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);

				}// unary minus code end
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}

			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
			break;
		case OPLESSTHAN:
		case OPLESSTHANEQUALTO:
		case OPGREATERTHAN:
		case OPGREATERTHANEQUALTO:
			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPPLUS || DetectOpToken(Temp) == OPMINUS)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					//std::cout << "\nOperator on top of op stack is unary minus";
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

					//#endif


					// Check if the op beneath a unary minus is a / or */
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					//PushTokenOpStack(CurrentToken);

					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);

				}// unary minus code end
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(Temp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(Temp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(Temp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == LEFTPARENTHESIS)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}

			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
			break;
		case OPLOGICALISEQUALTO:
		case OPLOGICALISNOTEQUALTO:

			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPPLUS || DetectOpToken(Temp) == OPMINUS)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					//std::cout << "\nOperator on top of op stack is unary minus";
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

					//#endif


					// Check if the op beneath a unary minus is a / or */
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					//PushTokenOpStack(CurrentToken);

					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);

				}// unary minus code end
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}

			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
			break;
		case OPBITWISEAND:
			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPPLUS || DetectOpToken(Temp) == OPMINUS)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (DetectOpToken(OpTemp) != LEFTPARENTHESIS))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					//std::cout << "\nOperator on top of op stack is unary minus";
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

					//#endif


					// Check if the op beneath a unary minus is a / or */
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					//PushTokenOpStack(CurrentToken);

					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (DetectOpToken(OpTemp) != LEFTPARENTHESIS))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);

				}// unary minus code end
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (DetectOpToken(OpTemp) != LEFTPARENTHESIS))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (DetectOpToken(OpTemp) != LEFTPARENTHESIS))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (DetectOpToken(OpTemp) != LEFTPARENTHESIS))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (DetectOpToken(OpTemp) != LEFTPARENTHESIS))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}

			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
			break;

		case OPBITWISEXOR:
			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// check for bitwise or
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPPLUS || DetectOpToken(Temp) == OPMINUS)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					//std::cout << "\nOperator on top of op stack is unary minus";
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

					//#endif


					// Check if the op beneath a unary minus is a / or */
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					//PushTokenOpStack(CurrentToken);

					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}// unary minus code end
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						// Operator ~
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}




					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}

			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}


			break;
			
		case OPBITWISEOR:
			if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// check for bitwise or
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// check for bitwise or
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPPLUS || DetectOpToken(Temp) == OPMINUS)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					//std::cout << "\nOperator on top of op stack is unary minus";
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

					//#endif


					// Check if the op beneath a unary minus is a / or */
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					//PushTokenOpStack(CurrentToken);

					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}// unary minus code end
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						// Operator ~
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}





					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}

			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
			break;
			
			case OPLOGICALAND:
				if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					// Push O2 back into opstack
					PushTokenOpStack(Temp);
					// Push O1 into opstack
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);

				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPPLUS || DetectOpToken(Temp) == OPMINUS)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					//std::cout << "\nOperator on top of op stack is unary minus";
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

					//#endif


					// Check if the op beneath a unary minus is a / or */
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					//PushTokenOpStack(CurrentToken);

					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);

				}// unary minus code end
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT || DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}

			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
				break;
				
			case OPLOGICALOR:
				if (PopTokenOpStack(Temp))
			{
				if (DetectOpToken(Temp) == OPLOGICALOR)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALAND)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEOR)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEXOR)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPBITWISEAND)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLOGICALISEQUALTO || DetectOpToken(Temp) == OPLOGICALISNOTEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLESSTHAN || DetectOpToken(Temp) == OPLESSTHANEQUALTO || DetectOpToken(Temp) == OPGREATERTHAN || DetectOpToken(Temp) == OPGREATERTHANEQUALTO)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPLEFTBITSHIFT || DetectOpToken(Temp) == OPRIGHTBITSHIFT)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}//std::cout << "\nOperator O2 is " << Temp.Data.u.cString;
				else if (DetectOpToken(Temp) == OPPLUS || DetectOpToken(Temp) == OPMINUS)
				{
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					PushTokenOpStack(CurrentToken);

				}
				else if (DetectOpToken(Temp) == OPDIV || DetectOpToken(Temp) == OPMODULUS || DetectOpToken(Temp) == OPMUL)
				{
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// modified code end
					//#endif
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);
				}// Unary minus code begin
				else if (DetectOpToken(Temp) == OPUNARYMINUS)
				{
					//std::cout << "\nOperator on top of op stack is unary minus";
					/* Push O2 back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					//#if(0)
					//modified code here
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp); // Push unary minus into postfix stack.

					//#endif


					// Check if the op beneath a unary minus is a / or */
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					//PushTokenOpStack(CurrentToken);

					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are + or -
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					PushTokenOpStack(CurrentToken);

				}// unary minus code end
				else if (DetectOpToken(Temp) == OPCOMPLEMENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == OPLOGICALNOT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus




					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);
				}
				else if (DetectOpToken(Temp) == OPSIN || DetectOpToken(Temp) == OPCOS || DetectOpToken(Temp) == OPTAN || \
					DetectOpToken(Temp) == OPSEC || DetectOpToken(Temp) == OPCOSEC || DetectOpToken(Temp) == OPCOT || DetectOpToken(Temp) == OPSQRT \
					|| DetectOpToken(Temp) == OPLOG || DetectOpToken(Temp) == OPPOWEXPONENT)
				{
					/* push token back into op stack. */
					PushTokenOpStack(Temp);
					// Pop operator o2 onto output stack.
					PopTokenOpStack(OpTemp);
					PushTokenPostfixStack(OpTemp);
					// Check one more op on op stack
					// Check if the op beneath a unary minus is a / or */
					// One check should be here to compare precedence of exisiting op  o2 on top of op stack with incoming op o1.
					// Check precedence of op on top of opstack witih incoming op and keep on popping operators from op stack
					// into output stack until there is a op which has a greater precedence on top of op stack compared to incoming op.
					// This is when incoming operator is + or - and ops at top of opstack are * or / or * - or unary minus
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPCOMPLEMENT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLOGICALNOT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPUNARYMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}


					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPMUL || DetectOpToken(OpTemp) == OPMODULUS || DetectOpToken(OpTemp) == OPDIV) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPPLUS || DetectOpToken(OpTemp) == OPMINUS) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);
						if ((DetectOpToken(OpTemp) == OPLEFTBITSHIFT || DetectOpToken(OpTemp) == OPRIGHTBITSHIFT) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLESSTHAN || DetectOpToken(OpTemp) == OPLESSTHANEQUALTO || DetectOpToken(OpTemp) == OPGREATERTHAN || DetectOpToken(OpTemp) == OPGREATERTHANEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALISEQUALTO || DetectOpToken(OpTemp) == OPLOGICALISNOTEQUALTO) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}
					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEXOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPBITWISEOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALAND) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}

					if (!isOpStackEmpty())
					{
						PopTokenOpStack(OpTemp);

						if ((DetectOpToken(OpTemp) == OPLOGICALOR) && (OpTemp.Type != Lexer::TokenType::separator_leftParen))
						{
							PushTokenPostfixStack(OpTemp);
						}
						else
						{
							PushTokenOpStack(OpTemp);
						}
					}



					PushTokenOpStack(CurrentToken);
				}
				else if (Temp.Type == Lexer::TokenType::separator_leftParen)
				{
					PushTokenOpStack(Temp);
					PushTokenOpStack(CurrentToken);
				}

			}
			else
			{
				PushTokenOpStack(CurrentToken);
			}
				break;
			case OPEQUALTO:
				break;
			case OPADDEQUALTO:
			case OPMINUSEQUALTO:
				break;
			case OPMULTIPLYEQUALTO:
			case OPDIVEQUALTO:
			case OPMODEQUALTO:
				break;
			case OPLEFTSHIFTEQUALTO:
			case OPRIGHTSHIFTEQUALTO:
				break;


		    default:
			    break;

		}
		break;
	case Lexer::TokenType::endofline:
		while (!isOpStackEmpty())
		{
			Lexer::ClearToken(Temp);
			PopTokenOpStack(Temp);
			if (Temp.Type != Lexer::TokenType::separator_leftParen)
			{
				PushTokenPostfixStack(Temp);
			}
		}


		break;
	case Lexer::TokenType::Emptytoken:
		break;
	case Lexer::TokenType::error:
		break;
	default:
		break;
	}

	/* Pop all operators from Opstack into postfix stack */
	// print op stack




	Lexer::ClearToken(PreviousToken);
	PreviousToken = CurrentToken;

}