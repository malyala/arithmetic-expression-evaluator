/*
Name:Modules.h
Copyright:
Author:Amit Malyala
Date: 21-11-16 14:17

License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
Initalizes modules
*/

#ifndef MODULES_H
#define MODULES_H

#include "Error.h"
#include "input.h"
#include "output.h"
#include "Stack.h"
#include "Lexer.h"
#include "Parser.h"
#include "Datastructures.h"
#include "EvaluateExpressions.h"

/*
Component Function:  void InitModules(void)
Arguments:  None
returns: None
Description:
Initializes all modules
Version : 0.1
*/
void InitModules(void);

#endif
