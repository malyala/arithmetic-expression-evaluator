/*
Name: Stack.cpp
Author: Amit Malyala
Date: 18-11-16 01:50
Version:
0.01 Initial version
0.02 Created Operator and operand stack
0.03 Changed stack data type from Tnode to Token_tag.

Reference: Book - Aho and Ullman, Foundations of computer science, W.H.Freeman & Co Ltd, 1995.

License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
Stacks implementation using a vector
To do:
Notes:
*/
#include "std_types.h"
#include <iostream>
#include "stack.h"
#include <vector>
#include "Lexer.h"
#include "Error.h"
#include "EvaluateExpressions.h"

/*
 Stacks for Operands and operators which have items of type Token_tag.
*/
#if defined (EVALUATION_METHOD_EXPRESSIONTREE)
  /* A stack for Tnode type data while constructing binary tree*/
static std::stack <Tnode > TreeStack;
#endif // #if defined (EVALUATION_METHOD_EXPRESSIONTREE)

static std::vector<Token_tag> OpStack;

static std::stack <Token_tag> ArithmeticStack;

static std::vector<Token_tag> PostfixStack;
/*
Component Function: static BOOL isPostfixStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isPostfixStackFull(void);
/*
Component Function: static BOOL isArithmeticEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isArithmeticStackFull(void);

#if defined (EVALUATION_METHOD_EXPRESSIONTREE)
/*
Component Function: BOOL isTreeStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if full
Version : 0.1
 */
static BOOL isTreeStackFull(void);
/*
Component Function: static void ClearTreeStack(TStack& pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearTreeStack(std::stack <Tnode> & pS);
#endif // #if defined (EVALUATION_METHOD_EXPRESSIONTREE)
/*
Component Function: static BOOL isOpStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isOpStackFull(void);
/*
Component Function: static void ClearStack(std::vector <Token_tag> & pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
*/
static void ClearStack(std::vector <Token_tag> & pS);

/*
Component Function: static void ClearArithmeticStack(std::stack <Token_tag> & pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearArithmeticStack(std::stack <Token_tag> & pS);

/* Comment to integrate with the project, UnComment to test */
#if(0)
SINT32 main(void)
{
	/* General Init functions for stacks */
	InitStack();
	if (CheckforErrors())
	{
		PrintErrors();
	}
	return 0;
}
#endif

/*
Component Function: void InitStack(void)
Arguments:  None
Returns: None
Description:  Initializes Operator and RPN stack
Version: 0.1 Initial version
 */
void InitStack(void)
{
	ClearStack(PostfixStack);
	ClearStack(OpStack);
	ClearArithmeticStack(ArithmeticStack);
#if defined (EVALUATION_METHOD_EXPRESSIONTREE)
	ClearTreeStack(TreeStack);

#endif
}

/* Function Definitions for composite data in a stack which contains operands and operators for
   RPN stack*/

   /*
   Component Function: static void ClearStack(std::vector <Token_tag> & pS)
   Arguments:  Reference to stack
   Returns: None
   Description:  Clears a stack. Use this at init
   Version : 0.1
   */
static void ClearStack(std::vector <Token_tag> & pS)
{
	pS.clear();
}

#if defined (EVALUATION_METHOD_EXPRESSIONTREE)
/*
Component Function: static void ClearTreeStack(TStack& pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearTreeStack(std::stack <Tnode> & pS)
{
	while (pS.size())
	{
		pS.pop();
	}
}
#endif
/*
Component Function: BOOL isArithmeticEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isArithmeticStackEmpty(void)
{
	return (ArithmeticStack.empty());
}

/*
Component Function: static BOOL isArithmeticEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isArithmeticStackFull(void)
{
	UINT32 size = ArithmeticStack.size();
	UINT32 ReturnValue = false;
	if (size == MAX)
	{
		ReturnValue = true;
	}
	else
	{
		ReturnValue = false;
	}
	return ReturnValue;
}
/*
Component Function: BOOL PopTokenPostfixStack(Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenPostfixStack(Token_tag& px)
{
	BOOL ReturnValue = false;
	if (isPostfixStackEmpty())
	{

		ReturnValue = false;
	}
	else
	{
		px = PostfixStack.back();
		PostfixStack.pop_back();
		ReturnValue = true;
	}
	/*
	  #if defined (STACK_DEBUGGING_ENABLED)
		//std::cout << "\nPos is at " << PostfixStack.Pos << std::endl;
	#endif
	*/
	return ReturnValue;
}

/*
Component Function: BOOL PushTokenPostfixStack(const Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenPostfixStack(const Token_tag& Token)
{
	BOOL ReturnValue = false;
	if (isPostfixStackFull())
	{
		ErrorTracer(STACK_ACCESS_ERROR, Token.LineNumber, Token.ColumnNumber);
		ReturnValue = false;
	}
	else
	{
		PostfixStack.push_back(Token);
		ReturnValue = true;
	}
	return ReturnValue;
}


/*
Component Function: BOOL PopTokenArithmeticStack(Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenArithmeticStack(Token_tag& px)
{
	BOOL ReturnValue = false;
	if (isArithmeticStackEmpty())
	{
		ReturnValue = false;
	}
	else
	{
		px = ArithmeticStack.top();
		ArithmeticStack.pop();
		ReturnValue = true;
	}
	/*
	  #if defined (STACK_DEBUGGING_ENABLED)
		//std::cout << "\nPos is at " << PostfixStack.Pos << std::endl;
	#endif
	*/
	return ReturnValue;
}

/*
Component Function: BOOL PushTokenArithmeticStack(const Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenArithmeticStack(const Token_tag& Token)
{
	BOOL ReturnValue = false;
	if (isArithmeticStackFull())
	{
		ErrorTracer(STACK_ACCESS_ERROR, Token.LineNumber, Token.ColumnNumber);
		ReturnValue = false;
	}
	else
	{
		ArithmeticStack.push(Token);
		ReturnValue = true;
	}
	return ReturnValue;
}

/*
Component Function: static void ClearArithmeticStack(std::stack <Token_tag> & pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearArithmeticStack(std::stack <Token_tag> & pS)
{
	while (pS.size())
	{
		pS.pop();
	}
}

/*
Component Function: BOOL PopTokenOpStack(Token_tag& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL PopTokenOpStack(Token_tag& px)
{
	BOOL ReturnValue = false;
	if (isOpStackEmpty())
	{

		ReturnValue = false;
	}
	else
	{
		px = OpStack.back();
		OpStack.pop_back();
		ReturnValue = true;
	}
	/*
	  #if defined (STACK_DEBUGGING_ENABLED)
		//std::cout << "\nPos is at " << pS.Pos << std::endl;
	#endif
	*/
	return ReturnValue;
}

/*
Component Function: static BOOL isOpStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isOpStackFull(void)
{
	UINT32 size = OpStack.size();
	BOOL ReturnValue = false;
	if (size == 100)
	{
		ReturnValue = true;
	}
	return  ReturnValue;
}

/*
Component Function: BOOL PushTokenOpStack(const Token_tag& Token)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL PushTokenOpStack(const Token_tag& Token)
{
	BOOL ReturnValue = false;
	if (isOpStackFull())
	{
		ErrorTracer(STACK_ACCESS_ERROR, Token.LineNumber, Token.ColumnNumber);
		ReturnValue = false;
	}
	else
	{
		OpStack.push_back(Token);
		ReturnValue = true;
	}
	return ReturnValue;
}
/*
Component Function: BOOL isOpStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isOpStackEmpty(void)
{
	return  OpStack.empty();
}

/*
Component Function: BOOL isPostfixStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isPostfixStackEmpty(void)
{
	return PostfixStack.empty();
}
/*
Component Function: static BOOL isPostfixStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isPostfixStackFull(void)
{
	UINT32 size = PostfixStack.size();
	BOOL ReturnValue = false;
	if (size == MAX)
	{
		ReturnValue = true;
	}
	return ReturnValue;
}

#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
/*
Component Function: void PrintOpStack(void)
Arguments:  None
Returns: None
Description:  Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintOpStack(void)
{
	DisplayOpStack(OpStack);
}

/*
Component Function: void DisplayOpStack(const std::vector <Token_tag> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:
Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void DisplayOpStack(const std::vector <Token_tag> &pS)
{
	//std::cout << "\nOperator stack is:";
	SINT32 index = 0;
	if (!(isOpStackEmpty()))
	{
            for (index = pS.size() - 1; index >= 0; index--)
            {
		if (pS[index].TokenDataType == LEFTPARENTHESIS)
		{
                    std::cout << "\nLeft Parenthesis " << pS[index].Data.u.cString;
		}
		else if (pS[index].TokenDataType == OPUNARYMINUS)
		{
                    std::cout << "\nUnary minus " << pS[index].Data.u.cString;
		}
		else
                {
                    std::cout << "\nOperator " << pS[index].Data.u.cString;
                }
            }
	}
	else
	{
		std::cout << "\nOperatorStack empty";

	}
}

/*
Component Function: void PrintPostfixStack(void)
Arguments:  None
Returns: None
Description:  Displays Postfix stack.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintPostfixStack(void)
{
	//std::cout << "in PrintPostfixStack" << std::endl;
	DisplayPostfixStack(PostfixStack);
}

/*
Component Function: void DisplayPostfixStack(const std::vector <Token_tag> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:  Displays Postfix stack.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void DisplayPostfixStack(const std::vector <Token_tag> &pS)
{
	std::cout << "Postfix stack is:" << std::endl;
	SINT32 index = 0;
	if (!(isPostfixStackEmpty()))
	{
		for (index = pS.size() - 1; index >= 0; index--)
		{
			// Use this code while filling Postfix and operator stack.
			switch (pS[index].TokenDataType)
			{
			case LEFTPARENTHESIS:
				std::cout << "Token Left parenthesis " << pS[index].Data.u.cString << std::endl;
				break;
			case RIGHTPARENTHESIS:
				std::cout << "Token Right parenthesis " << pS[index].Data.u.cString << std::endl;
				break;
			case SHORTTYPE:
				std::cout << "Short Value " << pS[index].Data.u.shortValue << std::endl;
				break;
			case USHORTTYPE:
				std::cout << "int Value " << pS[index].Data.u.ushortValue << std::endl;
				break;
			case INTTYPE:
				std::cout << "int Value " << pS[index].Data.u.intValue << std::endl;
				break;
			case UINTTYPE:
				std::cout << "uint Value " << pS[index].Data.u.uintValue << std::endl;
				break;
			case LONGINTTYPE:
				std::cout << "long Value " << pS[index].Data.u.longValue << std::endl;
				break;
			case ULONGINTTYPE:
				std::cout << "ulong Value " << pS[index].Data.u.ulongValue << std::endl;
				break;
			case FLOATTYPE:
				std::cout << "float Value " << pS[index].Data.u.floatValue << std::endl;
				break;
			case DOUBLETYPE:
				std::cout << "double Value " << pS[index].Data.u.doubleValue << std::endl;
				break;
			case LONGDOUBLETYPE:
				std::cout << "\nLong double Value " << pS[index].Data.u.longdoubleValue << std::endl;
				break;
			case OPPLUS:
			case OPMINUS:
			case OPDIV:
			case OPMUL:
			case OPMODULUS:
			case OPPOWEXPONENT:
			case OPSIN:
			case OPCOS:
			case OPTAN:
			case OPCOT:
			case OPSEC:
			case OPCOSEC:
			case OPSQRT:
			case OPLOG:
			case OPLOGICALAND:
			case OPLOGICALOR:
			case OPLOGICALNOT:
			case OPBITWISEOR:
			case OPBITWISEAND:
			case OPGREATERTHAN:
			case OPLESSTHAN:
			case OPLEFTBITSHIFT:
			case OPRIGHTBITSHIFT:
			case OPLESSTHANEQUALTO:
			case OPGREATERTHANEQUALTO:
			case OPLOGICALISEQUALTO:
			case OPLOGICALISNOTEQUALTO:
			case OPBITWISEXOR:
			case OPCOMPLEMENT:

				std::cout << "Operator " << pS[index].Data.u.cString << std::endl;
				break;
			case OPUNARYMINUS:
				std::cout << "Operator Unary minus " << pS[index].Data.u.cString << std::endl;
				break;
				break;
			default:
				break;
			}
		}
	}
	else
	{
		//std::cout << "Output stack empty" << std::endl;
	}
}
#endif // #if defined (DEVELOPMENT_DEBUGGING_ENABLED)
/*
Component Function: void ReversePostfixStack(void)
Arguments:  None
Returns: Nothing
Description:  Reverses a postfix stack in RPN notation.
Version : 0.1
 */
void ReversePostfixStack(void)
{
	SINT32 index = 0;
	Token_tag Temp;
	SINT32 j = 0;
	for (j = 0, index = (PostfixStack.size() - 1); j < index; --index, ++j)
	{
		Temp = PostfixStack[index];
		PostfixStack[index] = PostfixStack[j];
		PostfixStack[j] = Temp;
	}

}
#if defined (EVALUATION_METHOD_EXPRESSIONTREE)

#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
/*
Component Function: void PrintTreeStackPos(void)
Arguments:  None
Returns: None
Description:  Displays Tree stack top pos
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintTreeStackPos(void)
{
	//std::cout << "TreeStack top pos at " << TreeStack.Pos << std::endl;
}
#endif
/*
Component Function: BOOL isTreeStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL isTreeStackEmpty(void)
{
	BOOL ReturnValue = false;

	if (TreeStack.empty())
	{
		ReturnValue = true;
	}
	else
	{
		ReturnValue = false;
	}
	return ReturnValue;
}

/*
Component Function: BOOL isTreeStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if full
Version : 0.1
 */
static BOOL isTreeStackFull(void)
{
	BOOL ReturnValue = false;
	UINT32 size = TreeStack.size();
	if (size == MAX)
	{
		ReturnValue = true;
	}
	else
	{
		ReturnValue = false;
	}
	return ReturnValue;
}
/*
Component BOOL PushNode(Tnode& aNode)
Arguments: Tree node being pushed
Returns: true or not
Description:  Returns true if push is successful
Version : 0.1
 */
BOOL PushNode(Tnode& aNode)
{
	BOOL ReturnValue = false;
	if (isTreeStackFull())
	{
		ErrorTracer(STACK_ACCESS_ERROR, aNode.ColumnNumber, aNode.ColumnNumber);
		ReturnValue = false;
	}
	else
	{
            TreeStack.push(aNode);

            //std::cout <<"Node being pushed: " << std::endl;
            //PrintTreeNode(aNode);
            ReturnValue = true;
	}

#if defined (STACK_DEBUGGING_ENABLED)
	//std::cout << "TstackVector nodeindex after pushing node is at " << NodeIndex << std::endl;
#endif
	return ReturnValue;
}

/*
Component Function: BOOL PopNode(Tnode& aNode)
Arguments: Tree node being pushed
Returns: true or not
Description:  Returns true if push is successful
Version : 0.1
*/
BOOL PopNode(Tnode& aNode)
{
	BOOL ReturnValue = false;
	if (isTreeStackEmpty())
	{
		ReturnValue = false;
	}
	else
	{
		aNode = TreeStack.top();
		TreeStack.pop();
		//std::cout <<"Node being popped: " << std::endl;
		//PrintTreeNode(aNode);

		ReturnValue = true;
	}
#if defined (STACK_DEBUGGING_ENABLED)
	//std::cout << "NodeIndex is at " << NodeIndex << std::endl;
#endif
	return ReturnValue;
}
#endif // #if defined (EVALUATION_METHOD_EXPRESSIONTREE)
