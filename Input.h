/*
Name :  Input.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 20-06-2016
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
This module gets input from the user for entering a program. 
Notes:

Bug and revisioin history;
Version 0.1 Initial version
               
*/

#ifndef INPUT_H
#define INPUT_H

#include "vector"
#include <iostream>
#include "std_types.h"

void GetInput(void);



// Read and display contents of a filename
// No need to change this function
// From cplusplus.com

// Use this file for loading the dictionary module in problem 7-10.

void ReadFile(std::string &Filename);

#endif
