/*
Name :  Parser.h
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module initliazes symbol Table. 
Notes:

The syntax tree would have the following:
binary Operator node with two operands 
unary oprator node with Single operand.
identifier Operands in either one or two branches for each operator.
Numeric literal Operands in either one or two branches for each operator.
One numeric literal and one identifier branch for each operator.

A operator node is parent node. 

Bug and revision history;
0.1 Initial version
0.2 Added line numbers at which Errors occur
0.3 Changed some function parameters to const data type & 
*/

#ifndef PARSER_H
#define PARSER_H

#include "Error.h"
#include "TestFramework.h"
#include "Lexer.h"
#include "stack.h"

/* Syntax tree data structure.*/
struct Treenode
{
	Lexer::Token_tag Token;
	UINT32 n;
	struct Treenode* pRight;
	struct Treenode* pLeft;
};



namespace Parser
{
	

/*
 Component Function: void InitParser(void)
 Arguments: None
 Returns: None
 Description: 
  Initializes the parser
  Version : 0.1
*/
void InitParser(void);	

/*
Component Function: void  ParseTokenstream(Lexer::Token_tag& Token)
Arguments: Token stream
Returns: None
Version : 0.1 Initial version.
Description:
Notes:
To do:
*/
void  ParseTokenstream(Lexer::Token_tag& Token);

}


#endif // 