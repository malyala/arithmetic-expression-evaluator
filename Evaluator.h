/*
Name :  Evaluator.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 05-10-2016
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
This module evaluates expressions in a binary tree
*/

#ifndef EVALUTOR_H
#define EVALUTOR_H

#include "Lexer.h"
#include "EvaluateExpressions.h"

/* Evalutor Debugging code enabled or disabled */
#if(0)
#define DEVELOPMENT_DEBUGGING_ENABLED 0x01
#endif


#if defined (EVALUATION_METHOD_EXPRESSIONTREE)
/*
Component Function: Variant EvaluateExpressionTree(Tnode *Tree)
Arguments: A abstract syntax tree with its root as argument
Returns : None
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double. 
The requirement for this function to work is that the binary expression tree should be in correct order and all variant data types 
have their type declared.
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed. Write clear code.
*/
Variant EvaluateExpressionTree(Tnode *Tree);

/*
Component Function: Variant ComputeArithmetic(Tnode *Tree, Variant& ValLeft, Variant& ValRight, Variant& Value)
Arguments: 
Returns : None
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double. 
The requirement for this function to work is that the binary expression tree should be incorrect order.
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed. Write clear code.
*/
Variant ComputeArithmetic(Tnode *Tree, Variant& ValLeft, Variant& ValRight, Variant& Value);
#endif // #if defined (EVALUATION_METHOD_EXPRESSIONTREE)
/*
Component Function: void ChangeTypes(Variant& ValLeft, Variant& ValRight, Variant& Value)
Arguments: 
Returns : Variant
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double. 
The requirement for this function to work is that the binary expression tree should be incorrect order.
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed. Write clear code.
 */
void ChangeTypes(Variant& ValLeft, Variant& ValRight, Variant& Value);

#if defined (EVALUATION_METHOD_RPN)
/*
Component Function: Variant ComputeArithmeticOnTokens(const SINT8* Op, Variant& ValLeft, Variant& ValRight, Variant& Value)
Arguments: 
Returns : Variant
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double. 
The requirement for this function to work is that the binary expression tree should be incorrect order.
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed. 
To do:
Modify this function to include unary minus detection and compuation on unary minus.
Write code so that unary minus is allowed on operands , watch for unary minus  in ushort, uint or ulong type operands.
Unary minus subtree is folded as a operand for evaluation with other nodes.
Unary minus has either left or right child operand or operator with the other child being nullptr.
 */
Variant ComputeArithmeticOnTokens(const SINT8* Op, Variant& ValLeft, Variant& ValRight, Variant& Value);

/*
Component Function: void GetResult(Token_tag &ResultToken,Variant Value)
Arguments: Token, Token type with data. 
Returns : 
Version : 0.1
Description:
This function returns a variant type which can contain a value which can be a int, unsigned int, float or double. 
A variant is used to return a data type which is calculated at runtime.
Function being Designed and constructed. 
To do:
Modify this function to include unary minus detection and compuation on unary minus.
Write code so that unary minus is allowed on operands , watch for unary minus  in ushort, uint or ulong type operands.
Unary minus subtree is folded as a operand for evaluation with other nodes.
Unary minus has either left or right child operand or operator with the other child being nullptr.
 */
 
void GetResult(Token_tag &ResultToken,Variant Value);

#endif //#if defined (EVALUATION_METHOD_RPN)

#endif /* #define EVALUTOR_H */
