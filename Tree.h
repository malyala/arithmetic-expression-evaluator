/*
Name :  Tree.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
This module provides tree node definitions
*/
// Definition of a Tree node
#ifndef TREE_H
#define TREE_H

#include "std_types.h"
#include "vector"
#include "EvaluateExpressions.h"
#include "Error.h"

#if defined (EVALUATION_METHOD_EXPRESSIONTREE)

/* Define labels for nodes, these are essential in building Binary tree */
#define NODE_LABEL_OPERATOR 0x00
#define NODE_LABEL_OPERAND 0x01

/* Labels for binary trees */
#define NODE_POSITION_LEFT_CHILD 10
#define NODE_POSITION_RIGHT_CHILD 11
#define NODE_NOT_INITIALIZED 12
 
/*
Create a tree node as a list which connects three nodes  pPrev, pNext and pParent.
*/
// Remove op Data and use Variant Data for op symbol if possible.
typedef struct Tnode
{
    /* This Data will store Data or op of any type */
    Variant Data;
    const SINT8* Op;
	UINT32 LineNumber, ColumnNumber;
    UINT32 NodeType;
    Tnode *LeftChild, *RightChild, *pParent;
};

/*
Component Function: void InsertNode(UINT32 NodePos,Tnode *Tree,Tnode *NewNode)
Arguments: Node to be inserted and node position.
Returns : None
Description:
This function inserts a node into a binary expression tree. A node is 
inserted as a left or right node.
Version and bug history:
0.1 Initial version.
Notes: Function being designed.
*/
void InsertNode(UINT32 NodePos,Tnode *Tree,Tnode *NewNode);
/*
Component Function: void DeleteBinaryTree(Tnode *Tree)
Arguments: TBD
Returns : TBD
Description:
This function deletes nodes in a tree bottom up. First the function would delete all nodes in the left subtree and then it would delete nodes 
in the right subtree and deletes the root as well. Operands are deleted first, operators are deleted next.
Version and bug history:
0.1 Initial version.
*/
void DeleteBinaryTree(Tnode *Tree);

/*
Component Function: void PrintNode(const Tnode* Node)
Arguments:  Reference to Node
Returns: None
Description:  Displays data in node.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintNode(const Tnode* Node);

/*
Component Function: void PrintTreeNode(const Tnode& Node)
Arguments:  Reference to Node
Returns: true or not
Description:  Displays data in node.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintTreeNode(const Tnode& Node);
/*
Component Function: void ClearNode(Tnode &Node)
Arguments:  Reference to node
Returns: None
Description:
Clears at token to read next token
Version : 0.1
*/
void ClearNode(Tnode &Node);

/*
Component Function: inline void AssignChildNodeNull(Tnode *Node, UINT32 NodePosition)
Arguments:  Parent node, position of child node.
Returns: None
Version : 0.1 Initial version.
Description:
Initializes childnodes to nullptr
Notes:
*/
inline void AssignChildNodeNull(Tnode *Node, UINT32 NodePosition)
{
	if (NodePosition == NODE_POSITION_LEFT_CHILD)
	{
		Node->LeftChild=nullptr;

	}
	else if (NodePosition== NODE_POSITION_RIGHT_CHILD)
	{
		Node->RightChild=nullptr;		
	}
}

#endif //  #if defined (EVALUATION_METHOD_EXPRESSIONTREE)
#endif
