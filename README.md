## Arithmetic expression evaluator.
This project evaluates artihmetic expressions using Dijkstra's shunting yard algorithm and also with Binary trees. 
The code is written in C++. The arithmetic operators described in "The C++ programming language" were used in addition to a few mathematical 
functions.The project can evaluate complex arithmetic expressions with error detection. Component based software 
engineering was used during development. 

For selecting a evaluation method, a setting can be chosen with a compile time switch in EvaluateExpressions.h file.
You have to uncomment either of these below:
//#define EVALUATION_METHOD_EXPRESSIONTREE 0x01
//#define EVALUATION_METHOD_RPN        0x02 

Arithmetic expression is entered in ArithmeticExpression.txt. Only one arithmetic expresssion can be evaluated.

The project compiles in Visual studio 2017 under ISO C++14/C++17 standard and also in MinGW G++ under ISO C++11.

##Software architecture 
![alt text](https://bitbucket.org/malyala/arithmetic-expression-evaluator/downloads/Calculator.jpg)

Some featues to add:
Detecting exponential numerical values as 1.2e4. 
Grammar in lexical analyzer to detect types of characters to use in the expression.

Author: Amit Malyala 
