/*
Name :  Lexer.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module creates lexer tokens.
Notes:

The program executes as below with outputs or internal operations under << >>. Everything else is input. 

    a=1
	a
	1
	b=3
	c=4
	d=(a*123+b/123+c*8)/(c-0.2)
	d
    << Prints value of d >>

	CLEAR
	<< clears all variables >> 
           
    a=11
	a
	11
	b=31
	c=49
	(a*123+b/123+c*8)/(c-0.2)
    << Prints result of expression >>
    CLEAR
	<< clears all variables >> 

	EXIT
	<<program exits >>


Create Ops <<=   >>= 

Notes:
in a statement 
a= 1

token a is identified as ident and added into symbol table.Each ident to the left of '=' is added into symbol 
idents on the right of '=' are looked up.
The lexer looks up symbol names. The parser initializes symbols/idents durng construction of syntax tree.
	
Bug and revision history;
0.1 Initial version
*/
#include "Lexer.h"
#include "stack.h"
/* create head and sentinel node */
static Tnode* pSentinel= nullptr; 
static Tnode* pHead=nullptr; 
// Comment when module is integrated with the project
// Uncomment to develop or test

/*Create a symbol table for all idents. All symbols would be in one scope.*/
Scopes* CurrentScope = new Scopes;

#if(0)
SINT32 main(void)
{
	/* First try with stack, then a linked list */
	InitTokenStack();
	Lexer::Token_tag Token;
	while(1)
	{
		Token= Lexer::getNextToken();
	}
    return 0;
}
#endif

/*
Component function: void InitTokenList(void)
Arguments:  None 
Returns: NOne
Description:  
Initializes Token list. 
Version : 0.1
*/
void InitTokenList(void)
{
	pHead = new Tnode;
	pSentinel = new Tnode;
	
	pHead->pNext=pSentinel;
	pSentinel->pPrevious=pHead;
	pHead->pPrevious=nullptr;
	pSentinel->pNext=nullptr;
}



/*
Component function: void AddNode(Tnode *pNode)
Arguments:  A node of type Token node. 
Returns: NOne
Description:  
adds a node.
Version : 0.1
*/
void AddNode(Tnode *pNode)
{
	Tnode* pTemp = pSentinel->pPrevious;
	pTemp->pNext=pNode;
	pNode->pPrevious=pTemp;
	pNode->pNext=pSentinel;
}



/*
Component function: void DeleteList(void)
Arguments:  None.
Returns: None
Description:  
Deletes entire list excpet pHead and psentinel.
Version : 0.1
*/
void DeleteList(void)
{
	Tnode* pCurrent= pHead->pNext;
	while (pCurrent!= pSentinel) 
	{
		Tnode* pTemp=pCurrent->pNext;
		if (pCurrent->Token.Data.u.cString)
		{
			delete[] pCurrent->Token.Data.u.cString;
			pCurrent->Token.Data.u.cString=nullptr;
		}
		delete pCurrent;
		pCurrent=pTemp;
	}
	pHead->pNext=pSentinel;
	pSentinel->pPrevious=pHead;	
}

/*
Component function: SCHAR Lexer::PeekNextChar(SCHAR* str, UINT32 Length, UINT32 index)
Arguments:  char string, length, current index.
Returns: next char of index.
Description:  returns a char
Version : 0.1
*/
SCHAR Lexer::PeekNextChar(SCHAR* str, UINT32 Length, UINT32 index)
 {
 	//std::cout << "in peek next char function " << std::endl;
 	if ((index+1)<Length)
 	{
 		return str[index+1];		
	}
	else
	{
		return ENDOFINPUTSTRING;
	}
 }


/*
 Component Function: BOOL Lexer::IsWhiteSpace(SCHAR Character)
 Arguments:  Character
 Returns:
 Description:
 This function checks if a character is whitespace or not 
  Version : 0.1
 */
BOOL Lexer::isWhitespace(SCHAR Character)
{
    return (Character == ' ' || Character == '\t' );
}


/* Skip whitespace */
void Lexer::SkipWhitespace(SCHAR* str, UINT32 Length, UINT32& i)
{
	SCHAR ch; /* Use this variable outside of function?*/
    do
    {
    	ch = PeekNextChar(str,Length,i);
    	if (Lexer::isWhitespace(ch))
    	{
    		i++;    		
		}
	}
    while (Lexer::isWhitespace(ch));
}




/*
 Component Function: BOOL Lexer::isIdentChar(SCHAR Character)
 Arguments:  Character to be searched.
 Returns:
 Description: 
 Returns is ident char or not.
  Version : 0.1
 */
BOOL Lexer::isIdentChar(SCHAR Character)
{
	//std::cout << "in isIdentchar() function " << std::endl;
    return ((Character >= 'a' && Character <= 'z') || (Character >= 'A' && Character <= 'Z') || Character == '_');
}

/*
 Component Function: BOOL Lexer::isSeparator(SCHAR Character)
 Arguments:  Character to be searched in separators
 Returns:
 Description:
 Checks if a char is a separator.
  Version : 0.1
 */
BOOL Lexer::isSeparator(SCHAR Character)
{
    return (Character == ')' || Character == '(');
}

/*
 Component Function: BOOL Lexer::IsIdentorNumericchar(SCHAR Character)
 Arguments:  Character after ( 
 Returns:
 Description:
 Checks if a char is ident or numeric char.
  Version : 0.1
 */
BOOL Lexer::IsIdentorNumericchar(SCHAR Character)
{
    return (Lexer::isIdentChar(Character) || DataStructures::isDigit(Character));
}



/*
 Component Function: BOOL Lexer::isIdent(const SCHAR* str)
 Arguments:  string to be checked.
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z and digits
 Returns ident or not
 Version : 0.1
 */
BOOL Lexer::isIdent(const SCHAR* str)
{
    UINT32 i = 0;
    UINT32 StrLength = DataStructures::strlength(str);
    UINT32 CharCount = 0;
    BOOL Ident = false;
    while (i < StrLength)
    {
        if (isIdentChar(str[i]))
        {
        	CharCount++;        	
		}
        
        
        i++;
    }

    if (CharCount == (StrLength))
    {
        Ident = true;
    }
    return Ident;
}



/*
 Component Function: BOOL Lexer::isOperator(const SCHAR* str)
 Arguments:  string to be checked.
 Returns:
 Description: This function checks if char string is a operator. 
 Returns op or not
 Version : 0.1
 */
BOOL Lexer::isOperator(const SCHAR* str)
{
    UINT32 i = 0;
    UINT32 StrLength = DataStructures::strlength(str);
    UINT32 CharCount = 0;
    BOOL isOp = false;
    while (i < StrLength)
    {
    	if (Lexer::isOperchar(str[i]))
            CharCount++;
        i++;
    }

    if (CharCount == (StrLength))
    {
        isOp = true;
    }
    return isOp;
}
/*
 Component Function: BOOL Lexer::isOperchar(SCHAR Ch)
 Arguments:  character
 Returns:
 Description:
 returns char is a operator or not 
  Version : 0.1
 */
BOOL Lexer::isOperchar(SCHAR Ch)
{
	BOOL isoperchar;
    switch (Ch)
    {
    	case '+':
    	case '-':
		case '/':
		case '*':
		case '=':	
		case '<':
		case '>':
		case '|':
		case '&':
		case '~':
		case '!':
		case '^':
			isoperchar=true;
			break;
		default:
			break;	
	}
	return isoperchar;
}
/*
 Component function: SCHAR* Lexer::getTokencharstring(void)
 Arguments: char string
 returns : a substring of type char*
 Version : 0.1
 Description:
 parses a char string into substrings.
 Each substring canbe these:
 ident,
 numeric,
 operator 
 parenthesis ( )
 These substrings would be read by Tokenprocessor() function.
 
 Change funtion.
 This is not detecting a expression as:
  
 a=!b without any ' ' 
 a=!a
 a=~b
 
 modify program to detext expressions as 
 b= ident 
 c= name
 
 
 
 Notes:
 Function being edited
 
 */
 SCHAR* Lexer::getTokencharstring(void)
 {
 	static SCHAR* str=nullptr; 
 	static SCHAR* substring=nullptr;
 	 if (!str)
 	 {
 	 	str=io::getInput();
		//std::cout << "char string in getTokencharstring() " << str << std::endl; 	 	
	 }
 	 UINT32 length=DataStructures::strlength(str);
 	 //std::cout << "Length of char string " << length << std::endl;
 	 SCHAR ch= ' ';
 	 //create substring and send it to token processor which sends it to getNextToken() each time getNextToken() is called. 
 	 
 	 UINT32 Count=0;
 	 /*  separate counter for each type of substring */
 	 SCHAR NextChar =' ';
 	 static UINT32 i=0;
 	 BOOL substringdetected=false;
 
	 if (i == length)
	 {
		i=0;
		//std::cout << "deleting input string" << std::endl;
		delete[] str; 
		str=nullptr;
		str=io::getInput();
		length=DataStructures::strlength(str);
		//std::cout << "reached end of string while parsing"  << std::endl;
    }
     if (substring)
 	 {
 	 	//std::cout << "Substring is not empty, deleting it  "  << std::endl;
 	 	delete[] substring;
		substring=nullptr;
		substring = new SCHAR[length+2]; /* +2 for newline and line terminator for single char strings*/
     }
     else
     {
     	substring = new SCHAR[length+2]; /* +2 for newline and line terminator for single char strings*/
	 }
		//std::cout << "allocating memory for newline and Substring."  << std::endl;
 	 while(i<length && substringdetected== false)
 	 {
 	 	ch = str[i];
		if (Lexer::isWhitespace(ch))
		{
			Count=0;
			Lexer::SkipWhitespace(str,length,i);
		}
		else  if ( ch == '\n')
		{
			Count=0;
			//std::cout << "character is new line" << std::endl;
			/* Check if \n positoin is at length of char string.*/
			substring[Count]= ch;
			substring[Count+1]='\0';
			substringdetected=true;
		}
		else if (Lexer::isIdentChar(ch))
 	 	{
 	 		// Allocate memory for substrin g
 	 		substring[Count]=ch; 
 	 		//std::cout << " before PeekNextchar()"  << std::endl;
 	 		NextChar=Lexer::PeekNextChar(str,length,i);

 	 		if ((!Lexer::isIdentChar(NextChar)) || NextChar == '#' )
 	 		{
 	 			//std::cout <<  "substring is ident " << std::endl;
 	 			// Create end of substring here */
 	 			substring[Count+1]= '\0';
				substringdetected=true;
				//std::cout << "substring is " << substring << std::endl;
			}
			else
			{
				Count++;				
			}
		}
		else if ((DataStructures::isDigit(ch))|| ch == '.')
		{
			substring[Count]=ch;
			NextChar=Lexer::PeekNextChar(str,length,i);
			if (((!DataStructures::isDigit(NextChar)) &&  NextChar != '.') || NextChar == '#')
			{
				// Create end of substring here */
				substring[Count+1]= '\0';
				substringdetected=true;
			}
			else
			{
				Count++;				
			}
		}
		/* Write code for these two tokens */
		else  if (Lexer::isSeparator(ch))
		{
			/* Detect nested ()  as (((1*4)*2)*3) */
			Count=0;
			substring[Count]=ch;
			substring[Count+1]='\0';
			substringdetected=true;
			
		}
		else if (Lexer::isOperchar(ch))
		{
			substring[Count]=ch;
			/* Detect operators as ++, -- , +=,. *= , /=., a= !b, a=-b or a=+b.
			Detect a/=b, a*=b, a+=b, a%=b, a-=b. */
			NextChar=Lexer::PeekNextChar(str,length,i);
			if ((ch == '=') && (NextChar == '~' || NextChar == '!'|| NextChar == '-' || NextChar == '+'))
			{
				substring[Count+1]='\0';
				Count=0;
				substringdetected=true;
			}
			else if ((ch == '=') && (NextChar == '*' || NextChar == '/'|| NextChar == '^' || NextChar == '%'))
			{
				
				substring[Count+1]=NextChar;
				substring[Count+2]='\0';
				i++;
				substringdetected=true;
				ErrorTracer(ILLEGAL_CHARACTER,0,0);
				/* detect error in parser */
			}
			
			else if (!Lexer::isOperchar(NextChar))
			{
				// Create end of substring here */
				substring[Count+1]= '\0';
				Count=0;
				substringdetected=true;
			}
			else
			{
				Count++;				
			}						
		}
		
		else
		{
			ErrorTracer(ILLEGAL_CHARACTER,0,0);
		}
		
		i++;
	 }
	 
	 //std::cout << "substring in getTokencharString: " << substring << std::endl;
	 return substring; // This is appending \0 at the end of each string.
 }
 /*
Component function: Lexer::Token_tag Lexer::getNextToken(void)
Arguments:  None
Returns: Token
Description:  returns a token
Notes: 
This function gets each char string from each line entered and detects invidual tokens in it.
Each time this function is called, it returns a token. When all of the char string are scanned, 
it gets a new char string and the process is repeated.If there are incomplete tokens or errors, 
a error is signalled and the the program resumes functioning.
Version : 0.1
*/

Lexer::Token_tag Lexer::getNextToken(void)
{
	Lexer::Token_tag Token;
	Lexer::TokenProcessor(Token);
	/* linked list of tokens is not necessary perhaps*/
	return Token;
}
 
/*
 Component function:void Lexer::TokenProcessor(Token_tag& Token)
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns current Token after classifying it from a stream of tokens created on the fly. 
 Each token can be these:
 ident,
 numeric,
 operator 
 Separator- parenthesis ( ) or ' '
 Newline \n
  
 Notes:
 
in a statement,
a= 1
token a is identified as ident and added into symbol table.Each ident to the left of '=' is added into symbol 
Only one '=' can be used on one line.
when the above symbol is used 
 
 Function being edited
 */
void Lexer::TokenProcessor(Lexer::Token_tag& Token)
{
	BOOL opequaltoDetected=false;
	/* check each token string, classify it as ident, operator, parenthesis or numeric value.*/
	SCHAR* Tokenstring= getTokencharstring();
		
	if (strcmp(Tokenstring,"\n") ==0)
	{
		Token.Type=Lexer::TokenType::endofline;
		Memcopy(Token.Data.u.cString,Tokenstring);
		opequaltoDetected=false;
		PushTokenStack(Token);
    	/* No other deletion of  memory allocated for lexer tokens is required as it is done by parser.*/
	}
	else if (strcmp(Tokenstring,"(") ==0)
	{
		Token.Type=Lexer::TokenType::separator_leftParen;
		Memcopy(Token.Data.u.cString,Tokenstring);
		PushTokenStack(Token);
		
	}
	else if (strcmp(Tokenstring,")") ==0)
	{
		Token.Type=Lexer::TokenType::separator_rightParen;
		Memcopy(Token.Data.u.cString,Tokenstring);
		PushTokenStack(Token);
		
	}
	else if (strcmp(strlwr(Tokenstring),"exit") ==0)
	{
		
		Token.Type=Lexer::TokenType::command;
		Memcopy(Token.Data.u.cString,Tokenstring);
		
	}
	else if (strcmp(strlwr(Tokenstring),"clear") ==0)
	{
		
		Token.Type=Lexer::TokenType::command;
		Memcopy(Token.Data.u.cString,Tokenstring);
	} 
	
	else if (Lexer::isValidOp(Lexer::DetectOpType(Tokenstring)))
	{
	
		Token.Type=Lexer::TokenType::op;
		// Check other types of = operator  as += -= /= *= later
		Memcopy(Token.Data.u.cString,Tokenstring);
							
		if (DetectOpType(Tokenstring) == OPEQUALTO)
		{
			opequaltoDetected=true;
		    // Check if previous token is ident,insert it into symbol table.
			//Use stack or a list to store previous, current and next token.		    	
			if (opequaltoDetected==true)
			{
				// Add ident token into symbol table.
    			Lexer::Token_tag PreviousToken;
				if (PopTokenStack(PreviousToken))
				{
					PushTokenStack(PreviousToken);
					// This would look up symbols as a=b=c= 1+2
			         // insert symbol name (token) into symbol table. 
			          if (PreviousToken.Type== Lexer::TokenType::identifier)
			          {
			          	  CurrentScope->InsertSymbolName(PreviousToken.Data.u.cString);			          	
					  }
					  else
					  {
					  	 ErrorTracer(INCORRECT_ARITHMETIC,0,0);
					  }
				       
				}
				else
				{
					ErrorTracer(INCORRECT_ARITHMETIC,0,0);					
				}
				
			}
		}
		PushTokenStack(Token);
	}	 
	else if (Lexer::isIdent(Tokenstring))
	{
		Token.Type=Lexer::TokenType::identifier;	
		Memcopy(Token.Data.u.cString,Tokenstring);
		PushTokenStack(Token);
	}
	else if (DataStructures::isNumeric(Tokenstring))
	{
		Token.Type=Lexer::TokenType::numericliteral;
		Token.Data.u.doubleValue=DataStructures::atodouble(Tokenstring);
		PushTokenStack(Token);
	}
	else if ( (strcmp(Tokenstring,"=*") ==0)  ||  (strcmp(Tokenstring,"=/") ==0) || (strcmp(Tokenstring,"=%") ==0) || (strcmp(Tokenstring,"=^") ==0) )
	{
		Token.Type=Lexer::TokenType::error;
		Memcopy(Token.Data.u.cString,Tokenstring);
		PushTokenStack(Token);
	}

}


/*
 Component Function: BOOL Lexer::isValidOp(UINT32 OpType)
 Arguments:  Op type
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
BOOL Lexer::isValidOp(UINT32 OpType)
{
	if (OpType == NOTOP)
	{
		return false;		
	}
	else 
	{
		return true;		
	}
}

/*
 Component Function: UINT32 Lexer::DetectOpType(SCHAR* OpString)
 Arguments:  char string
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
UINT32 Lexer::DetectOpType(SCHAR* OpString)
{
    UINT32 ReturnOpType = NOTOP;
    if ((strcmp(OpString,"+")==0))
    {
        ReturnOpType = OPPLUS;
    } 
	else if ((strcmp(OpString,"-")==0))
    {
        ReturnOpType = OPMINUS;
    } 
	else if ((strcmp(OpString,"/")==0))
    {
        ReturnOpType = OPDIV;
    } 
	else if ((strcmp(OpString,"*")==0))
    {
        ReturnOpType = OPMUL;
    } 
	else if ((strcmp(OpString,"%")==0))
    {
        ReturnOpType = OPMODULUS;
    } 
	else if ((strcmp(OpString,"^")==0))
    {
        ReturnOpType = OPPOWEXPONENT;
    } 
	else if ((strcmp(OpString,"=")==0))
    {
        ReturnOpType = OPEQUALTO;
    }
	else if ((strcmp(OpString,"+=")==0))
	{
		ReturnOpType = OPADDEQUALTO;
	}
	else if ((strcmp(OpString,"-=")==0))
	{
		ReturnOpType = OPMINUSEQUALTO;
	}
	else if ((strcmp(OpString,"*=")==0))
	{
		ReturnOpType = OPMULTIPLYEQUALTO;
	}
	else if ((strcmp(OpString,"/=")==0))
	{
		ReturnOpType = OPDIVEQUALTO;
	} 
	else if ((strcmp(strlwr(OpString),"sin")==0))
    {
        ReturnOpType = OPSIN;
    } 
	else if ((strcmp(strlwr(OpString),"cos")==0))
    {
        ReturnOpType = OPCOS;
    } 
	else if ((strcmp(strlwr(OpString),"tan")==0))
    {
        ReturnOpType = OPTAN;
    } 
	else if ((strcmp(strlwr(OpString),"sec")==0))
    {
        ReturnOpType = OPSEC;
    } 
	else if ((strcmp(strlwr(OpString),"cosec")==0))
    {
        ReturnOpType = OPCOSEC;
    } 
	else if ((strcmp(strlwr(OpString),"cot")==0))
    {
        ReturnOpType = OPCOT;
    } 
	else if ((strcmp(strlwr(OpString),"sqrt")==0))
    {
        ReturnOpType = OPSQRT;
    } 
	else if ((strcmp(strlwr(OpString),"loge")==0))
    {
        ReturnOpType = OPLOG;
    }
    else if ((strcmp(OpString,"<")==0))
    {
    	ReturnOpType = OPLESSTHAN;
	}
	else if ((strcmp(OpString,">")==0))
	{
		ReturnOpType = OPGREATERTHAN;
	}
	else if ((strcmp(OpString,"<<")==0))
	{
		ReturnOpType = OPLEFTBITSHIFT;
	}
	else if ((strcmp(OpString,">>")==0))
	{
		ReturnOpType = OPRIGHTBITSHIFT;
	}
	else if ((strcmp(OpString,"!")==0))
	{
		ReturnOpType = OPLOGICALNOT;
	}
	else if ((strcmp(OpString,"|")==0))
	{
		ReturnOpType = OPBITWISEOR;
	}
	else if ((strcmp(OpString,"&")==0))
	{
		ReturnOpType = OPBITWISEAND;
	}
	else if ((strcmp(OpString,"&&")==0))
	{
		ReturnOpType = OPLOGICALAND;
	}
	else if ((strcmp(OpString,"||")==0))
	{
		ReturnOpType = OPLOGICALOR;
	}
	else if ((strcmp(OpString,"<=")==0))
	{
		ReturnOpType = OPLESSTHANEQUALTO;
	}
	else if ((strcmp(OpString,">=")==0))
	{
		ReturnOpType = OPGREATERTHANEQUALTO;
	}
	else if ((strcmp(OpString,"==")==0))
	{
		ReturnOpType = OPLOGICALISEQUALTO;
	}
	else if ((strcmp(OpString,"!=")==0))
	{
		ReturnOpType = OPLOGICALISNOTEQUALTO;
	}
	else if ((strcmp(strlwr(OpString),"xor")==0))
	{
		ReturnOpType = OPBITWISEXOR;
	}
	else if ((strcmp(OpString,"~")==0))
	{
		ReturnOpType = OPCOMPLEMENT;
	}
	
	else if ((strcmp(OpString,"%=")==0))
	{
		ReturnOpType = OPMODEQUALTO;
	}
	else if ((strcmp(OpString,"~=")==0))
	{
		ReturnOpType = OPCOMPLEMENTEQUALTO;
	}
	else if ((strcmp(OpString,"_")==0))
	{
		ReturnOpType  = OPUNARYMINUS;
	}
	else if ((strcmp(OpString,"<<=")==0))
	{
		ReturnOpType  = OPLEFTSHIFTEQUALTO;
	}
	else if ((strcmp(OpString,">>=")==0))
	{
		ReturnOpType  = OPRIGHTSHIFTEQUALTO;
	}
    else if ((strcmp(OpString,"&=")==0))
	{
		ReturnOpType  = OPANDEQUALTO;
	}
	else if ((strcmp(OpString,"|=")==0))
	{
		ReturnOpType  = OPOREQUALTO;
	}
    //std::cout << "At endof DetectOpType() function " << std::endl;
    return ReturnOpType;

}


/*
 Component Function: UINT32 Lexer::DetectOpToken(Lexer::Token_tag& OpToken)
 Arguments:  Token
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
UINT32 Lexer::DetectOpToken(Lexer::Token_tag& OpToken)
{
    UINT32 ReturnOpType = NOTOP;
    if (OpToken.Data.u.cString)
    {
    if ((strcmp(OpToken.Data.u.cString,"+")==0))
    {
        ReturnOpType = OPPLUS;
    } 
	else if ((strcmp(OpToken.Data.u.cString,"-")==0))
    {
        ReturnOpType = OPMINUS;
    } 
	else if ((strcmp(OpToken.Data.u.cString,"/")==0))
    {
        ReturnOpType = OPDIV;
    } 
	else if ((strcmp(OpToken.Data.u.cString,"*")==0))
    {
        ReturnOpType = OPMUL;
    } 
	else if ((strcmp(OpToken.Data.u.cString,"%")==0))
    {
        ReturnOpType = OPMODULUS;
    } 
	else if ((strcmp(OpToken.Data.u.cString,"^")==0))
    {
        ReturnOpType = OPPOWEXPONENT;
    } 
	else if ((strcmp(OpToken.Data.u.cString,"=")==0))
    {
        ReturnOpType = OPEQUALTO;
    }
	else if ((strcmp(OpToken.Data.u.cString,"+=")==0))
	{
		ReturnOpType = OPADDEQUALTO;
	}
	else if ((strcmp(OpToken.Data.u.cString,"-=")==0))
	{
		ReturnOpType = OPMINUSEQUALTO;
	}
	else if ((strcmp(OpToken.Data.u.cString,"*=")==0))
	{
		ReturnOpType = OPMULTIPLYEQUALTO;
	}
	else if ((strcmp(OpToken.Data.u.cString,"/=")==0))
	{
		ReturnOpType = OPDIVEQUALTO;
	} 
	else if ((strcmp(strlwr(OpToken.Data.u.cString),"sin")==0))
    {
        ReturnOpType = OPSIN;
    } 
	else if ((strcmp(strlwr(OpToken.Data.u.cString),"cos")==0))
    {
        ReturnOpType = OPCOS;
    } 
	else if ((strcmp(strlwr(OpToken.Data.u.cString),"tan")==0))
    {
        ReturnOpType = OPTAN;
    } 
	else if ((strcmp(strlwr(OpToken.Data.u.cString),"sec")==0))
    {
        ReturnOpType = OPSEC;
    } 
	else if ((strcmp(strlwr(OpToken.Data.u.cString),"cosec")==0))
    {
        ReturnOpType = OPCOSEC;
    } 
	else if ((strcmp(strlwr(OpToken.Data.u.cString),"cot")==0))
    {
        ReturnOpType = OPCOT;
    } 
	else if ((strcmp(strlwr(OpToken.Data.u.cString),"sqrt")==0))
    {
        ReturnOpType = OPSQRT;
    } 
	else if ((strcmp(strlwr(OpToken.Data.u.cString),"loge")==0))
    {
        ReturnOpType = OPLOG;
    }
    else if ((strcmp(OpToken.Data.u.cString,"<")==0))
    {
    	ReturnOpType = OPLESSTHAN;
	}
	else if ((strcmp(OpToken.Data.u.cString,">")==0))
	{
		ReturnOpType = OPGREATERTHAN;
	}
	else if ((strcmp(OpToken.Data.u.cString,"<<")==0))
	{
		ReturnOpType = OPLEFTBITSHIFT;
	}
	else if ((strcmp(OpToken.Data.u.cString,">>")==0))
	{
		ReturnOpType = OPRIGHTBITSHIFT;
	}
	else if ((strcmp(OpToken.Data.u.cString,"!")==0))
	{
		ReturnOpType = OPLOGICALNOT;
	}
	else if ((strcmp(OpToken.Data.u.cString,"|")==0))
	{
		ReturnOpType = OPBITWISEOR;
	}
	else if ((strcmp(OpToken.Data.u.cString,"&")==0))
	{
		ReturnOpType = OPBITWISEAND;
	}
	else if ((strcmp(OpToken.Data.u.cString,"&&")==0))
	{
		ReturnOpType = OPLOGICALAND;
	}
	else if ((strcmp(OpToken.Data.u.cString,"||")==0))
	{
		ReturnOpType = OPLOGICALOR;
	}
	else if ((strcmp(OpToken.Data.u.cString,"<=")==0))
	{
		ReturnOpType = OPLESSTHANEQUALTO;
	}
	else if ((strcmp(OpToken.Data.u.cString,">=")==0))
	{
		ReturnOpType = OPGREATERTHANEQUALTO;
	}
	else if ((strcmp(OpToken.Data.u.cString,"==")==0))
	{
		ReturnOpType = OPLOGICALISEQUALTO;
	}
	else if ((strcmp(OpToken.Data.u.cString,"!=")==0))
	{
		ReturnOpType = OPLOGICALISNOTEQUALTO;
	}
	else if ((strcmp(strlwr(OpToken.Data.u.cString),"xor")==0))
	{
		ReturnOpType = OPBITWISEXOR;
	}
	else if ((strcmp(OpToken.Data.u.cString,"~")==0))
	{
		ReturnOpType = OPCOMPLEMENT;
	}
	
	else if ((strcmp(OpToken.Data.u.cString,"%=")==0))
	{
		ReturnOpType = OPMODEQUALTO;
	}
	else if ((strcmp(OpToken.Data.u.cString,"~=")==0))
	{
		ReturnOpType = OPCOMPLEMENTEQUALTO;
	}
	
	else if ((strcmp(OpToken.Data.u.cString,"_")==0))
	{
		ReturnOpType  = OPUNARYMINUS;
	}
	else if ((strcmp(OpToken.Data.u.cString,"<<=")==0))
	{
		ReturnOpType  = OPLEFTSHIFTEQUALTO;
	}
	else if ((strcmp(OpToken.Data.u.cString,">>=")==0))
	{
		ReturnOpType  = OPRIGHTSHIFTEQUALTO;
	}
	else if ((strcmp(OpToken.Data.u.cString,"&=")==0))
	{
		ReturnOpType  = OPANDEQUALTO;
	}
	else if ((strcmp(OpToken.Data.u.cString,"|=")==0))
	{
		ReturnOpType  = OPOREQUALTO;
	}
	
   }
    //std::cout << "At endof DetectOpType() function " << std::endl;
    return ReturnOpType;

}

/*
 Component Function: void Lexer::PrintToken(const Lexer::Token_tag& Token)
 Arguments: 
 Returns:
 Description:
  Prints a token.
  Version : 0.1
 */
void Lexer::PrintToken(const Lexer::Token_tag& Token)
{
	switch(Token.Type)		
	{
		case Lexer::TokenType::identifier:
			std::cout << "Token is identifier: " << Token.Data.u.cString << std::endl;
			break;
		case Lexer::TokenType::separator_leftParen:
			std::cout << "Token is left paren: " << Token.Data.u.cString << std::endl;
			break;
		case Lexer::TokenType::separator_rightParen:
			std::cout << "Token is right paren: " << Token.Data.u.cString << std::endl;
			break;
		case Lexer::TokenType::op:
			std::cout << "Token operator: " << Token.Data.u.cString << std::endl;
			break;
		case Lexer::TokenType::numericliteral:
			std::cout << "Token numeric literal : " << Token.Data.u.doubleValue << std::endl;
			break;
		case Lexer::TokenType::command:	
		    std::cout << "Token command: " << Token.Data.u.cString << std::endl;
		    break;
        case Lexer::TokenType::endofline:
        	std::cout << "Token newline. " << std::endl;
		    break;		
        case Lexer::TokenType::error:
        	std::cout << "Token error: " << Token.Data.u.cString << std::endl;
        	break;
        case Lexer::TokenType::Emptytoken:
        	std::cout << "Token empty: " << Token.Data.u.cString << std::endl;
        	break;	
        default:
		    break;	
	}
}


/*
 Component Function: void Lexer::ClearToken(Lexer::Token_tag& Token)
 Arguments: 
 Returns:
 Description:
  Prints a token.
  Version : 0.1
 */
void Lexer::ClearToken(Lexer::Token_tag& Token)
{
	if (Token.Data.u.cString)
	{
		delete[] Token.Data.u.cString;
		Token.Data.u.cString=nullptr;
	}
	Token.Type=Lexer::TokenType::Emptytoken;
	
}