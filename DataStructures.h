/*
Name :  DataStructures.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 20-06-2016
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
Some data structures used for Abstract sytax tree, vector tables, string to number functions,
various search and detection fuctions.
Notes:
Bug and version history:
0.1 Initial version
0.2 Added definition of Tree node which uses a double linked list.
*/

#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H

#include <vector>
#include <cstring>
#include <string>
#include "Std_types.h"
#include <iostream>
#include "Error.h"
#include "Tree.h"
#include "Lexer.h"

/* Code for Number types */
/*
#define HEXADECIMAL_NUMBER 0x03
#define OCTAL_NUMBER 0x04
#define ALPHA_NUMERIC_NUMBER 0x05
*/
#define NOT_OPERAND 0x02

/* Delete nodes in the DoubleLinkedList by searching for a node */
void DeleteNode(const int& number);

/*
Component Function:  void InitDataStructures(void);
Arguments: None
returns : None
Version : 0.1
Description:
Initialize all vector tables and data structures, keyword list, symbol tables.
*/
void InitDataStructures(void);
/* Convert a string of Numbers to short integer */

/*
 Component Function: short atoshort(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
*/
short atoshort(const SINT8* str);

/*
 Component Function: unsigned short atoushort(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
*/
unsigned short atoushort(const SINT8* str);

/*
 Component Function: SINT32 atoint(const char* str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
*/
SINT32 atoint(const SINT8* str);

/*
 Component Function: SINT32 stringtoint(const std::string & str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
*/

SINT32 stringtoint(const std::string &str);

/*
 Component Function: UINT32 atouint(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an unsigned integer.
 */

UINT32 atouint(const SINT8* str);

/*
 Component Function: long atolong(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to long integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
*/
SINT64 atolong(const char* str);

/*
 Component Function: UINT64 atoulong(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to unsigned long integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
*/
UINT64 atoulong(const SINT8* str);
/*
Component Function: static std::string itostring(UINT32 a)
Arguments:  None
returns: None
Description:
Convert a decimal UINT32eger to std::string
Version : 0.1
*/
std::string itostring(const UINT32& a);

/*
 Component Function: BOOL IsAlphabet(SINT8 Character)
 Arguments:  Character to be searched in list of alphabets
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z
 Returns Alphabetic or not
 Optional: Write a regex algorithm to detect alphabetical characters in a string.
 Version : 0.1
 */
BOOL IsAlphabet(SINT8 Character);
/*
Component Function: UINT32 isNumeric(const char* Str)
Arguments:  String character to be searched in list of digits
Returns:
Description: This function looks up all characters of a string in a list of digits and returns the nature
of the number whether its a decimal,floating point or double precision number.
Optional: Write a regex algorithm to detect digit characters in a string which is faster.
Version : 0.1
*/

UINT32 IsNumeric(const SINT8* str);

/*
Component Function: void InitVariant(Variant &Var)
Arguments:  None
returns: None
Description:
Initializes a variant type. This function is necessary to avoid 
a anonymous union initialization warning. Create a assign variant 
function which can assign a value to a variant data type.
Function to be defined. 
 * container.
Version : 0.1
 */
void InitVariant(Variant &Var);

/*
 Component Function: ULONGLONG DecMultiplier(UINT32 index)
 Arguments: a string which contains digits
 returns : a multiplier of pow(10,n)
 Version : 0.1
 Description:
  To do:
 Use a loop to convert float digits upto 20 decimal points and 20 floating points.
 */
ULONGLONG DecMultiplier(UINT32 index);
 
/*
 Component Function: FLOAT32 tofloat(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to double integer
 Version : 0.1
 Description:
 Function to convert a string of Number into an Floating point number.
 Get the Number of digits entered as a string.
 Recognize a . in the string and put the decimal part before the . and the mantissa after the dot.
 Notes:
 This function would be used by parser/lexer module.

 To do:
 Use a loop to convert float digits upto 20 decimal points and 20 floating points.
 */

FLOAT32 atofloat(const char* str);

/*
 Component Function: FLOAT64 tofloat(const SINT8* str)
 Arguments: a string which contains digits
 returns : string converted to floating point
 Version : 0.1
 Description:
 Function to convert a string of Number into an Floating point number.
 Get the Number of digits entered as a string.
 Recognize a . in the string and put the decimal part before the . and the mantissa after the dot.
 Notes:

 This function would be used by parser/lexer module.

 To do:
 Use a loop to convert float digits upto 20 decimal points and 20 floating points.
 */
FLOAT64 atodouble(const SINT8* str);

/*
 Component FLOAT32 MantissaFloatMultiplier(UINT32 index)
 Arguments: Position of Mantissa digit
 returns : a multiplier of pow(0.1,n)
 Version : 0.1
 Description:
 Returns pow (0.1,index)
 */
 FLOAT32 MantissaFloatMultiplier(UINT32 index);

/*
 Component FLOAT64 MantissaDoubleMultiplier(UINT32 index)
 Arguments: Position of Mantissa digit
 returns : a multiplier of pow(0.1,n)
 Version : 0.1
 Description:
 Returns pow (0.1,index)
 */
 FLOAT64 MantissaDoubleMultiplier(UINT32 index);

 /*
 Component Function: void InitVariant(Variant &Var)
 Arguments:  None
 returns: None
 Description:
 Initializes a variant type. This function is necessary to avoid
 a anonymous union initialization warning.
 Version : 0.1
 */
void InitVariant(Variant &Var);


/*
Component Function: void PrintNumberType(UINT32 NumberType)
Arguments:  Number type
Returns: None
Description:  Displays data in token.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintNumberType(UINT32 NumberType);

/*
 Component Function: UINT32 DetectStringType(const std::string& CharString )
 Arguments:  String 
 returns: None
 Description:
 Detects type of string such as define or a IDENT 
 Version : 0.1
 */
UINT32 DetectStringType(const std::string& CharString );

/*
 Component Function:UINT32 DetectOpType(const SINT8* OpString)
 Arguments:  char string 
 returns: None
 Description:
 Detects type of string such as define or a IDENT 
 Version : 0.1
 Notes:
 Function being designed.
 This seems a bit crude yet could be fine for user input type situations.
 */
//UINT32 DetectOpType(const std::string& Op);
UINT32 DetectOpType(const SINT8* OpString);
#endif
