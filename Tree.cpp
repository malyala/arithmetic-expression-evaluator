/*
Name: Tree.cpp
Author: Amit Malyala
License:
Copyright <2016> <Amit Malyala>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of
the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Description:
This module provides functions for creating, inserting and deleting Tree nodes in a binary expression tree.
*/
#include "Tree.h"
#include <iostream>
#include "Evaluator.h"
#include "Parser.h"

#if defined (EVALUATION_METHOD_EXPRESSIONTREE)
 // Tree pointer for Root
Tnode *Root = new Tnode;
/*
Component Function: void InsertNode(UINT32 NodePos,Tnode *Tree,Tnode *NewNode)
Arguments: Node poisition, node pointer.
Returns : None
Description:
This function inserts a node into a binary expression tree. A node is
inserted as a left or right node.
Version and bug history:
0.1 Initial version.
0.2 Added left and right operands, parent node linkage.
0.3 Removed nullptr assignment to leftchild and RightChild. Added nullpointer check for Tnode *Tree
To do:

Notes:
For now we will add nodes directly into the root tree.
 */

void InsertNode(UINT32 NodePos, Tnode *Tree, Tnode *NewNode)
{
	if (Tree != nullptr)
	{
		NewNode->pParent = Tree;
		if (NewNode->NodeType == NODE_LABEL_OPERATOR)
		{
			if (NodePos == NODE_POSITION_LEFT_CHILD)
			{
				Tree->LeftChild = NewNode;
			}
			else if (NodePos == NODE_POSITION_RIGHT_CHILD)
			{
				Tree->RightChild = NewNode;
			}

		}
		else if (NewNode->NodeType == NODE_LABEL_OPERAND)
		{
			AssignChildNodeNull(NewNode, NODE_POSITION_LEFT_CHILD);
			AssignChildNodeNull(NewNode, NODE_POSITION_RIGHT_CHILD);
			if (NodePos == NODE_POSITION_LEFT_CHILD)
			{
				Tree->LeftChild = NewNode;
			}
			else if (NodePos == NODE_POSITION_RIGHT_CHILD)
			{
				Tree->RightChild = NewNode;
			}
		}
	}
	else
	{
		ErrorTracer(MEMORY_ALLOCATION_ERROR, 0, 0);
	}
}

/*
Component Function: void DeleteBinaryTree(Tnode *Tree)
Arguments: Root node of a binary tree.
Returns : None.
Description:
This function deletes nodes in a Binary expression tree bottom up. First the function would delete all leaf nodes in the left subtree
and then it would delete leaf nodes in the right subtree.Then operator nodes are deleted. The root is deleted as well.
Version and bug history:
0.1 Initial version.
Notes:
*/
void DeleteBinaryTree(Tnode *Tree)
{
	UINT32 OpType = NOTOP;
	if (Tree != nullptr)
	{
		Tnode *Temp = nullptr;
		if (Tree->NodeType == NODE_LABEL_OPERATOR)
		{
			//std::string OpString (Tree->Op);
			OpType = DetectOpType(Tree->Op);
		}
		//std::cout << "In DeleteBinaryTree() function"  << std::endl;
		if (Tree->NodeType == NODE_LABEL_OPERAND)
		{
			if (Tree->LeftChild == nullptr && Tree->RightChild == nullptr && Tree->pParent != nullptr)
			{
				//std::cout << "Detected leaf node: " << std::endl;
				// Detect if leaf is left child or right child,make sure there is no dangling leaf node which is not deleted.

				if (Tree == Tree->pParent->LeftChild)
				{
					//std::cout << "Deleting Left leaf operand node: ";
					// A left leaf
					Temp = Tree->pParent;
					Temp->LeftChild = nullptr;
					delete Tree;
					Tree = nullptr;
				}
				else if (Tree == Tree->pParent->RightChild)
				{
					// A right leaf
					//std::cout << "Deleting Right leaf operand node: ";
					Temp = Tree->pParent;
					Temp->RightChild = nullptr;
					delete Tree;
					Tree = nullptr;
				}
			}
			else if (Tree->LeftChild == nullptr && Tree->RightChild == nullptr && Tree->pParent == nullptr)
			{
				// Detect if operand is only node in tree.
				//std::cout << "Deleting Root operand node: ";
				// A Root operand
				delete Tree;
				Tree = nullptr;
			}
		}
		else if (Tree->NodeType == NODE_LABEL_OPERATOR && (OpType != OPUNARYMINUS && OpType != OPSIN && OpType != OPCOS\
			&& OpType != OPTAN && OpType != OPSEC && OpType != OPCOT && OpType != OPCOSEC && OpType != OPSQRT && OpType != OPLOG))
		{
			//#if(0)
			// Detect if op is left child or right child,make sure there is no dangling op node which is not deleted.
			//#if(0)
			if (Tree->pParent != nullptr and Tree == Tree->pParent->LeftChild)
			{
				// A left child op node
				//std::cout << "Deleting Left leaf operator node: " << Tree->Op << std::endl;

				Temp = Tree->pParent;
				Temp->LeftChild = nullptr;
				delete Tree;
				Tree = nullptr;

			}//#endif
				// Detect if op is left child or right child,make sure there is no dangling op node which is not deleted.
			else if (Tree->pParent != nullptr && Tree == Tree->pParent->RightChild)
			{
				// A Right child op node.
				//std::cout << "Deleting Right leaf operator node: " << Tree->Op << std::endl;
				Temp = Tree->pParent;
				Temp->RightChild = nullptr;
				delete Tree;
				Tree = nullptr;
			}
			else if (Tree->pParent == nullptr && Tree->LeftChild != nullptr && Tree->RightChild != nullptr)
			{

				//std::cout << "Detected Root operator node: " << Tree->Op << std::endl;
			}
			DeleteBinaryTree(Tree->LeftChild);
			DeleteBinaryTree(Tree->RightChild);
		} /* if Node is unary minus with one left child */
		else if (Tree->NodeType == NODE_LABEL_OPERATOR && (OpType == OPUNARYMINUS || OpType == OPSIN || OpType == OPCOS || \
			OpType == OPTAN || OpType == OPSEC || OpType == OPCOT || OpType == OPCOSEC || OpType == OPSQRT || OpType == OPLOG))
		{
			// Detect if op is left child or right child,make sure there is no dangling op node which is not deleted.
			if (Tree->pParent != nullptr and Tree == Tree->pParent->LeftChild)
			{
				// A left child op node

				//std::cout << "Deleting Left leaf unary minus operator node: " << Tree->Op << std::endl;
				Temp = Tree->pParent;
				Temp->LeftChild = nullptr;
				delete Tree;
				Tree = nullptr;
			}
			else if (Tree->pParent != nullptr && Tree == Tree->pParent->RightChild)
			{
				// A Right child op node.
				//std::cout << "Deleting Right leaf unary minus operator node: " << Tree->Op << std::endl;
				Temp = Tree->pParent;
				Temp->RightChild = nullptr;
				delete Tree;
				Tree = nullptr;
			}
			else if (Tree->pParent == nullptr && Tree->LeftChild != nullptr && Tree->RightChild == nullptr)
			{
				//std::cout << "Detected Root operator node: " << Tree->Op << std::endl;
				DeleteBinaryTree(Tree->LeftChild);
			}
		}
		if (Tree)
		{
			/* Delete root node*/
			if (Tree->NodeType == NODE_LABEL_OPERATOR && Tree->pParent == nullptr && Tree->LeftChild == nullptr && Tree->RightChild == nullptr)
			{
				//std::cout << "Deleting Root node " << std::endl;
				delete Tree;
				Tree = nullptr;
			}
			else if (Tree->NodeType == NODE_LABEL_OPERATOR && Tree->pParent == nullptr && Tree->LeftChild != nullptr && Tree->RightChild == nullptr)
			{
				//std::cout << "Deleting Root node which is unary minus" << std::endl;
				delete Tree;
				Tree = nullptr;
			}
		}
	}
}

#if defined (DEVELOPMENT_DEBUGGING_ENABLED)
/*
Component Function: void PrintNode(const Tnode* Node)
Arguments:  pointer to Node
Returns: true or not
Description:  Displays data in node.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintNode(const Tnode* Node)
{
	if (Node)
	{
		if (Node->NodeType == NODE_LABEL_OPERAND)
		{
			switch (Node->Data.Type)
			{
			case INTTYPE:
				//std::cout << "Operand node type is INTTYPE " << Node->Data.u.intValue << std::endl;
				break;
			case UINTTYPE:
				//std::cout << "Operand node type is UINTTYPE " << Node->Data.u.uintValue << std::endl;
				break;
			case FLOATTYPE:
				//std::cout << "Operand node type is FLOATTYPE " << Node->Data.u.floatValue << std::endl;
				break;
			case DOUBLETYPE:
				//std::cout << "Operand node type is DOUBLETYPE " << Node->Data.u.doubleValue << std::endl;
				break;
			case LONGDOUBLETYPE:
				//std::cout << "Operand node type is long double Type " << Node->Data.u.longdoubleValue << std::endl;
				break;
			case LONGINTTYPE:
				//std::cout << "Operand node type is LONGINTTYPE " << Node->Data.u.longValue << std::endl;
				break;
			case ULONGINTTYPE:
				//std::cout << "Operand node type is ULONGINTTYPE " << Node->Data.u.ulongValue << std::endl;
				break;
			case SHORTTYPE:
				//std::cout << "Operand node type is SHORTTYPE " << Node->Data.u.shortValue << std::endl;
				break;
			case USHORTTYPE:
				//std::cout << "Operand node type is USHORTTYPE " << Node->Data.u.ushortValue << std::endl;
				break;
			default:
				break;
			}
		}
		else if (Node->NodeType == NODE_LABEL_OPERATOR)
		{
			//std::cout << "Operator node is " << Node.Data.u.cString  << std::endl;
		}
		else if (Node->NodeType == IDENT)
		{
			//std::cout << "Ident node is " << Node.Data.u.cString << std::endl;
		}

	}
	else
	{
		//std::cout << "Node is nullptr" << std::endl;
	}
}

/*
Component Function: void PrintTreeNode(const Tnode& Node)
Arguments:  Reference to Node
Returns: true or not
Description:  Displays data in node.
Version : 0.1
Notes:
This function would be used while construction of Binary tree.
 */
void PrintTreeNode(const Tnode& Node)
{
	//std::cout << "In PrintTreeNode function " << std::endl;
	if (Node.NodeType == NODE_LABEL_OPERAND)
	{
		switch (Node.Data.Type)
		{
		case INTTYPE:
			std::cout << "Operand node type is INTTYPE " << Node.Data.u.intValue << std::endl;
			break;
		case UINTTYPE:
			std::cout << "Operand node type is UINTTYPE " << Node.Data.u.uintValue << std::endl;
			break;
		case FLOATTYPE:
			std::cout << "Operand node type is FLOATTYPE " << Node.Data.u.floatValue << std::endl;
			break;
		case DOUBLETYPE:
			std::cout << "Operand node type is DOUBLETYPE " << Node.Data.u.doubleValue << std::endl;
			break;
		case LONGDOUBLETYPE:
			std::cout << "Operand node type is long double Type " << Node.Data.u.longdoubleValue << std::endl;
			break;
		case LONGINTTYPE:
			std::cout << "Operand node type is LONGINTTYPE " << Node.Data.u.longValue << std::endl;
			break;
		case ULONGINTTYPE:
			std::cout << "Operand node type is ULONGINTTYPE " << Node.Data.u.ulongValue << std::endl;
			break;
		case SHORTTYPE:
			std::cout << "Operand node type is SHORTTYPE " << Node.Data.u.shortValue << std::endl;
			break;
		case USHORTTYPE:
			std::cout << "Operand node type is USHORTTYPE " << Node.Data.u.ushortValue << std::endl;
			break;
		case IDENT:
			std::cout << "IDENT is " << Node.Data.u.cString << std::endl;
			break;
		default:
			break;
		}
	}
	else if (Node.NodeType == NODE_LABEL_OPERATOR)
	{
		std::cout << "Operator node is " << Node.Data.u.cString << std::endl;
	}
}

#endif // #if defined (DEVELOPMENT_DEBUGGING_ENABLED)
/*
 Component Function: void ClearNode(Tnode &Node)
 Arguments:  Reference to node
 returns: None
 Description:
 Clears node
 Version : 0.1
 */
void ClearNode(Tnode &Node)
{
	Node.NodeType = NODE_NOT_INITIALIZED;
	Node.LineNumber = ZERO;
	Node.ColumnNumber = ZERO;
	Node.Data.u.cString = "NONE";
	Node.LeftChild = nullptr;
	Node.RightChild = nullptr;
	Node.pParent = nullptr;
}
#endif //  #if defined (EVALUATION_METHOD_EXPRESSIONTREE)
